# This file has the code for both AbsSynthe as well as without AbsSynthe.
# The part without AbsSynthe is now commented

import numpy as np
import sys
import random
from decodeSafe import AIG

from six import StringIO, b

import networkx as nx
import time
import os
from fractions import Fraction
from decimal import Decimal

from gym import Env, spaces, utils
from gym.utils import seeding


def get_tasks(file_name):
    f=open(file_name, "r")

    #readlines reads the individual line into a list
    fl =f.readlines()
    lines=[]
    tasks= []
    hard_tasks= []
    soft_tasks= []
    has_soft_tasks = False

    f.close()

    # for each line, extract the task parameters

    for x in fl:
        y = x.strip()

        if y[0] == '-':
            hard_tasks = tasks
            tasks = []
            has_soft_tasks = True

        elif y!='' and y[0]!='#':
            lines.append(y)
            task = y.split("|")  # task should have 4 elements

            arrival = int(task[0])

            dist = task[1].split(";")  # distribution on the execution time
            exe = []
            max_exe_time = 0
            for z in dist:
                z = z.strip("[")
                z = z.strip("]")
                z = z.split(",")
                time = int(z[0])

                if time > max_exe_time:  # compute maximum execution time
                    max_exe_time = time

                # change to fraction since float arithmetic produces bad precision
                #prob = float(z[1])
                #exe.append((time, prob))
                exe.append((time, Fraction(Decimal(z[1]))))

            deadline = int(task[2])

            dist = task[3].split(";")  # distribution on the period
            period = []
            arrive_time = []
            for z in dist:
                z = z.strip("[")
                z = z.strip("]")
                z = z.split(",")
                time = int(z[0])

                # change to fraction since float arithmetic produces bad precision
                #prob = float(z[1])
                #period.append((time, prob))
                period.append((time, Fraction(Decimal(z[1]))))

                arrive_time.append(time)

            min_arrive_time = min(arrive_time)

            if has_soft_tasks:
                cost = Fraction(Decimal(task[4]))
                tasks.append([arrival, exe, deadline, period, max_exe_time, min_arrive_time, cost])
            else:
                tasks.append([arrival, exe, deadline, period, max_exe_time, min_arrive_time])

    if has_soft_tasks:
        soft_tasks = tasks
    else:
        hard_tasks = tasks

    return hard_tasks, soft_tasks



def get_num_tasks(hard_tasks, soft_tasks):

    return len(hard_tasks) + len(soft_tasks)



def get_upper_bound_num_states(hard_tasks, soft_tasks):

    # We assume that for every task the  number of states S_i of task i is bounded above by
    # max of arrival time dist + (max of arrival time dist - min of arrival time dist) x num of elements in the arrival time dist
    # + (max of exe time dist - min of exe time dist) x num of elements in the exe time dist
    #
    # The total number of states is bounded above by the product of S_i over all tasks i.

    tasks = hard_tasks + soft_tasks
    num_states = 1

    for x in tasks:
        exe_dist = x[1]
        exe_times = [i[0] for i in exe_dist]
        max_exe_time = max(exe_times)
        min_exe_time = min(exe_times)

        arr_dist = x[3]
        arr_times = [i[0] for i in arr_dist]
        max_arr_time = max(arr_times)
        min_arr_time = min(arr_times)

        num_states = num_states + max_arr_time + (max_arr_time - min_arr_time) * len(arr_dist) +  \
                     (max_exe_time - min_exe_time) * len(exe_dist)

    return num_states



def get_init_state(hard_tasks, soft_tasks):
    # add states representing individual tasks

    tasks = hard_tasks + soft_tasks
    # extract the parameters for each task
    init_state = []
    for x in tasks:
        if x[0] > 0:  # initial arrival time > 0
            #task = [0, 0, x[0]]
            # change to fraction
            task = [[(0,1)], 0, [(x[0],1)]]
            #task = [[(0,Fraction(1,1))], 0, [(x[0],Fraction(1,1))]]

        else:
            rct_dist = [(support, float(prob)) for (support, prob) in x[1]]
            deadline = x[2]
            iat_dist = [(support, float(prob)) for (support, prob) in x[3]]
            task = [rct_dist, deadline, iat_dist]  # [RCT dist, deadline, inter-arrival time dist]
            #task = [x[1], x[2], x[3]]

        init_state.append(task)

    return init_state, [0] * len(tasks)  # [0] denotes bad_flag



def dec_dist(dist):
    new_dist = []
    for x in dist:
        new_dist.append((x[0]-1, x[1]))

    return new_dist



def modify_params(rct_dist, curr_deadline, iat_dist):
    dec_rct_dist = dec_dist(rct_dist)
    dec_iat_dist = dec_dist(iat_dist)

    if curr_deadline > 0:  # check if deadline is greater than 0
        deadline = curr_deadline - 1
    else:
        deadline = curr_deadline

    return dec_rct_dist, deadline, dec_iat_dist



def normalize_dist(dist):
    # dist is a list with elements of the form (time, prob)
    prob_dist = [x[1] for x in dist]
    sum_dist = sum(prob_dist)

    dist = [(x[0], 1 / sum_dist * x[1]) for x in dist]  # float is not needed since we use fraction
    #dist = [(x[0], float(1/sum_dist) * x[1]) for x in dist]
    # dist = [(x[0], float(x[1]/sum_dist)) for x in dist] # This expression is correct but gives imprecise floating point numbers
    return dist



def add_possible_states_task_j(rct_dist, iat_dist, init_config_j, deadline, dec_iat_dist, hard):

    possible_states_task_j = []

    if iat_dist[0][0] > 1:
        possible_states_task_j.append((1, [rct_dist, deadline, dec_iat_dist], 0))

    elif iat_dist[0][0] == 1:
        if hard:
            if deadline >= rct_dist[-1][0]:  # deadline >= max rct
                possible_states_task_j.append((iat_dist[0][1], init_config_j, 0))  # initial state
        else:
            bad_flag = 0
            if rct_dist[0][0] > 0:
                bad_flag = 1
            possible_states_task_j.append((iat_dist[0][1], init_config_j, bad_flag))  # initial state

        if len(iat_dist) > 1:
            possible_states_task_j.append((1-iat_dist[0][1], [rct_dist, deadline, dec_iat_dist[1:]], 0))

    return possible_states_task_j



# Right now, we don't use this function
# Why don't we use this?
def safe(task):
    # task is an element of the form [rct, deadline, iat], where rct is of the form [(t1, p1), ..., (tn, pn)]
    # check if deadline >= max rct
    max_rct = task[0][-1][0]
    deadline = task[1]
    if max_rct > deadline:
        return False

    else:
        return True



def compute_states(raw_states, index, num_hard_tasks):
    # raw_states is a list where each element is also a list that corresponds to each task and has elements (prob, [rct, deadline, iat], bad_flag)
    # it returns states where each element is of the form (prob, [[rct1, deadline1, iat1], [rct2, deadline2, iat2], ... [rctn, deadlinen, iatn]], [bad_flag1, bad_flag2, ... bad_flagn]),
    # that is the set of future states
    # we compute the probabilities of each state, denoted by prob and bad_array_i in state i is an array with an element
    # for each task; the element is 1 if the task does not meet its deadline, else it is 0

    hard = True
    if index > num_hard_tasks:
        hard = False
    states = []
    if len(raw_states)==1:
        for x in raw_states[0]:
            if hard:
                if safe(x[1]):
                    states.append((x[0], [x[1]], [0]))
            else:
                states.append((x[0], [x[1]], [x[2]]))

        return states

    else:
        # recursive call
        subsets = compute_states(raw_states[1:], index+1, num_hard_tasks)
        for x in raw_states[0]:
            if hard:
                if safe(x[1]):
                    for y in subsets:
                        states.append((x[0]*y[0], [x[1]] + y[1], [0] + y[2]))

            else:  # this will never be executed since we do not compute the product of the soft tasks together
                for y in subsets:
                    states.append((x[0] * y[0], [x[1]] + y[1], [x[2]] + y[2]))

        return states



def get_states_execute_i(all_tasks, i, tasks, num_hard_tasks):
    # all_tasks is a list of [rct dist, deadline, iat dist] elements, one for each task. It denotes the current state
    # i is the task that will be executed (start index is 0)
    # tasks is a list of the task descriptions [arrival, exe dist, deadline, period dist, max_exe_time, min_arrive_time]

    # This function returns a tuple (i, states), where states is the set of all states obtained after executing task i,
    # and the second member of the tuple denotes that task i has been executed to reach these states
    # each element of states is a state where each state is of the form
    # (prob, [[rct1, deadline1, iat1], [rct2, deadline2, iat2], ... [rctn, deadlinen, iatn]], [bad_flag1, bad_flag2, ... bad_flagn]),

    raw_states = []
    hard = True

    for j in range(len(all_tasks)):

        if j == num_hard_tasks:
            hard = False

        task = all_tasks[j]
        init_config_j = tasks[j][1:4]  # gives rct, deadline, iat in the current config

        # normalize distributions so that the probabilities sum up to 1
        rct_dist = normalize_dist(task[0])
        iat_dist = normalize_dist(task[2])
        possible_states_task_j = []

        # reduce times by 1 due to one clock tick
        dec_rct_dist, deadline, dec_iat_dist = modify_params(rct_dist, task[1], iat_dist)

        # compute remaining computation time distribution for task j that is scheduled
        if j == i:
            if (rct_dist[0][0] > 1 or len(rct_dist) == 1) and iat_dist[0][0] > 1:
                # only one successor for task j
                # the first parameter is the probability that task j evolves to this state
                # the probability is 1 since there is only one successor
                # the last parameter is a bad flag needed only for soft tasks, it is 1 when a new task comes but the previous one is not finished
                possible_states_task_j.append((1, [dec_rct_dist, deadline, dec_iat_dist], 0))

            elif iat_dist[0][0] == 1 and len(iat_dist) == 1:
                if not hard:
                    # soft task executes and remaining computation time > 1
                    if rct_dist[0][0] > 1:
                        possible_states_task_j.append((1, init_config_j, 1))  # initial state

                    else:  # rct_dist[0][0] == 1
                        possible_states_task_j.append((rct_dist[0][1], init_config_j, 0))

                        if len(rct_dist) > 1:  # task not getting finished with probability 1 - rct_dist[0][1]
                            possible_states_task_j.append(((1 - rct_dist[0][1]), init_config_j, 1))

                else:  # hard task
                    # one successor that is the initial config for task j
                    # the probability is 1 since there is only one successor
                    possible_states_task_j.append((1, init_config_j, 0))  # initial state

            else:
                # we reach here if
                # min RCT=1 and len(RCT) > 1 and len(iat) >= 1 (min(iat) > 1 when len(iat)=1) or
                # min(RCT) > 1 and len(iat) > 1 and min(iat) = 1
                if iat_dist[0][0] == 1:  # one of the successor states is the initial state of task i=j
                    # len(iat) > 1 here
                    if rct_dist[0][0] == 1 and len(rct_dist) > 1:
                        possible_states_task_j.append((rct_dist[0][1] * iat_dist[0][1], init_config_j, 0))  # initial state with task finished
                        if hard:
                            possible_states_task_j.append(((1 - rct_dist[0][1]) * iat_dist[0][1], init_config_j, 0))
                        else:
                            possible_states_task_j.append(((1 - rct_dist[0][1]) * iat_dist[0][1], init_config_j, 1))  # initial state with task not finished
                        possible_states_task_j.append((rct_dist[0][1] * (1 - iat_dist[0][1]), [[dec_rct_dist[0]], deadline, dec_iat_dist[1:]], 0))
                        possible_states_task_j.append(((1-rct_dist[0][1]) * (1-iat_dist[0][1]), [dec_rct_dist[1, :], deadline, dec_iat_dist[1:]], 0))

                    elif rct_dist[0][0] == 1:  # and len(rct_dist) == 1
                        possible_states_task_j.append((iat_dist[0][1], init_config_j, 0))
                        possible_states_task_j.append((1-iat_dist[0][1], [dec_rct_dist, deadline, dec_iat_dist[1:]], 0))

                    else:  # rct_dist[0][0] > 1
                        if hard:
                            possible_states_task_j.append((iat_dist[0][1], init_config_j, 0))
                        else:
                            possible_states_task_j.append((iat_dist[0][1], init_config_j, 1))
                        possible_states_task_j.append((1-iat_dist[0][1], [dec_rct_dist, deadline, dec_iat_dist[1:]], 0))

                else:  # min(iat) > 1
                    # Here min RCT=1 and len(RCT) > 1
                    possible_states_task_j.append((rct_dist[0][1], [[dec_rct_dist[0]], deadline, dec_iat_dist], 0))
                    possible_states_task_j.append((1-rct_dist[0][1], [dec_rct_dist[1:], deadline, dec_iat_dist], 0))

        else:  # when j!=i is scheduled
            possible_states_task_j = add_possible_states_task_j(rct_dist, iat_dist, init_config_j, deadline,
                                                                dec_iat_dist, hard)
        raw_states.append(possible_states_task_j)

    states = compute_states(raw_states, 1, num_hard_tasks)
    len_tasks = len(tasks)
    # states = filter(lambda (prob, next, bad_flag): len(next)==len_tasks, states)  # filters only valid states: the ones that have n tasks

    states_next = [x[1] for x in states]

    st = []
    for k in range(len(states_next)):
        if len(states_next[k])==len_tasks:
           st.append(states[k])

    # filters only valid states: the ones that have n tasks

    return i, st  # returns a tuple of the form (i, states)



def get_states_execute_none(all_tasks, tasks, num_hard_tasks):
    # This function returns a tuple (-1, states), where states is the set of all states obtained from the current state
    # all_tasks when no task is executed. The slacks can be used by soft tasks

    raw_states = []
    hard = True

    for j in range(len(all_tasks)):

        if j == num_hard_tasks:
            hard = False

        task = all_tasks[j]
        init_config_j = tasks[j][1:4]  # gives rct, deadline, iat in the current config

        # normalize distributions so that the probabilities sum up to 1
        rct_dist = normalize_dist(task[0])
        iat_dist = normalize_dist(task[2])

        dec_rct_dist, deadline, dec_iat_dist = modify_params(rct_dist, task[1], iat_dist)

        possible_states_task_j = add_possible_states_task_j(rct_dist, iat_dist, init_config_j, deadline, dec_iat_dist, hard)
        raw_states.append(possible_states_task_j)

    states = compute_states(raw_states, 1, num_hard_tasks)
    len_tasks = len(tasks)
    # states = filter(lambda (prob, next, bad_flag): len(next)==len_tasks, states)

    states_next = [x[1] for x in states]
    st = []
    for k in range(len(states_next)):
        if len(states_next[k])==len_tasks:
           st.append(states[k])

    return -1, st  # -1 denotes no task has been executed



# pop the first element of queue
def pop_queue(queue):

    return (queue[0], queue[1:])

# push n elements to the end of queue
def push_queue(queue, n):

    return queue + range(queue[-1]+1, queue[-1]+1+n)



def get_new_states(all_tasks, tasks):
    # all_tasks is a list of [rct dist, deadline, iat dist] elements, one for each task. It denotes the current state
    # tasks is a list of the task descriptions [arrival, exe dist, deadline, period dist, max_exe_time, min_arrive_time]
    # hard is a flag that is set to True for hard tasks and False for soft tasks

    # This function returns new_states which is a list of elements of the form (i, states), where
    # i -- (start index 0) is the task that is executed, -1 when no task is executed and
    # states -- is the list of states reached from current state all_tasks after executing task i.
    # Every state is a list of the form [rct dist, deadline, iat dist]

    new_states = []
    for i in range(len(all_tasks)):
        # execute task i if min rct > 0

        task_i_rct_dist = all_tasks[i][0]
        task_i_deadline = all_tasks[i][1]
        if task_i_rct_dist[0][0] > 0 and task_i_deadline > 0:
            # get_states_execute_i returns the set of states that are obtained by executing task i from current state all_tasks
            # and are locally known to be safe as guaranteed by function safe, i.e., max rct <= deadline
            new_states.append(get_states_execute_i(all_tasks, i, tasks, len(all_tasks)))

    # add states when no task is executed
    new_states.append((get_states_execute_none(all_tasks, tasks, len(all_tasks))))

    return new_states



def normalize_state(state):

    norm_state = []

    for task in state:
        [rct_dist, deadline, iat_dist] = task
        mod_task = [normalize_dist(rct_dist), deadline, normalize_dist(iat_dist)]
        norm_state.append(mod_task)

    return norm_state



def add_new_states(G, next_states, node_num, new_node_cnt):
    # next_states is a list of the form [(0, states1) (1, states2) ... (n,statesn) ... (-1, states)] and
    # each of states1, states2, ... statesn, states is a list of elements the form (prob, state)

    for j, next_states_exe_j in next_states:

        for prob, state, bad_flag in next_states_exe_j:
            # normalize the distributions of all tasks in a state
            state = normalize_state(state)

            # check if the state is already present in G, the edge from current_node to state still needs to be added
            # nodes_with_same_param = filter(lambda (n, d): d['config'] == (state, bad_flag), G.nodes(data=True))
            # the above line with lambda does not work with python3
            nodes_with_same_param = []
            for (n, d) in G.nodes(data=True):
                if d['config'] == (state, bad_flag):
                    nodes_with_same_param.append((n, d))

            node_num_same_param = [x[0] for x in nodes_with_same_param]

            # There should be at most one node with the same param
            len_node_num_same_param = len(node_num_same_param)
            # assert len_node_num_same_param <= 1

            if len_node_num_same_param == 0:  # no existing node with the same config
                # add a new node
                new_node_cnt+=1
                G.add_node(new_node_cnt, config=(state, bad_flag))

                # add an edge from the current node to the new node with the task that is executed and the probability
                G.add_edge(node_num, new_node_cnt, label=(j, prob))

            else:  # exists a node with the same config
                # add an edge from the current node to the existing node
                G.add_edge(node_num, node_num_same_param[0], label=(j, prob))

    return G, new_node_cnt



def construct_graph(tasks):

    # Build the graph of product of hard tasks
    # start_time = time.time()
    G = nx.DiGraph()

    # extract the parameters for each task
    state = []
    for x in tasks:
        if x[0] > 0:  # initial arrival time > 0
            #task = [0, 0, x[0]]
            # change to fraction
            #task = [[(0,1)], 0, [(x[0],1)]]
            task = [[(0,Fraction(1,1))], 0, [(x[0],Fraction(1,1))]]

        else:
            task = [x[1], x[2], x[3]]  # [RCT dist, deadline, Period dist]

        state.append(task)

    # construct the initial state
    node_cnt = 1
    G.add_node(node_cnt, config=(state, [0]))

    # construct the transitions
    queue = [node_cnt]

    while len(queue) > 0:
        (queue_head, queue) = pop_queue(queue)
        next_states = get_new_states(G.nodes[queue_head]['config'][0], tasks)

        if len(next_states) > 0:
            first_elem_to_push = queue_head+1 if not queue else queue[-1]+1
            G, new_node_cnt = add_new_states(G, next_states, queue_head, first_elem_to_push-1)

            # push the new nodes to the end of the queue so that they can be explored further to extend G
            queue = queue + list(range(first_elem_to_push, new_node_cnt+1))

    # time_reqd = time.time() - start_time
    # r= open("results.txt", "a+")
    # r.write("\r\n\r\nNum tasks %d, " % len(tasks))
    # r.write("Num states %d, " % len(G.nodes()))
    # r.write("Construct model: %s seconds \r\n" % time_reqd)
    return G



def get_reachable_nodes(G):

    queue = [1]
    visited = []
    while len(queue) > 0:
        (n, queue) = pop_queue(queue)

        if not n in visited:
            succ_nodes = list(G.successors(n))
            queue = queue + succ_nodes
            visited.append(n)

    return visited



def restrict_to_safe_states(G, len_hard_tasks):

    # start_time = time.time()
    labels = list(range(len_hard_tasks)) + [-1]

    # Remove those nodes for which every action may lead to an unsafe node to false
    flag = True
    unsafe_nodes = []

    while flag:
        flag = False

        for n in list(G.nodes()):

            to_remove = True
            for j in labels:
                # edges_task_j = filter(lambda (n1, n2, d): d['label'][0] == j, G.edges(n, data=True))
                # lambda with tuple does not work in Python3
                edges_task_j = []
                for (n1, n2, d) in G.edges(n, data=True):
                    if d['label'][0] == j:
                        edges_task_j.append((n1, n2, d))

                sum_prob_task_j = sum([d['label'][1] for (n1, n2, d) in edges_task_j])

                if sum_prob_task_j == 1:  # for action j all possible nodes are safe
                    to_remove = False

                elif edges_task_j:  # there are some edge for task j but their probabilities do not add up to 1
                    # Here 0 < sum_prob_task_j < 1
                    G.remove_edges_from(edges_task_j)

            if to_remove:
                # remove unsafe node and all the incoming and outgoing edges
                G.remove_node(n)
                flag = True

                # Debug
                unsafe_nodes.append(n)

    # Since some edges were removed, it is possible that some nodes become unreachable
    # Remove the unreachable nodes
    if G.nodes():
        unreachable_nodes = set(G.nodes()) - set(get_reachable_nodes(G))
        G.remove_nodes_from(unreachable_nodes)

        # Debug
        # print("%d Unreachable nodes" % len(unreachable_nodes))
        # print(unreachable_nodes)

    # Debug
    # print("Final list of nodes: ")
    # print(list(G.nodes()))
    # print("%d Unsafe nodes" % len(unsafe_nodes))
    # print(unsafe_nodes)
    # time_reqd = time.time() - start_time
    # r= open("results.txt", "a+")
    # r.write("Num safe states %d, " % len(G.nodes()))
    # r.write("Construct safe model: %s seconds \r\n\r\n" % time_reqd)

    return G


def get_dict_safe_actions(G, len_hard_tasks):

    labels = list(range(len_hard_tasks)) + [-1]
    safe_action_dict = {}

    for (n, st_data) in G.nodes(data=True):
        safe_actions = []
        for j in labels:
            for (n1, n2, d) in G.edges(n, data=True):
                if d['label'][0] == j:
                    safe_actions = safe_actions + [j]

        res = []
        for task in st_data['config'][0]:
            rct = tuple(support for (support, prob) in task[0])
            deadline = task[1]
            iat = tuple(support for (support, prob) in task[2])
            res.append((rct, deadline, iat))

        res = tuple(res)
        safe_action_dict[res] = list(set(safe_actions))

    return safe_action_dict


def fraction_to_float(hard_tasks, soft_tasks):
    ht=[]
    st = []

    for x in hard_tasks:
        rct_dist = [(support, float(prob)) for (support, prob) in x[1]]
        iat_dist = [(support, float(prob)) for (support, prob) in x[3]]
        task = [x[0], rct_dist, x[2], iat_dist, x[4], x[5]]  # [initial arrival, RCT dist, deadline, inter-arrival time dist]
        ht.append(task)

    for x in soft_tasks:
        rct_dist = [(support, float(prob)) for (support, prob) in x[1]]
        iat_dist = [(support, float(prob)) for (support, prob) in x[3]]
        task = [x[0], rct_dist, x[2], iat_dist, x[4], x[5], float(x[6])]  # [initial arrival, RCT dist, deadline, inter-arrival time dist]
        st.append(task)

    return ht, st


def choose_state(next_states_execute_i):
    # next_states_execute_i is a list of states of the form [(prob1, state1, bad_flag_list1), ... (probm, statem, bad_flag_listm)]
    # where m is the number of states that are obtained by executing task i. Here i can be -1 also denoting no task scheduled
    probs = [x[0] for x in next_states_execute_i]
    cum_probs = np.cumsum(probs)  # cumulative sum of probabilities

    #random_prob = random.uniform(0, 1)
    random_prob = random.random()
    # From next_states choose the state whose index i is such that probs[i-1] <= random_prob <= probs[i]
    for j in range(len(cum_probs)):
        if cum_probs[j] >= random_prob:
            return next_states_execute_i[j]
    #ind_list = [i for i in range(len(cum_probs)) if cum_probs[i] >= random_prob]

    #return next_states_execute_i[ind_list[0]]


def get_safe_actions(G, currentState, num_hard_tasks):
    # G is a graph of safe states and currentState is a list of tasks, where each task is of the form
    # [rct distribution, deadline, iat distribution]
    # This function returns the safe set of actions from currentState; an action is a hard task or a nop action
    curr_state_hard_tasks = currentState[0:num_hard_tasks]

    # get the set of actions from curr_state_hard_tasks in G
    # filter(lambda (n, d): d['config'] == (state, bad_flag), G.nodes(data=True))
    for (n, d) in list(G.nodes(data=True)):
        if d['config'][0] == curr_state_hard_tasks:
            actions = []
            # filter(lambda (n1, n2, d): d['label'][0] == j, G.edges(n, data=True))
            for (n1, n2, e) in G.edges(n, data=True):
                actions.append(e['label'][0])

            return list(set(actions))


def get_safe_actions_dict(safe_action_dict, currentState, num_hard_tasks):
    # This function returns the safe set of actions from currentState; an action is a hard task or a nop action
    curr_state_hard_tasks = currentState[0:num_hard_tasks]
    #res = tuple([(tuple(exe), deadline, tuple(iat)) for (exe, deadline, iat) in curr_state_hard_tasks])
    res = []
    for task in curr_state_hard_tasks:
        rct = tuple(support for (support, prob) in task[0])
        deadline = task[1]
        iat = tuple(support for (support, prob) in task[2])
        res.append((rct, deadline, iat))

    res = tuple(res)
    return safe_action_dict[res][:]


def find_exe_time(rct_dist, max_exe_dist):
    if len(rct_dist) == 1 and rct_dist[0][0] == 0:
        # return -2  # a task should be allowed to execute if remaining execution time is positive
        return max_exe_dist[-1][0]
    else:
        return max_exe_dist[-1][0] - rct_dist[-1][0]


def get_safe_actionsAS(G, safe_action_dict, currentState, hard_tasks):
    # G is a graph of safe states and currentState is a list of tasks, where each task is of the form
    # [rct distribution, deadline, iat distribution]
    # This function returns the safe set of actions from currentState; an action is a hard task or a nop action
    num_hard_tasks = len(hard_tasks)
    curr_state_hard_tasks = currentState[0:num_hard_tasks]
    #res = tuple([(tuple(exe), deadline, tuple(iat)) for (exe, deadline, iat) in curr_state_hard_tasks])
    res = []
    for task in curr_state_hard_tasks:
        rct = tuple(support for (support, prob) in task[0])
        deadline = task[1]
        iat = tuple(support for (support, prob) in task[2])
        res.append((rct, deadline, iat))

    res = tuple(res)

    if res in safe_action_dict:
        return (safe_action_dict, safe_action_dict[res])

    else:
        len_hard_tasks = len(hard_tasks)
        exe_times = []
        times_since_arr = []
        max_exe_times = []
        max_arr_times = []
        allowed_actions = []

        for ind in range(len_hard_tasks):
            rct_dist = currentState[ind][0]
            arr_dist = currentState[ind][2]

            max_exe_dist = hard_tasks[ind][1]
            max_arr_dist = hard_tasks[ind][3]

            exe_times.append(find_exe_time(rct_dist, max_exe_dist))

            assert(arr_dist[-1][0] != 0)
            times_since_arr.append((max_arr_dist[-1][0] - arr_dist[-1][0]))

            max_exe_times.append(hard_tasks[ind][4])
            max_arr_times.append(max_arr_dist[-1][0])

        for ind in range(len_hard_tasks):
            if exe_times[ind] >= 0 and currentState[ind][0][0][0] > 0:  # remaining execution time is greater than 0
                if G.decode(exe_times, times_since_arr, max_exe_times, max_arr_times, ind):
                    allowed_actions.append(ind)

        if G.decode(exe_times, times_since_arr, max_exe_times, max_arr_times, -1):
            allowed_actions.append(-1)

        safe_action_dict[res] = allowed_actions

        return (safe_action_dict, allowed_actions)


def active(i, all_tasks):
    # all_tasks is a list of [rct dist, deadline, iat dist] elements, one for each task. It denotes the current state
    # i is the task to be executed
    if i == -1:  # nop action is always considered active
        return True
    else:
        task_i_rct_dist = all_tasks[i][0]
        task_i_deadline = all_tasks[i][1]

        if task_i_rct_dist[0][0] > 0 and task_i_deadline > 0:  # task i can be executed only if its deadline
            # is not over and it has not finished execution, that is task i is active
            return True
        else:
            return False


def get_next_state(all_tasks, i, tasks, num_hard_tasks):  # all_tasks is the current State, and task i is executed.
    # all_tasks is a list of [rct dist, deadline, iat dist] elements, one for each task. It denotes the current state
    # i is the task to be executed
    # tasks is a list of the task descriptions [arrival, exe dist, deadline, period dist, max_exe_time, min_arrive_time]

    next_states = []
    if i >=0:
        task_i_rct_dist = all_tasks[i][0]
        task_i_deadline = all_tasks[i][1]

        if task_i_rct_dist[0][0] > 0 and task_i_deadline > 0:  # task i can be executed only if its deadline
            # is not over and it has not finished execution
            next_states.append(get_states_execute_i(all_tasks, i, tasks, num_hard_tasks))

    else:
        next_states.append((get_states_execute_none(all_tasks, tasks, num_hard_tasks)))
    # next_states is of the form [(i,states)]; this is a list of only one element
    # next_states[0][1] is states

    if len(next_states)==0:
        raise Exception("bad length when playing "+str(i)+" from state "+str(all_tasks))
    next_state = choose_state(next_states[0][1]) # returns a tuple of the form (prob, state, bad_flag_list)

    # state is a list where each element is of the form [[rct dist], deadline, [iat_dist]]
    return normalize_state(next_state[1]), next_state[2]  # returns a tuple of the form (state, bad_flag_list)


def get_observation(next_state, tasks, feature_len):
    # This function returns the observation obs where obs is a list of tuples where each tuple corresponds to a task j
    # and is of the form (max(rct_supp) for task j, remaining time before deadline for task j)

    obs = []

    for j in range(len(tasks)):

        max_rct_supp_taskj_next_state = max([x[0] for x in next_state[0][j][0]])
        deadline_taskj = next_state[0][j][1]
        # min_iat_supp_taskj_next_state = min([x[0] for x in next_state[0][j][2]])

        obs = obs + [max_rct_supp_taskj_next_state, deadline_taskj]

    #obs = tuple(obs)

    # debug
    if len(obs) != feature_len:
        print('error')

    return np.array(obs)



def get_observation_space(tasks, len_hard_tasks):

    feature_list_high = []

    for j in range(len(tasks)):

        if j >= len_hard_tasks:
            # for soft tasks
            [arrival, exe, deadline, period, max_exe_time, min_arrive_time, cost] = tasks[j]
        else:
            [arrival, exe, deadline, period, max_exe_time, min_arrive_time] = tasks[j]

        feature_list_high = feature_list_high + [max_exe_time, deadline]

    # debug
    print(len(feature_list_high))

    feature_list_low = [0] * 2 * len(tasks)
    return len(feature_list_high), spaces.Box(np.array(feature_list_low), np.array(feature_list_high), dtype=np.int8)



def get_reward(state, soft_tasks):
    # This function returns the cost if a soft task does not finish
    cost_list = [task[-1] for task in soft_tasks]
    bad_flag_list = state[-1]
    bad_flag_list = bad_flag_list[len(bad_flag_list)-len(soft_tasks):]

    return sum([a*b for a,b in zip(cost_list, bad_flag_list)])

def cost_based_sort(soft_tasks):
    costs = [x[6] for x in soft_tasks]
    soft_tasks_ind_sorted_cost = np.argsort(costs).tolist()
    soft_tasks_ind_sorted_cost.reverse()
    return soft_tasks_ind_sorted_cost, costs

class TaskSystemEnv(Env):

    def __init__(self, file_name):
        # create the task parameters
        self.hard_tasks, self.soft_tasks = get_tasks(file_name)

        self.action_space = spaces.Discrete(get_num_tasks(self.hard_tasks, self.soft_tasks) + 1)  # one more action for nop
        self.feature_len, self.observation_space = get_observation_space(self.hard_tasks + self.soft_tasks, len(self.hard_tasks))

        # Find the safe strategies
        G = construct_graph(self.hard_tasks)
        #os.system('python3 encodeTasks.py ' + file_name)

        # Compute subgraph for only safe strategies
        self.G = restrict_to_safe_states(G, len(self.hard_tasks))
        #os.system('./abssynthe --out_file strat.aag --win_region safe.aag tasks.aag')
        #self.G = AIG('safe.aag')

        # Get a dictionary of safe actions for each state
        #self.safe_action_dict = {}
        self.safe_action_dict = get_dict_safe_actions(self.G, len(self.hard_tasks))

        # Change hard and soft tasks to have decimals instead of fractions
        self.hard_tasks, self.soft_tasks = fraction_to_float(self.hard_tasks, self.soft_tasks)
        self.soft_tasks_ind_sorted_cost, self.costs = cost_based_sort(self.soft_tasks)

        self.seed()
        self.reset()


    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]


    def get_deadline_misses(self):
        return 0


    def reset(self):
        self.currentState = get_init_state(self.hard_tasks, self.soft_tasks)
        self.last_action=None
        return get_observation(self.currentState, self.hard_tasks + self.soft_tasks, self.feature_len)


    def step(self, a):
        # hard_miss_reward = 10000  # (We are not using this feature now.) This is a high cost; once a task is suggested that does not belong to the set of safe actions
        # Check the set of actions that can be executed from the current state
        # The current state should always be safe here
        #allowed_actions = get_safe_actions(self.G, self.currentState[0], len(self.hard_tasks)) # allowed action is a hard task or -1
        # allowed_actions = get_safe_actions_dict(self.safe_action_dict, self.currentState[0], len(self.hard_tasks)) # allowed action is a hard task or -1
        #
        # if not active(a-1, self.currentState[0]):
        #     print(a-1, self.currentState[0], file=sys.stderr)
        #     assert(False)
        # #(self.safe_action_dict, allowed_actions) = get_safe_actionsAS(self.G, self.safe_action_dict, self.currentState[0], self.hard_tasks)  # allowed action is a hard task or -1
        #
        # #if allowed_actions != allowed_actions2:
        # #    print('Not same set of allowed actions!')
        #
        # # Implement the shield
        # if -1 in allowed_actions:
        #     # add all soft tasks in allowed actions
        #     allowed_actions = allowed_actions + list(range(len(self.hard_tasks),len(self.hard_tasks)+len(self.soft_tasks)))
        #
        # # high_cost_flag = False
        # if active(a-1, self.currentState[0]) and a-1 in allowed_actions:
        #     next_state = get_next_state(self.currentState[0], a - 1, self.hard_tasks + self.soft_tasks,
        #                                 len(self.hard_tasks))  # self.currentState[1] contains the bad flag.
        #
        # elif not active(a-1, self.currentState[0]) and a-1 in allowed_actions:
        #     # a-1 is a soft task since a hard task that is not active will never appear in allowed_actions
        #     # This also implies that -1 is in allowed_actions
        #     next_state = get_next_state(self.currentState[0], -1, self.hard_tasks + self.soft_tasks,
        #                                 len(self.hard_tasks))  # execute nop
        #
        # elif active(a-1, self.currentState[0]) and not a-1 in allowed_actions:
        #     # if a-1 is soft, then since a-1 is not in allowed_actions, -1 is also not in allowed_actions
        #     # if a-1 is hard and active but not allowed, then nop cannot also be allowed; hence -1 is also not in allowed_actions
        #     # Thus only some hard tasks are allowed; execute one such hard task
        #     ind_random_allowed_action = random.randint(0,len(allowed_actions) - 1)
        #     next_state = get_next_state(self.currentState[0], allowed_actions[ind_random_allowed_action], self.hard_tasks + self.soft_tasks,
        #                                 len(self.hard_tasks))  # we randomly choose a hard task
        #     # high_cost_flag = True  # some hard task will miss deadline upon executing a-1
        #
        # else:  # not active(a-1, self.currentState[0]) and not a-1 in allowed_actions:
        #     if -1 in allowed_actions:  # it implies all soft tasks are in allowed_actions and hence a-1 is a hard task
        #         next_state = get_next_state(self.currentState[0], -1, self.hard_tasks + self.soft_tasks,
        #                                     len(self.hard_tasks))  # execute nop
        #     else:  # only hard tasks in allowed_actions; execute one such hard task
        #         ind_random_allowed_action = random.randint(0, len(allowed_actions) - 1)
        #         next_state = get_next_state(self.currentState[0], allowed_actions[ind_random_allowed_action], self.hard_tasks + self.soft_tasks,
        #                                     len(self.hard_tasks))  # we randomly choose a hard task
                # high_cost_flag = True  # some hard task will miss deadline upon executing a-1
                # random.random()
        # execute the action a and return the next state
        # next_state = get_next_state(self.currentState[0], a-1, self.hard_tasks + self.soft_tasks, len(self.hard_tasks))  # self.currentState[1] contains the bad flag.
        # if high_cost_flag:
        # reward = hard_miss_reward
        # else:
        #obs = get_observation(next_state, self.hard_tasks + self.soft_tasks, self.feature_len)
        next_state = get_next_state(self.currentState[0], a - 1, self.hard_tasks + self.soft_tasks,
                                    len(self.hard_tasks))
        obs = next_state
        #reward = get_reward(next_state, self.soft_tasks)

        self.currentState = next_state
        self.last_action = a  # The actual action is a-1

        #return (obs, reward*-1, False, {})  # In our case, the reward is the cost that we want to minimize.
        return obs

        #transitions = self.P[self.s][a]
        #i = categorical_sample([t[0] for t in transitions], self.np_random)
        #p, s, r, d= transitions[i]
        #self.s = s
        #self.last_action=a
        #return (s, r, d, {"prob" : p})

    def state_reward(self):
        reward = get_reward(self.currentState, self.soft_tasks)
        return reward * -1  # In our case, the reward is the cost that we want to minimize.

    # This is without AbsSynthe
    def get_allowed_actions(self):
        #allowed_actions = get_safe_actions(self.G, self.currentState[0],
        #                                   len(self.hard_tasks))  # allowed action is a hard task or -1
        allowed_actions = get_safe_actions_dict(self.safe_action_dict, self.currentState[0], len(self.hard_tasks)) # allowed action is a hard task or -1

        #if allowed_actions != allowed_actions2:
        #    print('Not same set of allowed actions in get_allowed_actions!')
        # Implement the shield
        len_curr_state = len(self.currentState[0])
        len_hard_tasks = len(self.hard_tasks)
        if -1 in allowed_actions:
            # add all soft tasks in allowed actions
            #allowed_actions = allowed_actions + list(range(len(self.hard_tasks),len(self.hard_tasks)+len(self.soft_tasks)))
            for j in range(len_hard_tasks, len_curr_state):
                if active(j, self.currentState[0]):
                    allowed_actions = allowed_actions+[j]

        allowed_actions = [x+1 for x in allowed_actions]

        return allowed_actions


    def get_allowed_actionsAS(self):
        #allowed_actions = get_safe_actions(self.G, self.currentState[0],
        #                                   len(self.hard_tasks))  # allowed action is a hard task or -1
        (self.safe_action_dict, allowed_actions) = get_safe_actionsAS(self.G, self.safe_action_dict, self.currentState[0], self.hard_tasks)  # allowed action is a hard task or -1

        #if allowed_actions != allowed_actions2:
        #    print('Not same set of allowed actions in get_allowed_actions!')

        # Implement the shield
        len_curr_state = len(self.currentState[0])
        len_hard_tasks = len(self.hard_tasks)
        if -1 in allowed_actions:
            # add all active soft tasks in allowed actions
            #allowed_actions = allowed_actions + list(range(len(self.hard_tasks),len(self.hard_tasks)+len(self.soft_tasks)))
            for j in range(len_hard_tasks, len_curr_state):
                if active(j, self.currentState[0]):
                    allowed_actions.append(j)

        allowed_actions = [x+1 for x in allowed_actions]

        return allowed_actions


    def set_current_state(self, state):
       self.currentState = state

    def get_current_state(self):
        return self.currentState

    def state_terminal_reward(self, state):
        #allowed_actions = get_safe_actions_dict(self.safe_action_dict, state[0],
        #                                        len(self.hard_tasks))  # allowed action is a hard task or -1
        #allowed_hard_actions = list(set(allowed_actions) - set([-1]))

        # find the total of maximum time taken by hard tasks to complete
        state = state[0]
        # debug
        #state = [[[(0, 1.0)], 0, [(1, 0.4), (2, 0.6)]], [[(2, 0.7), (3, 0.3)], 3, [(5, 1.0)]],
         #[[(2, 1.0)], 4, [(6, 0.1), (7, 0.9)]], [[(2, 0.8), (3, 0.2)], 6, [(7, 1.0)]]]

        len_hard_tasks = len(self.hard_tasks)
        total_time_tasks = 0
        for i in range(len_hard_tasks):
            total_time_tasks += state[i][0][-1][0]

        # for each soft task find if it can be finished: give priority to tasks with high cost
        #len_soft_tasks = len(self.soft_tasks)
        state = state[len_hard_tasks:]
        total_cost = 0
        for i in self.soft_tasks_ind_sorted_cost:
            dl_remaining = state[i][1] - total_time_tasks
            if state[i][0][-1][0] > dl_remaining:
                total_cost += self.costs[i]
            else:
                total_time_tasks += state[i][0][-1][0]

        return -1*total_cost