touch resultsTACAS_simplesoft2.txt

counter=10000
while [ $counter -ge 1000 ]
do
	echo $counter >> resultsTACAS_simplesoft2.txt
	for i in {1..10}
	do
		echo $i
		printf "\r\n" >> resultsTACAS_simplesoft2.txt
		python3 trainTasks.py simplesoft2.txt $counter >> resultsTACAS_simplesoft2.txt
		echo "------------------------------------------" >> resultsTACAS_simplesoft2.txt
	done
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_simplesoft2.txt
	((counter-=1000))
done

echo "===============================" >> resultsTACAS_simplesoft2.txt

counter=5000
num=2
while [ $counter -ge 500 ]
do
	steps=$((num * counter))
	txtfile="simplesoft2model${steps}.txt"
	pmfile="simplesoft2model${steps}.pm"
	echo $steps $txtfile $pmfile
	echo $steps >> resultsTACAS_simplesoft2.txt
	for i in {1..10}
	do
		echo $i
		python2.7 taskModelBased.py simplesoft2.txt $counter
		python2.7 task.py $txtfile
		printf "\r\n" >> resultsTACAS_simplesoft2.txt
		storm --prism $pmfile --prop "Rmin=?[LRA]" >> resultsTACAS_simplesoft2.txt
		rm $txtfile $pmfile
	done
	((counter-=500))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_simplesoft2.txt
done
