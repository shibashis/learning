touch resultsTACAS_simplesoft2.txt

counter=450
num=2
while [ $counter -ge 50 ]
do
	steps=$((num * counter))
	txtfile="simplesoft2model${steps}.txt"
	pmfile="simplesoft2model${steps}.pm"
	echo $steps $txtfile $pmfile
	echo $steps >> resultsTACAS_simplesoft2.txt
	for i in {1..10}
	do
		echo $i
		python2.7 taskModelBased.py simplesoft2.txt $counter
		python2.7 task.py $txtfile
		printf "\r\n" >> resultsTACAS_simplesoft2.txt
		storm --prism $pmfile --prop "Rmin=?[LRA]" >> resultsTACAS_simplesoft2.txt
		rm $txtfile $pmfile
	done
	((counter-=50))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_simplesoft2.txt
done
