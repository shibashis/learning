import numpy as np
import sys
import random
from six import StringIO, b

import networkx as nx
import time
from fractions import Fraction
from decimal import Decimal

from gym import Env, spaces, utils
from gym.utils import seeding


def get_tasks(file_name):
    f=open(file_name, "r")

    #readlines reads the individual line into a list
    fl =f.readlines()
    lines=[]
    tasks= []
    hard_tasks= []
    soft_tasks= []
    has_soft_tasks = False

    f.close()

    # for each line, extract the task parameters

    for x in fl:
        y = x.strip()

        if y[0] == '-':
            hard_tasks = tasks
            tasks = []
            has_soft_tasks = True

        elif y!='' and y[0]!='#':
            lines.append(y)
            task = y.split("|")  # task should have 4 elements

            arrival = int(task[0])

            dist = task[1].split(";")  # distribution on the execution time
            exe = []
            max_exe_time = 0
            for z in dist:
                z = z.strip("[")
                z = z.strip("]")
                z = z.split(",")
                time = int(z[0])

                if time > max_exe_time:  # compute maximum execution time
                    max_exe_time = time

                # change to fraction since float arithmetic produces bad precision
                #prob = float(z[1])
                #exe.append((time, prob))
                exe.append((time, Fraction(Decimal(z[1]))))

            deadline = int(task[2])

            dist = task[3].split(";")  # distribution on the period
            period = []
            arrive_time = []
            for z in dist:
                z = z.strip("[")
                z = z.strip("]")
                z = z.split(",")
                time = int(z[0])

                # change to fraction since float arithmetic produces bad precision
                #prob = float(z[1])
                #period.append((time, prob))
                period.append((time, Fraction(Decimal(z[1]))))

                arrive_time.append(time)

            min_arrive_time = min(arrive_time)

            if has_soft_tasks:
                cost = Fraction(Decimal(task[4]))
                tasks.append([arrival, exe, deadline, period, max_exe_time, min_arrive_time, cost])
            else:
                tasks.append([arrival, exe, deadline, period, max_exe_time, min_arrive_time])

    if has_soft_tasks:
        soft_tasks = tasks
    else:
        hard_tasks = tasks

    #print('num hard tasks: ' + str(len(hard_tasks)))
    #print('num soft tasks: ' + str(len(soft_tasks)))

    return hard_tasks, soft_tasks



def get_num_tasks(hard_tasks, soft_tasks):

    return len(hard_tasks) + len(soft_tasks)



def get_upper_bound_num_states(hard_tasks, soft_tasks):

    # We assume that for every task the  number of states S_i of task i is bounded above by
    # max of arrival time dist + (max of arrival time dist - min of arrival time dist) x num of elements in the arrival time dist
    # + (max of exe time dist - min of exe time dist) x num of elements in the exe time dist
    #
    # The total number of states is bounded above by the product of S_i over all tasks i.

    tasks = hard_tasks + soft_tasks
    num_states = 1

    for x in tasks:
        exe_dist = x[1]
        exe_times = [i[0] for i in exe_dist]
        max_exe_time = max(exe_times)
        min_exe_time = min(exe_times)

        arr_dist = x[3]
        arr_times = [i[0] for i in arr_dist]
        max_arr_time = max(arr_times)
        min_arr_time = min(arr_times)

        #num_states = num_states * (max_arr_time + (max_arr_time - min_arr_time) * len(arr_dist) +  \
        #             (max_exe_time - min_exe_time) * len(exe_dist))

        num_states = num_states * max_arr_time * max_exe_time * len(arr_times) * len(exe_times)

    return num_states



def get_init_state(hard_tasks, soft_tasks):
    # add states representing individual tasks

    tasks = hard_tasks + soft_tasks
    # extract the parameters for each task
    init_state = []
    for x in tasks:
        if x[0] > 0:  # initial arrival time > 0
            #task = [0, 0, x[0]]
            # change to fraction
            #task = [[(0,1)], 0, [(x[0],1)]]
            task = [[(0,Fraction(1,1))], 0, [(x[0],Fraction(1,1))]]

        else:
            task = [x[1], x[2], x[3]]  # [RCT dist, deadline, Period dist]

        init_state.append(task)

    # We assume that initially all tasks are safe
    return init_state, [0] * len(tasks)  # [0] denotes bad_flag



def dec_dist(dist):
    new_dist = []
    for x in dist:
        new_dist.append((x[0]-1, x[1]))

    return new_dist



def modify_params(rct_dist, curr_deadline, iat_dist):
    dec_rct_dist = dec_dist(rct_dist)
    dec_iat_dist = dec_dist(iat_dist)

    if curr_deadline > 0:  # check if deadline is greater than 0
        deadline = curr_deadline - 1
    else:
        deadline = curr_deadline

    return dec_rct_dist, deadline, dec_iat_dist



def normalize_dist(dist):
    # dist is a list with elements of the form (time, prob)
    prob_dist = [x[1] for x in dist]
    sum_dist = sum(prob_dist)

    dist = [(x[0], 1 / sum_dist * x[1]) for x in dist]  # float is not needed since we use fraction
    #dist = [(x[0], float(1/sum_dist) * x[1]) for x in dist]
    # dist = [(x[0], float(x[1]/sum_dist)) for x in dist] # This expression is correct but gives imprecise floating point numbers
    return dist



def add_possible_states_task_j(rct_dist, iat_dist, init_config_j, deadline, dec_iat_dist):

    possible_states_task_j = []

    if iat_dist[0][0] > 1:
        possible_states_task_j.append((1, [rct_dist, deadline, dec_iat_dist], 0))

    elif iat_dist[0][0] == 1:
        # if hard:
            # if deadline >= rct_dist[-1][0]:  # deadline >= max rct
                # possible_states_task_j.append((iat_dist[0][1], init_config_j, 0))  # initial state
        # else:
        bad_flag = 0
        if rct_dist[0][0] > 0:
            bad_flag = 1
        possible_states_task_j.append((iat_dist[0][1], init_config_j, bad_flag))  # initial state

        if len(iat_dist) > 1:
            possible_states_task_j.append((1-iat_dist[0][1], [rct_dist, deadline, dec_iat_dist[1:]], 0))

    return possible_states_task_j



# Right now, we don't use this function
def safe(task):
    # task is an element of the form [rct, deadline, iat], where rct is of the form [(t1, p1), ..., (tn, pn)]
    # check if deadline >= max rct
    max_rct = task[0][-1][0]
    deadline = task[1]
    if max_rct > deadline:
        return False

    else:
        return True



def compute_states(raw_states):
    # raw_states is a list where each element is also a list that corresponds to each task and has elements (prob, [rct, deadline, iat], bad_flag)
    # it returns states where each element is of the form (prob, [[rct1, deadline1, iat1], [rct2, deadline2, iat2], ... [rctn, deadlinen, iatn]], [bad_flag1, bad_flag2, ... bad_flagn]),
    # that is the set of future states
    # we compute the probabilities of each state, denoted by prob and bad_array_i in state i is an array with an element
    # for each task; the element is 1 if the task does not meet its deadline, else it is 0

    # hard = True
    # if index > num_hard_tasks:
        # hard = False
    states = []
    if len(raw_states)==1:
        for x in raw_states[0]:
            # if hard:
                # if safe(x[1]):
                # states.append((x[0], [x[1]], [0]))
            # else:
            states.append((x[0], [x[1]], [x[2]]))

        return states

    else:
        # recursive call
        subsets = compute_states(raw_states[1:])
        for x in raw_states[0]:
            # if hard:
                # if safe(x[1]):
                # for y in subsets:
                    # states.append((x[0]*y[0], [x[1]] + y[1], [0] + y[2]))

            # else:  # this will never be executed since we do not compute the product of the soft tasks together
            for y in subsets:
                states.append((x[0] * y[0], [x[1]] + y[1], [x[2]] + y[2]))

        return states



def get_states_execute_i(all_tasks, i, tasks):
    # all_tasks is a list of [rct dist, deadline, iat dist] elements, one for each task. It denotes the current state
    # i is the task that will be executed (start index is 0)
    # tasks is a list of the task descriptions [arrival, exe dist, deadline, period dist, max_exe_time, min_arrive_time]

    # This function returns a tuple (i, states), where states is the set of all states obtained after executing task i,
    # and the second member of the tuple denotes that task i has been executed to reach these states
    # each element of states is a state where each state is of the form
    # (prob, [[rct1, deadline1, iat1], [rct2, deadline2, iat2], ... [rctn, deadlinen, iatn]], [bad_flag1, bad_flag2, ... bad_flagn]),

    raw_states = []
    # hard = True

    for j in range(len(all_tasks)):

        # if j == num_hard_tasks:
            # hard = False

        task = all_tasks[j]
        init_config_j = tasks[j][1:4]  # gives rct, deadline, iat in the current config

        # normalize distributions so that the probabilities sum up to 1
        rct_dist = normalize_dist(task[0])
        iat_dist = normalize_dist(task[2])
        possible_states_task_j = []

        # reduce times by 1 due to one clock tick
        dec_rct_dist, deadline, dec_iat_dist = modify_params(rct_dist, task[1], iat_dist)

        # compute remaining computation time distribution for task j that is scheduled
        if j == i:
            if (rct_dist[0][0] > 1 or len(rct_dist) == 1) and iat_dist[0][0] > 1:
                # only one successor for task j
                # the first parameter is the probability that task j evolves to this state
                # the probability is 1 since there is only one successor
                # the last parameter is a bad flag, it is 1 when a new task comes but the previous one is not finished
                possible_states_task_j.append((1, [dec_rct_dist, deadline, dec_iat_dist], 0))

            elif iat_dist[0][0] == 1 and len(iat_dist) == 1:
                # if not hard:
                # task executes and remaining computation time > 1
                if rct_dist[0][0] > 1:
                    possible_states_task_j.append((1, init_config_j, 1))  # initial state

                else:  # rct_dist[0][0] == 1
                    possible_states_task_j.append((rct_dist[0][1], init_config_j, 0))

                    if len(rct_dist) > 1:  # task not getting finished with probability 1 - rct_dist[0][1]
                        possible_states_task_j.append(((1 - rct_dist[0][1]), init_config_j, 1))

                # else:  # hard task
                    # one successor that is the initial config for task j
                    # the probability is 1 since there is only one successor
                    # possible_states_task_j.append((1, init_config_j, 0))  # initial state

            else:
                # we reach here if
                # min RCT=1 and len(RCT) > 1 and len(iat) >= 1 (min(iat) > 1 when len(iat)=1) or
                # min(RCT) > 1 and len(iat) > 1 and min(iat) = 1
                if iat_dist[0][0] == 1:  # one of the successor states is the initial state of task i=j
                    # len(iat) > 1 here
                    if rct_dist[0][0] == 1 and len(rct_dist) > 1:
                        possible_states_task_j.append((rct_dist[0][1] * iat_dist[0][1], init_config_j, 0))  # initial state with task finished
                        # if hard:
                            # possible_states_task_j.append(((1 - rct_dist[0][1]) * iat_dist[0][1], init_config_j, 0))
                        # else:
                        possible_states_task_j.append(((1 - rct_dist[0][1]) * iat_dist[0][1], init_config_j, 1))  # initial state with task not finished
                        possible_states_task_j.append((rct_dist[0][1] * (1 - iat_dist[0][1]), [[dec_rct_dist[0]], deadline, dec_iat_dist[1:]], 0))
                        possible_states_task_j.append(((1-rct_dist[0][1]) * (1-iat_dist[0][1]), [dec_rct_dist[1, :], deadline, dec_iat_dist[1:]], 0))

                    elif rct_dist[0][0] == 1:  # and len(rct_dist) == 1
                        possible_states_task_j.append((iat_dist[0][1], init_config_j, 0))
                        possible_states_task_j.append((1-iat_dist[0][1], [dec_rct_dist, deadline, dec_iat_dist[1:]], 0))

                    else:  # rct_dist[0][0] > 1
                        # if hard:
                            # possible_states_task_j.append((iat_dist[0][1], init_config_j, 0))
                        # else:
                        possible_states_task_j.append((iat_dist[0][1], init_config_j, 1))
                        possible_states_task_j.append((1-iat_dist[0][1], [dec_rct_dist, deadline, dec_iat_dist[1:]], 0))

                else:  # min(iat) > 1
                    # Here min RCT=1 and len(RCT) > 1
                    possible_states_task_j.append((rct_dist[0][1], [[dec_rct_dist[0]], deadline, dec_iat_dist], 0))
                    possible_states_task_j.append((1-rct_dist[0][1], [dec_rct_dist[1:], deadline, dec_iat_dist], 0))

        else:  # when j!=i
            possible_states_task_j = add_possible_states_task_j(rct_dist, iat_dist, init_config_j, deadline,
                                                                dec_iat_dist)
        raw_states.append(possible_states_task_j)

    # states is a list of the form [(prob1, state1), (prob2,state2), ... (probn,staten)]
    # states = compute_states(raw_states, 1, num_hard_tasks)
    states = compute_states(raw_states)
    len_tasks = len(tasks)
    #states = filter(lambda (prob, next, bad_flag): len(next)==len_tasks, states)  # lambda with tuples does not work with Python3

    states_next = [x[1] for x in states]

    st = []
    for k in range(len(states_next)):
        if len(states_next[k])==len_tasks:
           st.append(states[k])

    # filters only valid states: the ones that have n tasks

    return i, st  # returns a tuple of the form (i, states)



def get_states_execute_none(all_tasks, tasks):
    # This function returns a tuple (-1, states), where states is the set of all states obtained from the current state
    # all_tasks when no task is executed. The slacks can be used by soft tasks

    raw_states = []
    # hard = True

    for j in range(len(all_tasks)):

        # if j == num_hard_tasks:
            # hard = False

        task = all_tasks[j]
        init_config_j = tasks[j][1:4]  # gives rct, deadline, iat in the current config

        # normalize distributions so that the probabilities sum up to 1
        rct_dist = normalize_dist(task[0])
        iat_dist = normalize_dist(task[2])

        dec_rct_dist, deadline, dec_iat_dist = modify_params(rct_dist, task[1], iat_dist)

        possible_states_task_j = add_possible_states_task_j(rct_dist, iat_dist, init_config_j, deadline, dec_iat_dist)
        raw_states.append(possible_states_task_j)

    # states = compute_states(raw_states, 1, num_hard_tasks)
    states = compute_states(raw_states)
    len_tasks = len(tasks)
    # states = filter(lambda (prob, next, bad_flag): len(next)==len_tasks, states) # lambda with tuples does not work with Python3

    states_next = [x[1] for x in states]
    st = []
    for k in range(len(states_next)):
        if len(states_next[k])==len_tasks:
           st.append(states[k])

    return -1, st  # -1 denotes no task has been executed



def choose_state(next_states_execute_i):
    # next_states_execute_i is a list of states of the form [(prob1, state1, exec1), ... (probm, statem, bad_flag_listm)]
    # where m is the number of states that are obtained by executing task i. Here i can be -1 also denoting no task scheduled
    probs = [x[0] for x in next_states_execute_i]
    cum_probs = np.cumsum(probs)  # cumulative sum of probabilities

    random_prob = random.uniform(0, 1)
    # From next_states choose the state whose index i is such that probs[i-1] <= random_prob <= probs[i]
    ind_list = [i for i in range(len(cum_probs)) if cum_probs[i] >= random_prob]

    return next_states_execute_i[ind_list[0]]



def normalize_state(state):
    # normalize the distributions of all tasks in a state
    norm_state = []

    for task in state:
        [rct_dist, deadline, iat_dist] = task
        mod_task = [normalize_dist(rct_dist), deadline, normalize_dist(iat_dist)]
        norm_state.append(mod_task)

    return norm_state



def get_next_state(all_tasks, i, tasks):  # all_tasks is the current State, and task i is executed.
    # all_tasks is a list of [rct dist, deadline, iat dist] elements, one for each task. It denotes the current state
    # i is the task to be executed
    # tasks is a list of the task descriptions [arrival, exe dist, deadline, period dist, max_exe_time, min_arrive_time]

    next_states = []
    if i >=0:
        task_i_rct_dist = all_tasks[i][0]
        task_i_deadline = all_tasks[i][1]

        if task_i_rct_dist[0][0] > 0 and task_i_deadline > 0:
            next_states.append(get_states_execute_i(all_tasks, i, tasks))

        else: # either execution has finished or deadline is over
            # Implement a shield: perform nop
            next_states.append((get_states_execute_none(all_tasks, tasks)))

    else:
        next_states.append((get_states_execute_none(all_tasks, tasks)))
    # next_states is of the form [(i,states)]; this is a list of only one element
    # next_states[0][1] is states
    next_state = choose_state(next_states[0][1]) # returns a tuple of the form (prob, state, bad_flag_list)

    # state is a list where each element is of the form [[rct dist], deadline, [iat_dist]]
    return normalize_state(next_state[1]), next_state[2]  # returns a tuple of the form (state, bad_flag_list)



def get_observation(next_state, tasks, feature_len):
    # This function returns the observation obs where obs is a list of tuples where each tuple corresponds to a task j
    # and is of the form (max(rct_supp) for task j, remaining time before deadline for task j, min(rct_supp) for task j)

    obs = []

    for j in range(len(tasks)):

        # max_rct_supp_taskj_next_state = max([x[0] for x in next_state[0][j][0]])
        deadline_taskj = next_state[0][j][1]
        # min_iat_supp_taskj_next_state = min([x[0] for x in next_state[0][j][2]])

        obs = obs + [deadline_taskj]

    #obs = tuple(obs)

    # debug
    if len(obs) != feature_len:
        print('error')

    return np.array(obs)



def get_observation_space(tasks, len_hard_tasks):

    feature_list_high = []

    for j in range(len(tasks)):

        if j >= len_hard_tasks:
            # for soft tasks
            [arrival, exe, deadline, period, max_exe_time, min_arrive_time, cost] = tasks[j]
        else:
            [arrival, exe, deadline, period, max_exe_time, min_arrive_time] = tasks[j]

        feature_list_high = feature_list_high + [deadline]

    # debug
    print(len(feature_list_high))

    feature_list_low = [0] * len(tasks)
    return len(feature_list_high), spaces.Box(np.array(feature_list_low), np.array(feature_list_high), dtype=np.int8)



def hard_task_deadline_miss(state_w_bad_flag_list, len_hard_tasks):
    # state_w_bad_flag_list is a tuple of the form (state, bad_flag_list)
    bad_flag_list = state_w_bad_flag_list[-1]
    bad_flag_list = bad_flag_list[0:len_hard_tasks]

    if sum(bad_flag_list) > 0:
        return True
    else:
        return False



def get_reward(state_w_bad_flag_list, soft_tasks):
    # state_w_bad_flag_list is a tuple of the form (state, bad_flag_list)
    # This function returns the cost if a soft task does not finish
    cost_list = [task[-1] for task in soft_tasks]
    bad_flag_list = state_w_bad_flag_list[-1]
    bad_flag_list = bad_flag_list[len(bad_flag_list)-len(soft_tasks):]

    return sum([a*b for a,b in zip(cost_list, bad_flag_list)])



def get_succ_states(all_tasks, i, tasks):
    # all_tasks is a list of [rct dist, deadline, iat dist] elements, one for each task. It denotes the current state
    # i is the task to be executed
    # tasks is a list of the task descriptions [arrival, exe dist, deadline, period dist, max_exe_time, min_arrive_time]

    #next_states = []
    if i >= 0:
        task_i_rct_dist = all_tasks[i][0]
        task_i_deadline = all_tasks[i][1]

        if task_i_rct_dist[0][0] > 0 and task_i_deadline > 0:
            next_states = get_states_execute_i(all_tasks, i, tasks)

        else:  # either execution has finished or deadline is over
            # Implement a shield: perform nop
            next_states = get_states_execute_none(all_tasks, tasks)

    else:
        next_states = get_states_execute_none(all_tasks, tasks)
    # next_states is of the form (i,states);
    return next_states[1]



# pop the first element of queue
def pop_queue(queue):
    return (queue[0], queue[1:])



class TaskSystemEnv(Env):

    def __init__(self, file_name):
        # create the task parameters
        self.hard_tasks, self.soft_tasks = get_tasks(file_name)

        self.action_space = spaces.Discrete(get_num_tasks(self.hard_tasks, self.soft_tasks) + 1)  # one more action for nop
        self.feature_len, self.observation_space = get_observation_space(self.hard_tasks + self.soft_tasks, len(self.hard_tasks))

        #self.hard_miss = False  # set to True if a hard task misses its deadline
        self.num_hard_miss = 0

        self.seed()
        self.reset()


    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]



    def get_deadline_misses(self):
        return self.num_hard_miss


    def reset(self):
        self.currentState = get_init_state(self.hard_tasks, self.soft_tasks)
        self.last_action=None
        #self.hard_miss = False
        self.num_hard_miss = 0
        return get_observation(self.currentState, self.hard_tasks + self.soft_tasks, self.feature_len)


    def step(self, a):
        # Check: very important: What if a is a task which cannot be executed?
        # Below we assume that a can be executed, that is, there is still some remaining execution time and remaining deadline

        # execute the action a and return the next state
        # next_state is a tuple of the form (state, bad_flag_list)
        next_state = get_next_state(self.currentState[0], a-1, self.hard_tasks + self.soft_tasks)  # self.currentState[1] contains the bad flag.

        obs = get_observation(next_state, self.hard_tasks + self.soft_tasks, self.feature_len)

        hard_miss_reward = 10000 # This is a high cost; once a hard task misses deadline,
        # this high cost will always be returned until the reset function is called

        #self.hard_miss = False

        if hard_task_deadline_miss(next_state, len(self.hard_tasks)):
            #self.hard_miss = True
            #print('Hard task missed deadline.')

        #if self.hard_miss:
            reward = hard_miss_reward
            self.num_hard_miss += 1
        else:
            reward = get_reward(next_state, self.soft_tasks)

        self.currentState = next_state
        self.last_action = a  # The actual action is a-1

        return (obs, -1 * reward, False, {})

        #transitions = self.P[self.s][a]
        #i = categorical_sample([t[0] for t in transitions], self.np_random)
        #p, s, r, d= transitions[i]
        #self.s = s
        #self.last_action=a
        #return (s, r, d, {"prob" : p})


    def check_safety(self, act):
        # Do a breadth first search
        self.reset()
        queue = [self.currentState]
        visited = []
        len_hard_tasks = len(self.hard_tasks)

        while len(queue) > 0:
            (state, queue) = pop_queue(queue)

            if hard_task_deadline_miss(state, len_hard_tasks):
                print('Unsafe task: ')
                break

            if not state in visited:
                obs = get_observation(state, self.hard_tasks + self.soft_tasks, self.feature_len)
                action = act(np.array(obs)[None], update_eps=0)[0]
                succ_states = get_succ_states(state[0], action-1, self.hard_tasks + self.soft_tasks)  # successor states for action action
                next_states = [tuple(x[1:]) for x in succ_states]
                queue = queue + next_states
                visited.append(state)