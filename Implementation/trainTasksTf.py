# This works with Python 3.6.5_1, but TensorFlow gives an error with Python 3.7
import gym
import pickle
import numpy as np
import time
import sys
import random

#from baselines import deepq
from gym.envs.registration import register

from enum import Enum
import tensorflow as tf
#import pandas as pd
#import matplotlib.pyplot as plt

from tf_agents.environments import py_environment
from tf_agents.environments import tf_environment
from tf_agents.environments import tf_py_environment
from tf_agents.environments import suite_gym
from tf_agents.environments import utils
from tf_agents.specs import array_spec
from tf_agents.trajectories import time_step as timeStep

from tf_agents.policies import random_tf_policy

from tf_agents.metrics import tf_py_metric
from tf_agents.metrics import tf_metrics
from tf_agents.metrics import py_metric
from tf_agents.drivers import py_driver
from tf_agents.drivers import dynamic_episode_driver

from tf_agents.agents.dqn import dqn_agent
from tf_agents.drivers import dynamic_step_driver
from tf_agents.networks import q_network
from tf_agents.replay_buffers import tf_uniform_replay_buffer

from tf_agents.policies import random_tf_policy
from tf_agents.trajectories import trajectory
from tf_agents.utils import common

from tf_agents.policies import policy_saver

def callback(lcl, _glb):
    # stop training if reward exceeds 199
    is_solved = lcl['t'] > 100 and sum(lcl['episode_rewards'][-101:-1]) / 100 >= 199
    return is_solved


def main():

    # file_name = input("Enter file name: ")
    file_name = sys.argv[1]
    file_name = file_name.strip()

    register(
        id='learnTasks-v0',
        #entry_point='learnTasks:TaskSystemEnv', kwargs={'file_name':file_name},  # observation is exact state space
        #entry_point='learnTasks_safe:TaskSystemEnv', kwargs={'file_name':file_name}, # observation is exact state space
        #entry_point='learnTasksAbs:TaskSystemEnv', kwargs={'file_name': file_name}, # observation: max remaining exe time, deadline, min remaining arrival time
        #entry_point='learnTasks_safeAbs:TaskSystemEnv', kwargs={'file_name': file_name},
        #entry_point='learnTasksDeadline:TaskSystemEnv', kwargs={'file_name': file_name},  # observation: per task remaining time before deadline
        #entry_point='learnTasks_safeDeadline:TaskSystemEnv', kwargs={'file_name': file_name},
        #entry_point='learnTasksSortActiveDL:TaskSystemEnv', kwargs={'file_name': file_name},
        entry_point='learnTasksED:TaskSystemEnv', kwargs={'file_name': file_name}, # observation: max remaining exe time, deadline
        #entry_point='learnTasksED_debug:TaskSystemEnv', kwargs={'file_name': file_name}, # observation: max remaining exe time, deadline
        #entry_point='learnTasks_safeED:TaskSystemEnv', kwargs={'file_name': file_name},
        #entry_point='learnTasks_safeED_AbsSynthe:TaskSystemEnv', kwargs={'file_name': file_name}, # observation: max remaining exe time, deadline
        #entry_point='learnTasksAbs_lax:TaskSystemEnv', kwargs={'file_name': file_name}, # observation: max(0,laxity with max rct) per task
        #entry_point='learnTasksSortActiveLax:TaskSystemEnv', kwargs={'file_name': file_name}, # observation: active tasks sorted on laxity
        #entry_point='learnTasksEDFshield:TaskSystemEnv', kwargs={'file_name': file_name}, # EDF as shield, observation: max remaining exe time, deadline, min remaining arrival time
    )

    #env_name = gym.make("learnTasks-v0")
    env_name = 'learnTasks-v0'

    train_env = tf_py_environment.TFPyEnvironment(suite_gym.load(env_name))
    eval_env = tf_py_environment.TFPyEnvironment(suite_gym.load(env_name))

    fc_layer_params = [32, 64, 128]

    q_net = q_network.QNetwork(
        train_env.observation_spec(),
        train_env.action_spec(),
        fc_layer_params=fc_layer_params
    )

    train_step = tf.Variable(0)
    learning_rate = 1e-3

    update_period = 4
    #optimizer = tf.keras.optimizers.Adam(lr=2.5e-4, epsilon=0.00001)
    optimizer = tf.compat.v1.train.AdamOptimizer(learning_rate=learning_rate)

    epsilon_fn = tf.keras.optimizers.schedules.PolynomialDecay(
        initial_learning_rate=1.0,
        decay_steps=250000 // update_period,
        end_learning_rate=0.01)

    agent = dqn_agent.DqnAgent(
        train_env.time_step_spec(),
        train_env.action_spec(),
        q_network=q_net,
        optimizer=optimizer,
        td_errors_loss_fn=common.element_wise_squared_loss,
        train_step_counter=train_step)

    agent = dqn_agent.DdqnAgent(
        train_env.time_step_spec(),
        train_env.action_spec(),
        q_network=q_net,
        optimizer=optimizer,
        target_update_period=2000,
        td_errors_loss_fn=tf.keras.losses.Huber(reduction="none"),
        gamma=1,
        train_step_counter=train_step,
        epsilon_greedy=lambda:epsilon_fn(train_step))

    agent.initialize()

    replay_buffer = tf_uniform_replay_buffer.TFUniformReplayBuffer(
        data_spec=agent.collect_data_spec,
        batch_size=train_env.batch_size,
        max_length=1000000)

    replay_buffer_observer = replay_buffer.add_batch

    train_metrics = [tf_metrics.AverageReturnMetric(), tf_metrics.AverageEpisodeLengthMetric()]

    collect_driver = dynamic_step_driver.DynamicStepDriver(
        train_env,
        agent.collect_policy,
        observers=[replay_buffer_observer] + train_metrics,
        num_steps=update_period)

    class ShowProgress:
        def __init__(self, total):
            self.counter = 0
            self.total = total

        def __call__(self, trajectory):
            if not trajectory.is_boundary():
                self.counter += 1
            if self.counter % 100 == 0:
                print("\r{}/{}".format(self.counter, self.total), end="")

    initial_collect_policy = random_tf_policy.RandomTFPolicy(train_env.time_step_spec(), train_env.action_spec())

    init_driver = dynamic_step_driver.DynamicStepDriver(
        train_env,
        initial_collect_policy,
        observers=[replay_buffer.add_batch, ShowProgress(20000)],
        num_steps=20000)

    final_time_step, final_policy_state = init_driver.run()

    dataset = replay_buffer.as_dataset(sample_batch_size=64, num_steps=2, num_parallel_calls=3).prefetch(3)

    collect_driver.run = common.function(collect_driver.run)
    agent.train = common.function(agent.train)

    all_train_loss = []
    all_metrics = []

    def train_agent(n_iterations):
        time_step = None
        policy_state = agent.collect_policy.get_initial_state(train_env.batch_size)
        iterator = iter(dataset)

        for iteration in range(n_iterations):
            current_metrics = []

            time_step, policy_state = collect_driver.run(time_step, policy_state)
            trajectories, buffer_info = next(iterator)

            train_loss = agent.train(trajectories)
            all_train_loss.append(train_loss.loss.numpy())

            for i in range(len(train_metrics)):
                current_metrics.append(train_metrics[i].result().numpy())

            all_metrics.append(current_metrics)

            if iteration % 500 == 0:
                print("\nIteration: {}, loss:{:.2f}".format(iteration, train_loss.loss.numpy()))

                for i in range(len(train_metrics)):
                    print('{}: {}'.format(train_metrics[i].name, train_metrics[i].result().numpy()))

    train_agent(15000)

    def compute_avg_return(environment, policy, time_step, num_episodes=10):

        total_return = 0.0
        num_steps = 15000

        for _ in range(num_episodes):
            #time_step = environment.reset()
            episode_return = 0.0

            while _ in range(num_steps):
                action_step = policy.action(time_step)
                time_step = environment.step(action_step.action)
                episode_return += time_step.reward

            total_return += episode_return

        avg_return = -total_return / (num_episodes * num_steps)

        return avg_return.numpy()[0]

    # Reset the train step
    agent.train_step_counter.assign(0)

    # reset eval environment
    eval_env.reset()

    # Evaluate the agent's policy once before training.
    time_step = eval_env.reset()
    avg_return = compute_avg_return(eval_env, agent.policy, time_step, 1)

if __name__ == '__main__':
    main()
