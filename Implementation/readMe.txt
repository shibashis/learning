Installation:

This is specified for Ubuntu. Pip should be installed.
Install pip by using sudo apt install python3-pip if pip is not already installed.

We would need both Python2.7 and Python3.6 or python3.7.

Software requirements:
1. OpenAI baselines (Only for python3)
2. networkx (for both python2.7 and python3)

Go to the OpenAI baselines page (https://github.com/openai/baselines) and see the installation procedure that is the following:

This requires python3

For Linux

1. sudo apt-get update && sudo apt-get install cmake libopenmpi-dev python3-dev zlib1g-dev
(May need to uninstall current cloudpickle and then install version 1.2.0 by python3 -m pip install cloudpickle==1.2.0 if not done automatically.)

2. Clone repo:
git clone https://github.com/openai/baselines.git
cd baselines

3. python3 -m pip install tensorflow==1.14

4. python3 -m pip install -e .

----------------------------------------------------------

For Mac

1. brew install cmake openmpi

2. git clone https://github.com/openai/baselines.git
cd baselines

3. python3 -m pip install tensorflow==1.14 
(Please not that python3.8 uses tensorflow2.2 which is incompatible with openAI baselines. The tf2 branch of openAI baselines does not work for all act functions like act.save() and act() to get the environment action.)

4. python3 -m pip install -e .


----------------------------------------------------------

5. install networkx (install using pip)

6. Install AbsSynthe
git clone https://github.com/gaperez64/AbsSynthe/
cd AbsSynthe
./build.sh

7. Install task2aig
git clone https://github.com/gaperez64/task2aig.git

cd task2aig/aiger/
(The Makefile in aiger folder might need to be changed to use python3 instead of python in the line python setup.py ... )
make all
(For the error message "fatal error: Python.h: No such file or directory" do sudo apt install libpython3.x-dev)
(Also install swig if not already installed by sudo apt install swig.)

cd ..
make all

8. Now cd to the Implementation folder inside learning and copy

AbsSynthe/binary/abssynthe
task2aig/encodeTasks.py
task2aig/decodeSafe.py
task2aig/aigprod
task2aig/task2aig
Task2aig/aiger

to the Implementation folder. The first 5 are files while the last one is a directory.

For model-based learning, we have been using python2.7. We need to install networkx.

1. install networkx (install using pip; This will install networkx2.2 which is the latest version compatible with python2.7)


Q. Which programs run with python3 and which programs run with python2?

trainTasks.py, txt2csv.py, and all those that use openAI gym package run with python3.

The files for model-based learning, e.g., task.py runs with python2.7
For random safe simulation: python2.7 task_random_safe_simulate.py onlysoft10.txt

The random safe simulation with AbsSynthe however uses python3: python3 task_random_safe_simulateAbsSynthe_EDF.py soft12.txt 

Q. What is the format for writing the task systems?

Below is the content from soft2.txt.
The lines with # are comment lines.
The lines above --------------------- correspond to hard tasks, while the lines below correspond to the soft tasks.

Here there are one hard task, and two soft tasks.

first_arrival denotes the time of arrival of the first job of the task.
The supports of all distributions should be in an increasing order. In the example, for the hard task has an execution (computation) time 1 with probability 0.2 while the execution time is 2 with probability 0.8. The supports of the distribution are 1 and 2.
The deadline for the hard task is 5, and the inter-arrival time distribution is 7 with probability 0.4 and 8 with probability 0.6.

# first_arrival|exe_time_dist|deadline|period_dist
0|[1,.2;2,.8]|5|[7,.4;8,.6]
---------------------
# soft tasks
# first_arrival|exe_time_dist|deadline|period_dist|cost
0|[2,.7;3,.3]|3|[5,1]|3
1|[2,1]|6|[8,.1;9,.9]|7


Q. How to run the scripts for model-free learning?

The top level python file is called trainTasks.py. It can be run using python3. Depending on the version of python3, there may be some warning messages from Tensorflow that can be ignored.

Run this file as
python trainTasks.py <input_task_system> <num_training_steps>

Here is an example:
python trainTasks.py onlysoft4.txt 4000

A policy (same as schedule or strategy) is learnt for 4000 training steps, and the output is obtained as taking the mean cost of a simulation of the learnt policy over 10000 steps.

We now list the different kinds of model-free learning as reported in the paper.

The specific kind of model-free learning is done by setting the correct entry_point in trainTasks.py.
There should be only one entry_point uncommented in trainTasks.py.

Training an subsequent simulation of system without shielding: 
Applicable for systems with only soft tasks; 
for systems with hard tasks, a high penalty is applied if a high cost is incurred if the deadline of a job of a hard task is missed.

Input to neural network:

1. (NN) the maximum among the remaining computation time, the time before deadline,  and  the  minimum  among  the  remaining  time  before  arrival  of  the next job of the same task: 
The entry_point is learnTasksAbs.

2. (NN-ED) the maximum among the remaining computation time and the time before deadline:
The entry_point is learnTasksED.

3. (NN-DL) remaining time before deadline: The entry point is learnTasksDeadline.

4. (NN-lax) difference between the remaining time before deadline and the maximum remaining computation time: The entry point is learnTasksAbs_lax.

---------------------------------------------------------------------------

For model-free learning with task systems having hard tasks: Training with shield, and execution with shielding.

We consider the following entry points:

1. (NN): learnTasks_safeAbs

2. (NN-ED): learnTasks_safeED or learnTasks_safeED_AbsSynthe
learnTasks_safeED_AbsSynthe uses the AbsSynthe to compute the most general safe strategies and is faster.

We have shell scripts to run batches of experiments. One such example is soft3_3E_D.sh for soft3.txt corresponding to NN-ED. The '3' in '3E' corresponds to NN with shielding during learning and execution time.

3. EDF shield: learnTasksEDFshield


---------------------------------------------------------------------------

For model-free learning, high cost during training, and shield during execution:
(This requires python3)

python trainTasks_cost_shield.py <input_task_system> <num_training_steps>

We consider the following entry points:

1. (NN): learnTasksAbs_shield

2. (NN-ED): learnTasksED_shield


---------------------------------------------------------------------------

Q. How do we check if a schedule that has been learnt using model-free learning can lead to an unsafe state or not?

python trainTasksModelCheck.py <input_task_system>
(This uses python3)

An example can be
python trainTasksModelCheck.py onlysoft4.txt

The training takes place for 10000 steps, and then the learnt schedule is model-checked for safety by performing a breadth fast search on the states that can be reached in the MDP by the learnt schedule.

Q. How do we generate a random schedule?

python2.7 task_random_safe_simulate.py onlysoft4.txt

Note that this uses python2.7


---------------------------------------------------------------------------


Model based learning

1. For systems with only soft tasks:
A system with only soft tasks is simulated for n time steps to estimate the distributions of the tasks. The command to run is
python2.7 taskModelBased.py <task_system_file> <num_steps_to_simulate>
python2.7 taskModelBased.py onlysoft4.txt 1000

Then we run Storm on the output file.

2. For systems with both hard and soft tasks
If the task system has m soft tasks, we create m files where each file has all hard tasks and one soft task. (As of now this step is done manually. One can write a script to do that.)
We check if there exists a safe strategy for tasks in each file.
python2.7 task.py <task_system_with_one_soft_task_1> 
python2.7 task.py <task_system_with_one_soft_task_2> 
:
:
:
python2.7 task.py <task_system_with_one_soft_task_m> 

If there is a safe strategy, for each file, then we run the following to estimate the distribution for each task.
python2.7 taskModelBased.py <task_system_file> <num_steps_to_simulate>

As an example, we construct two files soft2_s1.txt and soft2_s2.txt from soft2.txt that has one hard task and two soft tasks.

We run
python2.7 task.py soft2_s1.txt
python2.7 task.py soft2_s2.txt

python2.7 taskModelBased.py soft2.txt 1000

Then we run Storm on the output file.

Another appraoch we do is to extract an optimal strategy for a learnt model, and apply the strategy on the actual model, and observe the value. For this, we run

python3.7 useOptLearntStrategy.py <learnt_file.pm> <orig_file.pm>

An example is:
python3.7 useOptLearntStrategy.py soft2model180.pm soft2.pm

The python script useOpt_parseResults.py is used to parse the results from several runs over multiple training steps using a shell script, and it produces a summary of the results.

3. Computing the distance between the learnt and the actual distributions:
We report this for systems with only soft tasks. It produces the estimates of several kinds of distances like sum norm, Euclidean norm, and Max norm for both computation time and inter-arrival time distributions for various training steps.

The command to run the code is
python2.7 taskModelBased_dist.py <input_task_system> <max_training_steps> <training_steps_interval>

An example is
python2.7 taskModelBased_dist.py onlysoft4.txt 10000 1000

The above command will generate the distance norms for 10000 to 1000 time steps at an interval of 1000.


---------------------------------------------------------------------------
Shell scripts:

We use shell scripts to run batches of simulations for both model-free and model-based learning.

An example of such a shell script for the file hard2.txt is hard2_2_4.sh.
The nomenclature is the following. Considering model-free and model-based learning, we have 5 kinds of learning. The 2 and 4 in _2_4 refer to the second and the fourth kind of learnings together.

1. NN (neural network - model-free) with high cost for missing deadlines of hard tasks, and execute the learned schedule directly. The learnt strategy may not be safe. This can however be used for systems with only soft tasks without any problem.

2. NN with high cost during training, and shielded during execution.

3. NN with shielding during training execution.

4. Model-based learning + FSTTCS procedure: get a safe strategy

5. Shielded model-based learning + optimization

The command to run the shell script is ./hard2_2_4.sh
(Note that sh hard2_2_4.sh may not run it properly in Ubuntu 20.04.)

---------------------------------------------------------------------------


Script txt2csv.py

Script txt2csv_safe.py

---------------------------------------------------------------------------


Shell scripts for generating plots


From task2aig run the following:
python3 encodeTasks.py ../learning_new_repo/learning/Implementation/hard2.txt

(It generates a file tasks.aag in the Aiger format.)

../AbsSynthe/binary/abssynthe --out_file strat.aag --win_region safe.aag tasks.aag

(It generates two files: the most general safe strategy and the winning region in Aiger format)
