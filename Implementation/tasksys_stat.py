#!/usr/bin/env python
"""
Copyright (c) 2019, Guillermo A. Perez @ UAntwerp

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import sys
import re
import math
import numpy as np


class Task(object):
    def __init__(self, bsv, soft=True):
        """
        bsv is a bar-separated-value string whose format is, more or less,
        first_arrival|exe_time_dist|deadline|interarrival_times|cost
        with cost being omitted if the task is hard
        """
        values = map(str.strip, bsv.split("|"))
        # softness
        self.soft = soft
        # first arrival
        self.first_arrival = int(values[0])
        # execution time
        val1_stripped = values[1].strip()
        et_pairs = val1_stripped[1:len(val1_stripped) - 1].split(";")

        def dist(pair_str):
            pair = pair_str.strip().split(",")
            assert(len(pair) == 2)
            return (int(pair[0]), float(pair[1]))

        self.execution_times = dict(map(dist, et_pairs))
        # deadline
        self.deadline = int(values[2])
        # interarrival time
        val3_stripped = values[3].strip()
        pd_pairs = val3_stripped[1:len(val3_stripped) - 1].split(";")
        self.interarrival_times = dict(map(dist, pd_pairs))
        # cost, if needed
        if soft:
            self.cost = int(values[4])

    def min_interarrival_prob(self):
        return min(self.interarrival_times.values())

    def min_execution_prob(self):
        return min(self.execution_times.values())

    def max_arrival_time(self):
        return max(self.interarrival_times.keys())

    def __str__(self):
        info = []
        info.append("First arrival = " + str(self.first_arrival))
        info.append("Execution times = " + str(self.execution_times))
        info.append("Deadline = " + str(self.deadline))
        info.append("Inter-arrival times = " + str(self.interarrival_times))
        info.append("Soft = " + str(self.soft))
        if self.soft:
            info.append("Cost = " + str(self.cost))
        return "\n".join(info)


def parse(f):
    hard2soft = False
    tasks = []
    for line in f.readlines():
        line = line.rstrip()
        if re.search("^[ \t]*#", line):
            continue
        if line.startswith("---------------------"):
            hard2soft = True
            continue
        task = Task(line.strip(), hard2soft)
        tasks.append(task)
    return tasks


def stat(f, eps, delta):
    tasks = parse(f)
    # Upper bound on states
    max_arrival_time = map(lambda t: t.max_arrival_time(), tasks)
    mat_per_task = map(lambda x: x ** 3, max_arrival_time)
    states_bound = np.prod(mat_per_task)
    print("== INFORMATION REGARDING THE SEMANTIC MDP ==")
    print("Max no. states in MDP = " + str(states_bound))
    # Lower bound on probabilities
    min_probs = map(
        lambda t: t.min_execution_prob() * t.min_interarrival_prob(),
        tasks)
    prob_bound = np.prod(min_probs)
    print("Min non-zero probability in MDP = " + str(prob_bound))
    print("== INFORMATION FOR LEARNING ==")
    # Below, we will use notation from the CONCUR18 paper
    mu = (prob_bound / float(len(tasks)))
    print("mu = " + str(mu) + "^" + str(states_bound))
    print("computed mu = " + str(np.float_power(mu, states_bound)))
    t1 = math.log(4.0 * (states_bound ** 2) * float(len(tasks)))
    t2 = math.log(delta)
    t3 = 2.0 * (eps ** 2)
    k = math.ceil((t1 - t2) / t3)
    print("k = " + str(k))
    # we need to compute a complicated root with
    # final piece of information
    print("Min no. of episodes (of length " +
          str(states_bound) + ") required is")
    print("max(k / mu, r) where r is a root of a polynomial I did not compute")


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print(sys.argv[0] + " expects three arguments <file> <eps> <delta>")
        print("<file> - the full path to a task-system specification file")
        print("<eps> - a number between 0 and 1 (strict) for epsilon")
        print("<delta> - another number between 0 and 1 for delta")
        exit(1)
    try:
        f = open(sys.argv[1], "r")
    except Exception:
        print("ERROR! Unable to open the specified file: " + sys.argv[1])
        exit(1)
    try:
        eps = float(sys.argv[2])
        assert(eps > 0 and eps < 1)
    except Exception:
        print("ERROR! Unable to parse epsilon from: " + sys.argv[2])
    try:
        delta = float(sys.argv[3])
        assert(delta > 0 and delta < 1)
    except Exception:
        print("ERROR! Unable to parse delta from: " + sys.argv[3])
    stat(f, eps, delta)
    f.close()
    exit(0)
