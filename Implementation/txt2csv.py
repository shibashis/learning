#!/usr/bin/env python3

import sys
import re  # for regular expressions

storm_output = "Storm output: "
mean_cost = "mean cost "
time2train = "Time to train with "
time2sim = "Time to simulate with "
time2cons = "Time for model construction: "
time2mc = "Time for model checking: "
time_re = re.compile("[0-9]*.[0-9]* seconds")
result_storm = "Result (for initial states): "
mb_train_re = re.compile("[0-9]+ steps.*")
cl_args = "Command line arguments"


def error(line_no, msg):
    print("[Error] " + msg + " (ln. " + str(line_no) + ")")


def avg(nblist):
    if len(nblist) == 0:
        return -1
    else:
        return sum(nblist) / float(len(nblist))


def main(ifilename, ofilename):
    ifile = open(ifilename, 'r')
    found_ground = False
    found_train_steps = False
    nnmc_dict = {}
    mbmc_dict = {}
    nntt_dict = {}
    nnst_dict = {}
    mbtot_dict = {}
    line_no = 1
    for line in ifile.readlines():
        # fetch the actual value from Storm
        pos = line.find(storm_output)
        if pos >= 0:
            # we have the index where the string starts from, we cut out
            # the string itself from the prefix
            from_number = line[pos + len(storm_output):]
            pos = from_number.find(")")
            # and now we cut the suffix from the closing bracket
            # before we cast the string to a number
            ground = float(from_number[:pos])
            found_ground = True
        # fetch a new number of experiments (for NN)
        pos = line.find(time2train)
        if pos >= 0:
            from_number = line[pos + len(time2train):]
            pos = from_number.find(" steps")
            train_steps = int(from_number[:pos])
            found_train_steps = True
            # we now also fetch the training time
            match = time_re.search(line)
            if not match:
                error(line_no, "Found no training time!")
                exit(0)
            sub = line[match.start():match.end()]
            pos = sub.find(" seconds")
            train_time = float(sub[:pos])
            if train_steps not in nntt_dict:
                nntt_dict[train_steps] = [train_time]
            else:
                nntt_dict[train_steps].append(train_time)
        # fetch the simulation time
        pos = line.find(time2sim)
        if pos >= 0:
            match = time_re.search(line)
            if not match:
                error(line_no, "Found no simulation time!")
                exit(0)
            sub = line[match.start():match.end()]
            pos = sub.find(" seconds")
            sim_time = float(sub[:pos])
            if train_steps not in nnst_dict:
                nnst_dict[train_steps] = [sim_time]
            else:
                nnst_dict[train_steps].append(sim_time)
        # fetch the times for MB
        pos = line.find(time2cons)
        if pos >= 0:
            sub = line[len(time2cons):line.find("s.")]
            cons_time = float(sub)
            if train_steps not in mbtot_dict:
                mbtot_dict[train_steps] = [cons_time]
            else:
                mbtot_dict[train_steps].append(cons_time)
        pos = line.find(time2mc)
        if pos >= 0:
            sub = line[len(time2mc):line.find("s.")]
            mc_time = float(sub)
            if train_steps not in mbtot_dict:
                error(line_no, "Found no previous construction time!")
            mbtot_dict[train_steps][-1] += mc_time
        # fetch a new number of experiments (for MB)
        match = mb_train_re.match(line)
        if match:
            pos = match.string.find(" steps")
            train_steps = int(match.string[:pos])
            found_train_steps = True
        # FIXME: this is a hack required by some files
        pos = line.find(cl_args)
        if pos >= 0:
            pos1 = line.find("model")
            pos2 = line.find(".pm")
            train_steps = int(line[pos1 + 5:pos2])
            #train_steps = int(line[pos1 + 10:pos2])
            found_train_steps = True
        # fetch a simulated mean-cost value in decimal notation
        pos = line.find(mean_cost)
        if pos >= 0 and line.find("/") < 0:
            from_number = line[pos + len(mean_cost):]
            if not found_train_steps and from_number == "0\n":
                # FIXME: this is a corner case, 0 is not written as a
                # rational for Shibashis
                pass
            else:
                if not found_train_steps:
                    error(line_no, "Found an NN mean cost, but I did not find "
                          "a training time")
                    exit(0)
                mc = float(from_number)
                if train_steps not in nnmc_dict:
                    nnmc_dict[train_steps] = [mc]
                else:
                    nnmc_dict[train_steps].append(mc)
            # by setting this to false every time, we can check
            # that every mean cost is followed by a restatement of the
            # training step number
            found_train_steps = False
        # fetch a mean-cost from storm in decimal notation
        pos = line.find(result_storm)
        if pos >= 0:
            if not found_train_steps:
                error(line_no, "Found an MB mean cost, but I did not find "
                      "a training time")
                exit(0)
            from_number = line[pos + len(result_storm):]
            mc = float(from_number)
            if train_steps not in mbmc_dict:
                mbmc_dict[train_steps] = [mc]
            else:
                mbmc_dict[train_steps].append(mc)
        # increase line number count
        line_no += 1
    # prepare error dict
    nnerror_dict = {}
    mberror_dict = {}
    if found_ground:
        for k in mbmc_dict:
            mberror_dict[k] = []
            for v in mbmc_dict[k]:
                mberror_dict[k].append(abs(v - ground))
        for k in nnmc_dict:
            nnerror_dict[k] = []
            for v in nnmc_dict[k]:
                nnerror_dict[k].append(abs(v - ground))
    # open output csv file and fill it
    ofile = open(ofilename, "w")
    header = ["No. Steps", "NN", "MB", "NN training time",
              "NN simulation time", "MB total STORM time",
              "NN-error", "MB-error"]
    ofile.write(";".join(header) + "\n")
    for k in nnmc_dict:
        line = [str(k), str(avg(nnmc_dict[k])), str(avg(mbmc_dict[k])),
                str(avg(nntt_dict[k])), str(avg(nnst_dict[k])),
                str(avg(mbtot_dict[k]))]
        if found_ground:
            line.append(str(avg(nnerror_dict[k])))
            line.append(str(avg(mberror_dict[k])))
        ofile.write(";".join(line) + "\n")
    for k in [k for k in mbmc_dict if k not in nnmc_dict]:
        line = [str(k), "", str(avg(mbmc_dict[k])),
                "", "", str(avg(mbtot_dict[k]))]
        if found_ground:
            line.append("")
            line.append(str(avg(mberror_dict[k])))
        ofile.write(";".join(line) + "\n")
    # Print diagnostics
    if not found_ground:
        print("[Warn] Did not find ground value by Storm")
    else:
        print("[Info] Found ground value = " + str(ground))
    print("[Info] NN costs: " + str(nnmc_dict))
    print("[Info] NN training times: " + str(nntt_dict))
    print("[Info] NN simulation times: " + str(nnst_dict))
    print("[Info] MB total STORM times: " + str(mbtot_dict))
    print("[Info] MB costs: " + str(mbmc_dict))
    print("[Info] NN errors: " + str(nnerror_dict))
    print("[Info] MB errors: " + str(mberror_dict))


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("[Error] Please provide input and output filepaths")
        exit(1)

    main(sys.argv[1], sys.argv[2])
    exit(0)
