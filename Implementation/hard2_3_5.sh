touch resultsTACAS_hard2_3_5.txt

counter=10000
while [ $counter -ge 1000 ]
do
	echo $counter >> resultsTACAS_hard2_3_5.txt
	for i in {1..10}
	do
		echo $i
		printf "\r\n" >> resultsTACAS_hard2_3_5.txt
		python3 trainTasks.py hard2.txt $counter >> resultsTACAS_hard2_3_5.txt
		echo "------------------------------------------" >> resultsTACAS_hard2_3_5.txt
	done
	((counter-=1000))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_hard2_3_5.txt
done

echo "===============================" >> resultsTACAS_hard2_3_5.txt

counter=10000
while [ $counter -ge 1000 ]
do
	pmfile="hard2model_safe${counter}.pm"
	echo $counter $pmfile
	echo $counter >> resultsTACAS_hard2_3_5.txt
	for i in {1..10}
	do
		echo $i
		python2.7 taskModelBased_safe.py hard2.txt $counter
		printf "\r\n" >> resultsTACAS_hard2_3_5.txt
		storm --prism $pmfile --prop "Rmin=?[LRA]" >> resultsTACAS_hard2_3_5.txt
		rm $pmfile
	done
	((counter-=1000))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_hard2_3_5.txt
done
