import sys
import stormpy

# returns a scheduler for optimal mean-payoff
def find_scheduler(prismFile):
    program = stormpy.parse_prism_program(prismFile)
    formulas = stormpy.parse_properties_for_prism_program("Rmin=? [LRA]", program) # change to Rmax if you want to maximize
    model = stormpy.build_model(program, formulas)
    result = stormpy.model_checking(model, formulas[0], extract_scheduler=True)
    assert result.has_scheduler
    scheduler = result.scheduler
    assert scheduler.memoryless
    assert scheduler.deterministic
    return scheduler

# given a prism file and scheduler returns a markov chain (technically an MDP with single action for each state)
def get_mc(prismFile,scheduler):
    program = stormpy.parse_prism_program(prismFile)
    model = stormpy.build_model(program)
    dtmc = model.apply_scheduler(scheduler)
    assert dtmc.model_type == stormpy.ModelType.MDP
    for state in dtmc.states:
            assert len(state.actions) == 1
    return dtmc

# returns mean-payoff
def get_value(dtmc):
    formulas = stormpy.parse_properties("Rmin=? [ LRA ]") # Rmax or Rmin does not matter
    result = stormpy.model_checking(dtmc, formulas[0])
    assert result.result_for_all_states
    init_state = dtmc.initial_states[0]
    return(result.at(init_state))

# takes optimal scheduler for prismFile1 and finds mean-payoff for prismFile2 with the scheduler
def main(prismFile1, prismFile2):
    scheduler = find_scheduler(prismFile1)
    dtmc = get_mc(prismFile2,scheduler)
    v = get_value(dtmc)
    return(v)

if __name__ == "__main__":
    learnt_file = sys.argv[1]
    learnt_file = learnt_file.strip()
    orig_file = sys.argv[2]
    orig_file = orig_file.strip()
    v = main(learnt_file,orig_file)
    print(v)
