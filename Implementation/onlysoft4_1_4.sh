touch resultsTACAS_onlysoft4.txt

counter=10000
while [ $counter -ge 1000 ]
do
	echo $counter >> resultsTACAS_onlysoft4.txt
	for i in {1..10}
	do
		echo $i
		printf "\r\n" >> resultsTACAS_onlysoft4.txt
		python3 trainTasks.py onlysoft4.txt $counter >> resultsTACAS_onlysoft4.txt
		echo "------------------------------------------" >> resultsTACAS_onlysoft4.txt
	done
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_onlysoft4.txt
	((counter-=1000))
done

echo "===============================" >> resultsTACAS_onlysoft4.txt

counter=2500
num=4
while [ $counter -ge 250 ]
do
	steps=$((num * counter))
	txtfile="onlysoft4model${steps}.txt"
	pmfile="onlysoft4model${steps}.pm"
	echo $steps $txtfile $pmfile
	echo $steps >> resultsTACAS_onlysoft4.txt
	for i in {1..10}
	do
		echo $i
		python2.7 taskModelBased.py onlysoft4.txt $counter
		python2.7 task.py $txtfile
		printf "\r\n" >> resultsTACAS_onlysoft4.txt
		storm --prism $pmfile --prop "Rmin=?[LRA]" >> resultsTACAS_onlysoft4.txt
		rm $txtfile $pmfile
	done
	((counter-=250))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_onlysoft4.txt
done
