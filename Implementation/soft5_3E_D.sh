touch results_soft5_3E_D.txt

# Add some dummy lines so that txt2csv can parse

printf "soft5.txt: safe states 3273 (Storm output: 10.2817493834)" >> results_soft5_3E_D.txt

printf "\r\n" >> results_soft5_3E_D.txt

counter=10000
#while [ $counter -ge 1000 ]
while [ $counter -ge 9001 ]
do
	echo $counter >> results_soft5_3E_D.txt
	for i in {1..10}
	do
		echo $i
		printf "\r\n" >> results_soft5_3E_D.txt
		python3.7 trainTasks.py soft5.txt $counter >> results_soft5_3E_D.txt
		echo "------------------------------------------" >> results_soft5_3E_D.txt
	done
	((counter-=1000))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> results_soft5_3E_D.txt
done

echo "===============================" >> results_soft5_3E_D.txt

python3.7 avg.py results_soft5_3E_D.txt

# This is a dummy file with results of MB learning for soft2.txt that is needed so that txt2csv can parse
#cat dummy_MB.txt >> results_soft5_3E_D.txt

#python3.7 txt2csv_safe.py results_soft5_3E_D.txt out_soft5_3E_D_tmp.txt

#cut -d';' -f1-2 out_soft5_3E_D_tmp.txt > out_soft5_3E_D.txt
#rm out_soft5_3E_D_tmp.txt

