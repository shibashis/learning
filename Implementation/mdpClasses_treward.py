# mdpClasses.py

import math
import random

import util
from util import raiseNotDefined


class CopyFactory:
	# Abstract class
	# abstract methods that need to be redefined
	@staticmethod
	def newMDPPredicate(*x):
		raiseNotDefined()
		return MDPPredicate(*x)
	@staticmethod
	def newMDPState(*x):
		raiseNotDefined()
		return MDPState(*x)
	@staticmethod
	def newMDPAction(*x):
		raiseNotDefined()
		return MDPAction(*x)
	@staticmethod
	def newMDPStochasticAction(*x):
		raiseNotDefined()
		return MDPStochasticAction(*x)
	@staticmethod
	def newMDPTransition(*x):
		raiseNotDefined()
		return MDPTransition(*x)
	@staticmethod
	def newMDPPath(*x):
		raiseNotDefined()
		return MDPPath(*x)
	@staticmethod
	def newMDPExecution(*x):
		raiseNotDefined()
		return MDPExecution(*x)
	@staticmethod
	def newMDPOperations(*x):
		raiseNotDefined()
		return MDPOperations(*x)
	@staticmethod
	def newMDPExecutionEngine(*x):
		raiseNotDefined()
		return MDPExecutionEngine(*x)


class FileStrFactory:
	# Abstract class
	# abstract methods that need to be redefined
	@staticmethod
	def getMDPPredicate(s:str):
		raiseNotDefined()
		return MDPPredicate.fromFileStr(s)
	@staticmethod
	def getMDPState(s:str):
		raiseNotDefined()
		return MDPState.fromFileStr(s)
	@staticmethod
	def getMDPAction(s:str):
		raiseNotDefined()
		return MDPAction.fromFileStr(s)
	@staticmethod
	def getMDPStochasticAction(s:str):
		raiseNotDefined()
		return MDPStochasticAction.fromFileStr(s)
	@staticmethod
	def getMDPTransition(s:str):
		raiseNotDefined()
		return MDPTransition.fromFileStr(s)
	@staticmethod
	def getMDPPath(s:str):
		raiseNotDefined()
		return MDPPath.fromFileStr(s)
	@staticmethod
	def getMDPExecution(s:str):
		raiseNotDefined()
		return MDPExecution.fromFileStr(s)
	@staticmethod
	def getMDPOperations(s:str):
		raiseNotDefined()
		return MDPOperations.fromFileStr(s)
	@staticmethod
	def getMDPExecutionEngine(s:str):
		raiseNotDefined()
		return MDPExecutionEngine.fromFileStr(s)

class MDPPredicate:
	# Abstract class
	# abstract methods that need to be redefined
	def copyFactory(self):
		raiseNotDefined()
		return CopyFactory
	@classmethod
	def fileStrFactory(cls):
		raiseNotDefined()
		return FileStrFactory

	def __init__(self):
		raiseNotDefined()
	def deepCopy(self):
		raiseNotDefined()
		return self.copyFactory().newMDPState()
	def initFromCopy(self, other):
		raiseNotDefined()
	def __str__(self) -> str:
		raiseNotDefined()
		return "()"

	# abstract methods that can be redefined if needed
	def consoleStr(self) -> str:
		return str(self)
	def fastReset(self, fastResetData):
		self.initFromCopy(fastResetData)
	def getFastResetData(self):
		fastResetData=self.deepCopy()
		return fastResetData

	# define if you intend to write or read instances from a text file
	def fileStr(self) -> str:
		raiseNotDefined()
		return "p"
	@classmethod
	def fromFileStr(cls, s:str):
		raiseNotDefined()
		if s=="":
			raise Exception("parsing error:"+s)
		return cls()

class MDPState:
	# Abstract class
	# abstract methods that need to be redefined
	def copyFactory(self):
		raiseNotDefined()
		return CopyFactory
	@classmethod
	def fileStrFactory(cls):
		raiseNotDefined()
		return FileStrFactory

	def __init__(self):
		raiseNotDefined()
	def deepCopy(self):
		raiseNotDefined()
		return self.copyFactory().newMDPState()
	def initFromCopy(self, other):
		raiseNotDefined()
	def __str__(self) -> str:
		raiseNotDefined()
		return "()"

	# abstract methods that can be redefined if needed
	def consoleStr(self) -> str:
		return str(self)
	def fastReset(self, fastResetData):
		self.initFromCopy(fastResetData)
	def getFastResetData(self):
		fastResetData=self.deepCopy()
		return fastResetData

	# define if you intend to write or read instances from a text file
	def fileStr(self) -> str:
		raiseNotDefined()
		return "s"
	@classmethod
	def fromFileStr(cls, s:str):
		raiseNotDefined()
		if s=="":
			raise Exception("parsing error:"+s)
		return cls()


class MDPAction:
	# Virtual class, Immutable class
	# abstract methods that need to be redefined
	def copyFactory(self):
		raiseNotDefined()
		return CopyFactory
	@classmethod
	def fileStrFactory(cls):
		raiseNotDefined()
		return FileStrFactory

	def __init__(self, infoStr:str):
		raiseNotDefined()
		self.infoStr=infoStr
	def deepCopy(self):
		raiseNotDefined()
		return self.copyFactory().newMDPAction(self.infoStr)
	def __hash__(self) -> int:
		raiseNotDefined()
		return hash(())
	def __eq__(self, other) -> bool:
		raiseNotDefined()
		if self is None and other is None:
			return True
		elif self is None or other is None:
			return False
		return True
	def __str__(self) -> str:
		raiseNotDefined()
		return "("+"infoStr:"+str(self.infoStr)+")"

	# abstract methods that can be redefined if needed
	def consoleStr(self) -> str:
		return str(self)
	def miniConsoleStr(self) -> str:
		return self.consoleStr()

	# define if you intend to write or read instances from a text file
	def fileStr(self) -> str:
		raiseNotDefined()
		return "a"
	@classmethod
	def fromFileStr(cls, s:str):
		raiseNotDefined()
		if s=="":
			raise Exception("parsing error:"+s)
		return cls()


class MDPStochasticAction:
	# Virtual class, Immutable class
	# abstract methods that need to be redefined
	def copyFactory(self):
		raiseNotDefined()
		return CopyFactory
	@classmethod
	def fileStrFactory(cls):
		raiseNotDefined()
		return FileStrFactory

	def __init__(self, infoStr:str):
		raiseNotDefined()
		self.infoStr=infoStr
	def deepCopy(self):
		raiseNotDefined()
		return self.copyFactory().newMDPStochasticAction(self.infoStr)
	def __hash__(self) -> int:
		raiseNotDefined()
		return hash(())
	def __eq__(self, other) -> bool:
		raiseNotDefined()
		if self is None and other is None:
			return True
		elif self is None or other is None:
			return False
		return True
	def __str__(self) -> str:
		raiseNotDefined()
		return "("+"infoStr:"+str(self.infoStr)+")"

	# abstract methods that can be redefined if needed
	def consoleStr(self) -> str:
		return str(self)
	def miniConsoleStr(self) -> str:
		return self.consoleStr()

	# define if you intend to write or read instances from a text file
	def fileStr(self) -> str:
		raiseNotDefined()
		return "s"
	@classmethod
	def fromFileStr(cls, s:str):
		raiseNotDefined()
		if s=="":
			raise Exception("parsing error:"+s)
		return cls()


class MDPTransition:
	# Virtual class, Immutable class
	# abstract methods that need to be redefined
	def copyFactory(self):
		raiseNotDefined()
		return CopyFactory
	@classmethod
	def fileStrFactory(cls):
		raiseNotDefined()
		return FileStrFactory

	# methods that can be inherited
	STR_SEPARATOR=" "
	FILE_SEPARATOR="\n"
	def __init__(self,mdpAction,mdpStochasticAction):
		self.mdpAction=mdpAction # an MDPAction instance
		self.mdpStochasticAction=mdpStochasticAction # an MDPStochasticAction instance
	def deepCopy(self):
		return self.copyFactory().newMDPTransition(self.mdpAction.deepCopy(),self.mdpStochasticAction.deepCopy())
	def __hash__(self) -> int:
		return hash((self.mdpAction, self.mdpStochasticAction))
	def __eq__(self, other) -> bool:
		if self is None and other is None:
			return True
		elif self is None or other is None:
			return False
		if not self.mdpAction == other.mdpAction:
			return False
		if not self.mdpStochasticAction == other.mdpStochasticAction:
			return False
		return True
	def __str__(self) -> str:
		return "(mdpAction:"+str(self.mdpAction)+", mdpStochasticAction:" + str(self.mdpStochasticAction) + ")"

	def consoleStr(self) -> str:
		return self.mdpAction.consoleStr()+MDPTransition.STR_SEPARATOR+self.mdpStochasticAction.consoleStr()

	def fileStr(self) -> str:
		return self.mdpAction.fileStr()+MDPTransition.FILE_SEPARATOR+self.mdpStochasticAction.fileStr()
	@classmethod
	def fromFileStr(cls, s:str):
		splits=s.split(MDPTransition.FILE_SEPARATOR)
		s1=splits[0]
		s2=""
		for i in range(1,len(splits)):
			s2+=splits[i]
		return cls(cls.fileStrFactory().getMDPAction(s1),cls.fileStrFactory().getMDPStochasticAction(s2))


class MDPPath:
	# Virtual class
	# abstract methods that need to be redefined
	def copyFactory(self):
		raiseNotDefined()
		return CopyFactory
	@classmethod
	def fileStrFactory(cls):
		raiseNotDefined()
		return FileStrFactory

	# methods that can be inherited
	FILE_PREFIX="Initial state:\n"
	FILE_SEPARATOR="\nTransitions:\n"
	FILE_LIST_SEPARATOR="\n;\n"
	def __init__(self, mdpInitialState, mdpTransitionsSequence, mdpPredicatesSequence):
		self.mdpInitialState = mdpInitialState # an MDPState instance
		self.mdpTransitionsSequence = mdpTransitionsSequence # a list such that self.mdpTransitionsSequence[i] contains an MDPTransition instance
		self.mdpPredicatesSequence = mdpPredicatesSequence  # a list such that self.mdpPredicatesSequence[i] contains an mdpPredicates list
	def deepCopy(self):
		return self.copyFactory().newMDPPath(self.mdpInitialState.deepCopy(),[mdpTransition.deepCopy() for mdpTransition in self.mdpTransitionsSequence], [[mdpPredicate.deepCopy() for mdpPredicate in mdpPredicates] for mdpPredicates in self.mdpPredicatesSequence])
	def __str__(self) -> str:
		return "(mdpInitialState:"+str(self.mdpInitialState)+", mdpTransitionsSequence:" + "["+"\n".join( [str(mdpTransition) for mdpTransition in self.mdpTransitionsSequence] ) +"]"+", mdpPredicatesSequence:" + "["+"\n".join( [" ".join([str(mdpPredicate) for mdpPredicate in mdpPredicates ]) for mdpPredicates in self.mdpPredicatesSequence] ) +"]" + ")"

	def consoleStr(self) -> str:
		return "state:"+self.mdpInitialState.consoleStr()+"->"+"[\n"+"\n".join( [mdpTransition.consoleStr() for mdpTransition in self.mdpTransitionsSequence] )+"\n]" #+"[\n"+"\n".join( [ " ".join([mdpPredicate.consoleStr() for mdpPredicate in mdpPredicates]) for mdpPredicates in self.mdpPredicatesSequence] )+"\n]"

	def fileStr(self) -> str:
		return MDPPath.FILE_PREFIX+self.mdpInitialState.fileStr()+MDPPath.FILE_SEPARATOR+MDPPath.FILE_LIST_SEPARATOR.join( [mdpTransition.fileStr() for mdpTransition in self.mdpTransitionsSequence] )
	@classmethod
	def fromFileStr(cls, s:str):
		if MDPPath.FILE_PREFIX==s[:len(MDPPath.FILE_PREFIX)]:
			s=s[len(MDPPath.FILE_PREFIX):]
		else:
			raise Exception("bad prefix in parsing path")
		splits=s.split(MDPPath.FILE_SEPARATOR)
		s1=splits[0]
		s2=""
		for i in range(1,len(splits)):
			s2+=splits[i]
		splits2=s2.split(MDPPath.FILE_LIST_SEPARATOR)
		return cls(cls.fileStrFactory().getMDPState(s1),[cls.fileStrFactory().getMDPTransition(ss) for ss in splits2])

	def transitionsCopy(self):
		"""
		half-shallow copy of an MDPPath instance. The transitions sequence can be modified independently
		but not the transitions themselves nor the initial state.
		"""
		return self.copyFactory().newMDPPath(self.mdpInitialState,[mdpTransition for mdpTransition in self.mdpTransitionsSequence], [mdpPredicates for mdpPredicates in self.mdpPredicatesSequence])
	def lastConsoleStr(self) -> str:
		return self.mdpTransitionsSequence[-1].consoleStr()
	def _subConsoleStr(self, i:int, j:int) -> str:
		r1=""
		if i!=0:
			r1=str(i)+":"
		r2=""
		if j!=self.length():
			r2=":"+str(j)
		return r1+"["+" | ".join( [mdpTransition.consoleStr() for mdpTransition in self.mdpTransitionsSequence[i:j]] )+"]"+r2 #+"\n"+r1+"["+" | ".join( [ " ".join([mdpPredicate.consoleStr() for mdpPredicate in mdpPredicates]) for mdpPredicates in self.mdpPredicatesSequence[i:j+1]] )+"]"+r2
	def suffixConsoleStr(self, depth:int) -> str:
		return self._subConsoleStr(depth,self.length())
	def initFileStr(self) -> str:
		return self.mdpInitialState.fileStr()+MDPPath.FILE_SEPARATOR
	def lastFileStr(self) -> str:
		l=self.length()
		if l==0:
			raise Exception("empty sequence")
		elif l==1:
			return self.mdpTransitionsSequence[-1].fileStr()
		else:
			return MDPPath.FILE_LIST_SEPARATOR+self.mdpTransitionsSequence[-1].fileStr()

	def length(self) -> int:
		return len(self.mdpTransitionsSequence)
	def append(self, mdpTransition, mdpPredicates):
		self.mdpTransitionsSequence.append(mdpTransition)
		self.mdpPredicatesSequence.append(mdpPredicates)

	def fastReset(self, i:int):
		self.mdpTransitionsSequence=self.mdpTransitionsSequence[:i]
		self.mdpPredicatesSequence=self.mdpPredicatesSequence[:i+1]


def isTerminalStr(isTerminal:bool) -> str:
	if isTerminal:
		return "Terminal"
	else:
		return ""

def discountFactorStr(discountFactor:float) -> str:
	if discountFactor!=1:
		return "discount:"+str(discountFactor)
	else:
		return ""

class MDPExecution:
	# Virtual class
	# abstract methods that need to be redefined
	def copyFactory(self):
		raiseNotDefined()
		return CopyFactory
	def fileStrFactory(self):
		raiseNotDefined()
		return FileStrFactory

	# methods that can be inherited
	def __init__(self, mdpPath, mdpEndState, mdpPathReward:float, isTerminal:bool, discountFactor:float):
		self.mdpPath = mdpPath # an MDPPath instance
		self.mdpEndState = mdpEndState # an MDPState instance
		self.mdpPathReward = mdpPathReward # reward obtained along the path, including terminal reward if terminal
		self.isTerminal = isTerminal # a boolean that is True if the path is terminal
		self.discountFactor = discountFactor # the current discount factor
	def deepCopy(self):
		return self.copyFactory().newMDPExecution(self.mdpPath.deepCopy(),self.mdpEndState.deepCopy(),self.mdpPathReward,self.isTerminal,self.discountFactor)
	def __str__(self):
		return "(mdpPath:" + str(self.mdpPath)+", mdpEndState:" + str(self.mdpEndState)+", mdpPathReward:" + str(self.mdpPathReward)+", isTerminal:" + str(self.isTerminal)+", discountFactor:" + str(self.discountFactor) + ")"

	def consoleStr(self) -> str:
		return self.mdpPath.consoleStr()+"->state:"+self.mdpEndState.consoleStr()+" reward:"+str(self.mdpPathReward)+" "+isTerminalStr(self.isTerminal)+" "+ discountFactorStr(self.discountFactor)
	def executionCopy(self):
		"""
		half-shallow copy of an MDPExecution instance. Can be used to run independent executions on the same MDP.
		"""
		return self.copyFactory().newMDPExecution(self.mdpPath.transitionsCopy(),self.mdpEndState.deepCopy(),self.mdpPathReward,self.isTerminal,self.discountFactor)
	def lastConsoleStr(self) -> str:
		return self.mdpPath.lastConsoleStr()+"->state:"+self.mdpEndState.consoleStr()+" reward:"+str(self.mdpPathReward)+" "+isTerminalStr(self.isTerminal)+" "+ discountFactorStr(self.discountFactor)
	def suffixConsoleStr(self, depth:int) -> str:
		return self.mdpPath.suffixConsoleStr(depth)+"->state:"+self.mdpEndState.consoleStr()+" reward:"+str(self.mdpPathReward)+" "+isTerminalStr(self.isTerminal)+" "+ discountFactorStr(self.discountFactor)
	def stateConsoleStr(self) -> str:
		return "state:"+self.mdpEndState.consoleStr()+" reward:"+str(self.mdpPathReward)+" "+isTerminalStr(self.isTerminal)+" "+ discountFactorStr(self.discountFactor)


	def length(self) -> int:
		return self.mdpPath.length()
	def append(self, mdpTransition, mdpPredicates):
#		if self.isTerminal:
#			raise Exception("appending to terminal path")
		self.mdpPath.append(mdpTransition, mdpPredicates)

	def fastReset(self, fastResetData):
		length,fastResetDataState,mdpPathReward,isTerminal,discountFactor=fastResetData
		self.mdpPath.fastReset(length)
		self.mdpEndState.fastReset(fastResetDataState)
		self.mdpPathReward = mdpPathReward
		self.isTerminal = isTerminal
		self.discountFactor = discountFactor
	def getFastResetData(self):
		return (self.length(),self.mdpEndState.getFastResetData(),self.mdpPathReward,self.isTerminal,self.discountFactor)



class MDPOperations:
	# Virtual class
	# abstract methods that need to be redefined
	def copyFactory(self):
		raiseNotDefined()
		return CopyFactory
	@classmethod
	def fileStrFactory(cls):
		raiseNotDefined()
		return FileStrFactory

	def __init__(self, discountFactor:float):
		raiseNotDefined()
		self.discountFactor=discountFactor
	def deepCopy(self):
		raiseNotDefined()
		return self.copyFactory().newMDPOperations(self.discountFactor)
	def __str__(self) -> str:
		raiseNotDefined()
		return "(discountFactor:"+str(self.discountFactor)+")"

	def applyTransitionOnState(self, mdpState, mdpTransition):
		raiseNotDefined()
		mdpReward = 0
		# make changes to mdpState according to mdpTransition, and return the reward of this transition
		return mdpReward
	def drawTransition(self, mdpState, mdpAction):
		raiseNotDefined()
		# draw a stochastic transition according to the MDP's distributions
		mdpStochasticAction=self.copyFactory().newMDPStochasticAction("")
		return self.copyFactory().newMDPTransition(mdpAction,mdpStochasticAction)
	def getLegalActions(self, mdpState):
		raiseNotDefined()
		legalActions=[]
		legalActions.append(self.copyFactory().newMDPAction(""))
		return legalActions


	# abstract methods that can be redefined if needed
	def consoleStr(self) -> str:
		return str(self)
	def getAllPredicates(self, mdpState):
		mdpPredicates=[]
		# list all predicates available, true or false
		# mdpPredicates.append(self.copyFactory().newMDPPredicate())
		return mdpPredicates
	def getPredicates(self, mdpState):
		mdpPredicates=[]
		# list all predicates that hold on mdpState
		# mdpPredicates.append(self.copyFactory().newMDPPredicate())
		return mdpPredicates
	def isExecutionTerminal(self, mdpExecution) -> bool:
		# Redefine this method if you want a notion of terminal state/path
		isTerminal = False
		# is the execution over? (terminal state reached, horizon reached, etc)
		return isTerminal
	def getTerminalReward(self, mdpExecution) -> float:
		# Redefine this method if you want a notion of terminal reward
		mdpTerminalReward=0
		# reward for terminal states
		return mdpTerminalReward * mdpExecution.discountFactor
	def getHorizonReward(self, mdpState) -> float:
		mdpHorizonReward=0
		return mdpHorizonReward

	def applyTransition(self, mdpExecution, mdpTransition):
		# Redefine this method if you want something else than discounted total reward
		mdpReward = 0
		isTerminal = mdpExecution.isTerminal
		if isTerminal:
			raise Exception("applying transition to terminal execution")
		mdpState = mdpExecution.mdpEndState
		# make changes to mdpState according to mdpTransition, and set mdpReward accordingly
		mdpReward = self.applyTransitionOnState(mdpState, mdpTransition)
		# update mdpExecution
		mdpExecution.discountFactor *= self.discountFactor
		mdpExecution.mdpPathReward += mdpReward * mdpExecution.discountFactor
		isTerminal = self.isExecutionTerminal(mdpExecution)
		mdpExecution.isTerminal = isTerminal
		if isTerminal:
			mdpExecution.mdpPathReward += self.getTerminalReward(mdpExecution)

	# define if you intend to write or read instances from a text file
	def fileStr(self) -> str:
		raiseNotDefined()
		return "d"+str(self.discountFactor)
	@classmethod
	def fromFileStr(cls, s:str):
		raiseNotDefined()
		if s[:1]!="d":
			raise Exception("bad str provided")
		discountFactor=float(s[1:])
		return cls(discountFactor)


class MDPExecutionEngine:
	# Virtual class
	# abstract methods that need to be redefined
	def copyFactory(self):
		raiseNotDefined()
		return CopyFactory
	def fileStrFactory(self):
		raiseNotDefined()
		return FileStrFactory

	# abstract methods that can be redefined if needed
	def __init__(self, mdpOperations, mdpExecution):
		self.mdpOperations = mdpOperations # an MDPOperations instance
		self.mdpExecution = mdpExecution # an MDPExecution instance
	def deepCopy(self):
		return self.copyFactory().newMDPExecutionEngine(self.mdpOperations.deepCopy(),self.mdpExecution.deepCopy())
	def __str__(self) -> str:
		return "(mdpOperations:"+str(self.mdpOperations)+", mdpExecution:"+str(self.mdpExecution) + ")"

	def consoleStr(self) -> str:
		return self.mdpOperations.consoleStr()+"\n"+self.mdpExecution.consoleStr()
	def executionCopy(self):
		"""
		half-shallow copy of an MDPExecutionEngine instance. Can be used to run independent executions on the same MDP.
		"""
		return self.copyFactory().newMDPExecutionEngine(self.mdpOperations,self.mdpExecution.executionCopy())

	def append(self, mdpTransition):
		self.mdpOperations.applyTransition(self.mdpExecution,mdpTransition)
		mdpPredicates = self.mdpOperations.getPredicates(self.mdpEndState())
		self.mdpExecution.append(mdpTransition, mdpPredicates)
	def getAllPredicates(self):
		return self.mdpOperations.getAllPredicates(self.mdpEndState())
	def getPredicatesSequence(self):
		return self.mdpPath().mdpPredicatesSequence
	def appendPath(self, mdpPath):
		for i in range(mdpPath.length()):
			self.append(mdpPath.mdpTransitionsSequence[i])

	def fastReset(self, fastResetData):
		fastResetDataExecution=fastResetData
		self.mdpExecution.fastReset(fastResetDataExecution)
	def getFastResetData(self):
		return (self.mdpExecution.getFastResetData())

	# methods that can be inherited
	def lastConsoleStr(self) -> str:
		return self.mdpExecution.lastConsoleStr()
	def suffixConsoleStr(self, depth:int) -> str:
		return self.mdpExecution.suffixConsoleStr(depth)
	def stateConsoleStr(self) -> str:
		return self.mdpExecution.stateConsoleStr()

	def length(self) -> int:
		return self.mdpExecution.length()
	def mdpPath(self):
		return self.mdpExecution.mdpPath
	def mdpEndState(self):
		return self.mdpExecution.mdpEndState
	def mdpPathReward(self) -> float:
		return self.mdpExecution.mdpPathReward
	def isTerminal(self) -> bool:
		return self.mdpExecution.isTerminal
	def getHorizonReward(self) -> float:
		endState = self.mdpEndState()
		return self.mdpOperations.getHorizonReward(endState)
	def drawTransition(self, mdpAction):
		return self.mdpOperations.drawTransition(self.mdpExecution.mdpEndState,mdpAction)


class MDPActionStrategy:
	"""
	A strategy for choosing actions
	"""
	# Virtual class
	def __init__(self):
		pass
	def getMDPAction(self, mdpState, mdpOperations):
		"""
		The strategy will receive an MDPOperations instance and
		must return a legal MDPAction
		"""
		mdpActions=mdpOperations.getLegalActions(mdpState)
		return self._getMDPActionInSubset(mdpActions, mdpState, mdpOperations)
	def getMDPActionInSubset(self, mdpActions, mdpState, mdpOperations):
		"""
		The strategy will receive a set of actions and an MDPOperations instance and
		must return a legal MDPAction from the set
		"""
		legal=mdpOperations.getLegalActions(mdpState)
		mdpLegalActions=[]
		for a in mdpActions:
			if a in legal:
				mdpLegalActions.append(a)
		return self._getMDPActionInSubset(mdpLegalActions, mdpState, mdpOperations)
	def _getMDPActionInSubset(self, mdpActions, mdpState, mdpOperations):
		"""
		The Agent will receive an MDPOperations and
		must return an action taken from a set of legal mdpActions
		"""
		raiseNotDefined()


class MDPProbabilisticActionStrategy( MDPActionStrategy ):
	"""
	A probabilistic strategy that draws from a distribution over legal actions
	"""
	# Virtual class
	def _getMDPActionInSubset(self, mdpActions, mdpState, mdpOperations):
		dist = self._getDistribution(mdpActions, mdpState, mdpOperations)
		if len(dist) == 0:
			return None
			# raise Exception("empty distribution")
		choice = util.chooseFromDistribution( dist ).deepCopy()
		isUniversal = True
		vv = None
		for k,v in dist.items():
			if vv is None : vv = v
			if v != vv: isUniversal = False
		if isUniversal:
			pass
			# if choice.infoStr != '':
			# 	choice.infoStr += '#'
			# choice.infoStr += 'SU'
		else:
			if choice.infoStr != '':
				choice.infoStr += '#'
			choice.infoStr += 'S'+str(dist)
		return choice
	def _getDistribution(self, mdpActions, mdpState, mdpOperations):
		"Returns a Counter from util.py encoding a distribution over legal actions."
		raiseNotDefined()


class MDPUniformActionStrategy( MDPProbabilisticActionStrategy ):
	"""
	A strategy that chooses a legal action uniformly at random.
	"""
	def _getDistribution(self, mdpActions, mdpState, mdpOperations):
		dist = util.ConsoleStrFloatCounter()
		for a in mdpActions: dist[a] = 1.0
		dist.normalize()
		return dist


class MDPActionAdvice:
	"""
	A strategy for choosing actions
	"""
	def __init__(self):
		pass
	def getMDPActionAdvice(self, mdpState, mdpOperations):
		"""
		The strategy will receive an MDPOperations instance and
		must return a legal MDPAction
		"""
		mdpActions=mdpOperations.getLegalActions(mdpState)
		return self._getMDPActionAdviceInSubset(mdpActions, mdpState, mdpOperations)
	def _getMDPActionAdviceInSubset(self, mdpActions, mdpState, mdpOperations):
		"""
		The Agent will receive an MDPOperations and
		must return a subset of a set of legal mdpActions
		"""
		choices = []
		for mdpAction in mdpActions:
			if self._isMDPActionAllowed(mdpAction, mdpState, mdpOperations):
				choices.append(mdpAction.deepCopy())
		if len(choices)==len(mdpActions):
			pass
			# for mdpAction in choices:
			# 	if mdpAction.infoStr != '':
			# 		mdpAction.infoStr += '#'
			# 	mdpAction.infoStr += 'A1'
		else:
			for mdpAction in choices:
				if mdpAction.infoStr != '':
					mdpAction.infoStr += '#'
				mdpAction.infoStr += 'A['+','.join([mdpAction.consoleStr() for mdpAction in choices])+']'

		return choices


class MDPFullActionAdvice( MDPActionAdvice ):
	"""
	A trivial advice that allow everything
	"""
	def _isMDPActionAllowed(self, mdpAction, mdpState, mdpOperations):
		return True


class MDPPathAdvice:
	"""
	An advice that allows or rejects paths
	"""
	def __init__(self):
		pass
	def isValidPath(self, mdpExecutionEngine) -> bool:
		"""
		The strategy will receive an MDPExecutionEngine instance
		"""
		raiseNotDefined()


class MDPFullPathAdvice(MDPPathAdvice):
	def isValidPath(self, mdpExecutionEngine) -> bool:
		return True


class MDPEmptyPathAdvice(MDPPathAdvice):
	def isValidPath(self, mdpExecutionEngine) -> bool:
		return False


class MDPNonTerminalPathAdvice(MDPPathAdvice):
	def isValidPath(self, mdpExecutionEngine) -> bool:
		return not mdpExecutionEngine.isTerminal()

class MDPStateScore:
	"""
	A state score for a given agent
	"""
	def __init__(self):
		pass

	def getScore( self, executionEngine ):
		"""
		The Agent will receive an executionEngine and
		must return a score
		"""
		return executionEngine.getHorizonReward()

class MDPSimulationEngine:
	"""
	MDPSimulationEngine is used to draw simulations that extend an execution,
	according to an action strategy.
	The execution is sequentially extended and reset after each simulation,
	Use execution.executionCopy() if running several MDPSimulationEngine instances in parallel
	to give independent instances to the constructors.
	"""
	def __init__(self, mdpExecutionEngine, mdpActionStrategy, mdpActionAdvice, mdpPathAdvice, mdpStateScore, rejectFactor=100, quiet=False):
		self.mdpExecutionEngine = mdpExecutionEngine # an ExecutionEngine instance used to play the new decisions
		self.mdpActionStrategy = mdpActionStrategy # an action strategy
		self.mdpActionAdvice = mdpActionAdvice # an advice on actions (prunes actions)
		self.mdpPathAdvice = mdpPathAdvice # an advice on paths (prunes paths)
		self.mdpStateScore = mdpStateScore # a score to add for an execution during simulation
		self.rejectFactor = rejectFactor
		self.quiet = quiet

	def _getMDPAction(self):
		mdpActions = self.mdpActionAdvice.getMDPActionAdvice(self.mdpExecutionEngine.mdpEndState(), self.mdpExecutionEngine.mdpOperations)
		mdpAction = self.mdpActionStrategy.getMDPActionInSubset(mdpActions,self.mdpExecutionEngine.mdpEndState(), self.mdpExecutionEngine.mdpOperations)
		if mdpAction is None:
			mdpAction = self.mdpActionStrategy.getMDPAction(self.mdpExecutionEngine.mdpEndState(), self.mdpExecutionEngine.mdpOperations)
			if mdpAction is None:
				raise Exception("could not get an action")
		return mdpAction

	def _extend(self):
		"""
		Simulates one time step: draw an action and play the new time step
		in the execution engine.
		"""
		mdpAction = self._getMDPAction()
		mdpTransition = self.mdpExecutionEngine.drawTransition(mdpAction)
		self.mdpExecutionEngine.append(mdpTransition)

	def _runSimulation(self, timeI:int, depth:int, printEachStep:bool):
		"""
		Simulates one full run until a given depth.
		"""
		# if not self.quiet: print("--------------------------------------------------------------")
		# if not self.quiet: print("simulating for",depth-timeI,"steps from state",self.mdpExecutionEngine.stateConsoleStr())

		for j in range(timeI, depth):
			if self.mdpExecutionEngine.isTerminal():
				break
			self._extend()
			if not self.quiet and printEachStep : print("step",j-timeI,":",self.mdpExecutionEngine.lastConsoleStr())
		# if not self.quiet: print("--------------------------------------------------------------")

	def getSimulations(self, numSims:int, depth:int, printEachStep=False):
		"""
		Draws numSims simulations.
		Returns a list of pairs (mdpExecutionEngineS,numS),
		so that mdpExecutionEngineS contains a simulation until horizon depth,
		and so that the sum of numS equals numSims.
		"""
		if not self.quiet: print("[===========================================================[")
		timeI=self.mdpExecutionEngine.length()
		fastResetDataI=self.mdpExecutionEngine.getFastResetData()
		results=[]
		if not self.mdpExecutionEngine.isTerminal() and timeI<depth:
			results=[]
			if not self.quiet: print("running",numSims,"simulations for depth",timeI,"to",depth,"from",self.mdpExecutionEngine.stateConsoleStr())
			if printEachStep: print("")
			numResults = 0
			for s in range(numSims * self.rejectFactor):
				if s>0:
					self.mdpExecutionEngine.fastReset(fastResetDataI)
				# if not self.quiet: print("===========================================================")
				# if not self.quiet: print("running simulation for depth",depth,"from",self.mdpExecutionEngine.stateConsoleStr())
				self._runSimulation(timeI,depth,printEachStep)

				if self.mdpPathAdvice.isValidPath(self.mdpExecutionEngine):
					results.append((self.mdpExecutionEngine.executionCopy(),1))
					numResults += 1
					if not self.quiet and not printEachStep : print("|\t",self.mdpExecutionEngine.suffixConsoleStr(timeI))
				if numResults >= numSims:
					break
			if not self.quiet: print("ran",s+1,"simulations")
		else:
			results=[(self.mdpExecutionEngine.executionCopy(),numSims)]
			if not self.quiet and not printEachStep: print("|\t",self.mdpExecutionEngine.suffixConsoleStr(timeI),"*",numS)
		self.mdpExecutionEngine.fastReset(fastResetDataI)
		if not self.quiet: print("]===========================================================]")
		return results
	def getSimulationReward(self, numSims:int, depth:int, alpha:float) -> float:
		"""
		Draws numSims simulations until horizon depth
		and returns the average mdpReward obtained.
		"""
		if numSims<=0:
			raise Exception("zero simulations is not enough")
			# return 0
		mdpRewardEstimates = 0
		numSelect = 0
		timeI=self.mdpExecutionEngine.length()
		simulations=self.getSimulations(numSims,depth)
		# if not self.quiet: print("obtained",len(simulations),"simulations:")
		for mdpExecutionEngineS,numS in simulations:
			# if not self.quiet: print("|\t",mdpExecutionEngineS.suffixConsoleStr(timeI),"*",numS)
			mdpRewardS=mdpExecutionEngineS.mdpPathReward()
			#mdpRewardS+=alpha*self.mdpStateScore.getScore(mdpExecutionEngineS)
			mdpRewardS = (1-alpha)*mdpRewardS + alpha*self.mdpStateScore.getScore(mdpExecutionEngineS)
			mdpRewardEstimates+=mdpRewardS*numS
			numSelect+=numS
		if numSelect!=numSims:
			if not self.quiet: print("found",numSelect,"valid simulations out of",numSims)
			return None
			# raise Exception("invalid number of simulations obtained")
		mdpRewardEstimates/=numSelect
		return mdpRewardEstimates


class MDPMCTSNode:
	"""
	MDPMCTSNode
	"""

	def __init__(self,mdpState,mdpParentNode,mdpParentTransition,depth,legalActions):
		self.mdpState = mdpState # a MDPState instance
		self.mdpParentNode = mdpParentNode # an MDPMCTSNode
		self.mdpParentTransition = mdpParentTransition # a MDPTransition that leads from mdpParentNode to self
		self.depth = depth # depth of current node
		self.legalActions = legalActions # list of available actions from this node
		self.numVisits = 0 # MCTS counter
		self.totalScore = 0 # MCTS counter
		self.children = {} # maps an action to a list of children MDPMCTSNodes
		self.actionVisits = {}
		self.actionScore = {}

	@classmethod
	def rootFromExec(cls, mdpExecutionEngine, legalActions):
		if mdpExecutionEngine.length()>0:
			raise Exception("bad MCTS root")
		mdpState = mdpExecutionEngine.mdpEndState().deepCopy()
		mdpParentNode = None
		mdpParentTransition = None
		depth = 0
		return cls(mdpState,mdpParentNode,mdpParentTransition,depth,legalActions)

	def __str__(self) -> str:
		strState = str(self.mdpState)
		strDepth = 'Depth : '+str(self.depth)+'\n'
		if not self.parent is None:
			strParent = 'Parent node : ' + str(hash(self.mdpParentNode)) + '\n'
		else:
			strParent = 'Parent node : ' + str(None) + '\n'
		strParentAction = 'Parent decisions : ' + str(self.mdpParentTransition)
		if not self.legalActions is None:
			strLegalActions = '\nLegal actions : ' + str(self.legalActions)
		else:
			strLegalActions = ''
		strPrint = strState + strDepth + strParent + strParentAction + strLegalActions
		return (strPrint)


class MCTSEngine:
	"""
	MCTSEngine
	"""
	def __init__(self, mdpExecutionEngine, mdpActionStrategySelection, mdpActionStrategySimulation, mdpActionAdviceSelection, mdpActionAdviceSimulation, mdpPathAdvice, mdpStateScore, rejectFactor=100, mctsConstant=math.sqrt(2)/2, quiet=False):
		self.mdpExecutionEngine = mdpExecutionEngine # an ExecutionEngine instance used to construct the tree
		mdpActions = mdpActionAdviceSelection.getMDPActionAdvice(self.mdpExecutionEngine.mdpEndState(), self.mdpExecutionEngine.mdpOperations)
		self.root = MDPMCTSNode.rootFromExec(self.mdpExecutionEngine,mdpActions) # an MDPMCTSNode instance for the root of MCTS
		self.mdpActionStrategySelection = mdpActionStrategySelection # an action strategy
		self.mdpActionStrategySimulation = mdpActionStrategySimulation # an action strategy
		self.mdpActionAdviceSelection = mdpActionAdviceSelection # an advice on actions (prunes actions)
		self.mdpActionAdviceSimulation = mdpActionAdviceSimulation # an advice on actions (prunes actions)
		self.mdpPathAdviceSimulation = mdpPathAdvice # an advice on paths (prunes pathd)
		self.mdpStateScore = mdpStateScore #  adds additional score with rewards
		self.rejectFactor = rejectFactor # for the reject path advice
		self.mctsConstant = mctsConstant # the constant used by UCB

		self.quiet = quiet
	def _getMDPActionSelection(self,mdpActions):
		mdpAction = self.mdpActionStrategySelection.getMDPActionInSubset(mdpActions,self.mdpExecutionEngine.mdpEndState(), self.mdpExecutionEngine.mdpOperations)
		if mdpAction is None:
			mdpAction = self.mdpActionStrategySelection.getMDPAction(self.mdpExecutionEngine.mdpEndState(), self.mdpExecutionEngine.mdpOperations)
			if mdpAction is None:
				raise Exception("could not get an action")
		return mdpAction

	def doMCTSIteration(self, numSims, horizon, alpha):
		node=self.root
		executionEngine=self.mdpExecutionEngine
		fastResetData0=executionEngine.getFastResetData()
		# Selection phase
		if not self.quiet: print("Selection phase")
		for depth in range(horizon):
			# Selects an action
			legal=node.legalActions
			ucbScores = util.ConsoleStrFloatCounter()
			notExplored = []
			# extraData=''
			for action in legal:
				action.infoStr = ""
				if action in node.actionVisits and node.actionVisits[action]>0:
					ucbScores[action] = node.actionScore[action] / node.actionVisits[action] + self.mctsConstant * math.sqrt(2 * math.log(node.numVisits) / node.actionVisits[action])
				else:
					notExplored.append(action)
					ucbScores[action] = float('inf')
			if len(notExplored)>0:
				actionChosen = self._getMDPActionSelection(notExplored)
				# extraData='new tie'+str(infoStr)
				if actionChosen.infoStr != '':
					actionChosen.infoStr += '#'
				actionChosen.infoStr += 'UCB'+str(ucbScores)+'NEW' #'NEW['+','.join([a.miniConsoleStr() for a in notExplored])+']'
			else:
				argMax=ucbScores.argMax()
				if argMax==None:
					raise Exception("no move in argMax")
				if len(argMax)>1:
					# actionChosen=random.choice(argMax)
					actionChosen = self._getMDPActionSelection(argMax)
					if actionChosen.infoStr != '':
						actionChosen.infoStr += '#'
					actionChosen.infoStr += 'UCB'+str(ucbScores)+'TIE' #'['+','.join([a.miniConsoleStr() for a in argMax])+']'
					# extraData='ucb '+str(ucbScores)+' tie '+str(infoStr)
				else:
					actionChosen=argMax[0]
					if actionChosen.infoStr != '':
						actionChosen.infoStr += '#'
					actionChosen.infoStr += 'UCB'+str(ucbScores)
					# extraData='ucb '+str(ucbScores)
			# print("Selected action",actionChosen)

			# Draw a child of actionChosen
			mdpTransition = self.mdpExecutionEngine.drawTransition(actionChosen)

			if not self.quiet: print("+\t",mdpTransition.consoleStr())

			# go to next node in execution engine
			executionEngine.append(mdpTransition)
			# Search for next node in children
			nextNode=None
			if actionChosen in node.children:
				for child in node.children[actionChosen]:
					if child.mdpParentTransition == mdpTransition:
						nextNode=child
			if nextNode!=None:
				if not self.quiet: print("->\tfound child in MCTS tree")
				node=nextNode
			else:
				# Constructs the new node
				actionChosen=actionChosen.deepCopy()
				actionChosen.infoStr = ""
				node.actionVisits[actionChosen]=0
				node.actionScore[actionChosen]=0
				legalActions = self.mdpActionAdviceSelection.getMDPActionAdvice(executionEngine.mdpEndState(), executionEngine.mdpOperations)

				newNode = MDPMCTSNode(executionEngine.mdpEndState(),node,mdpTransition,depth+1,legalActions)
				if actionChosen not in node.children:
					node.children[actionChosen]=[newNode]
				else:
					node.children[actionChosen].append(newNode)
				if not self.quiet: print("->\tnew child added to MCTS tree")
				node=newNode
				break
			if executionEngine.isTerminal():
				break
		# Simulation phase
		if not self.quiet: print("Simulation phase")
		simEngine = MDPSimulationEngine(executionEngine, self.mdpActionStrategySimulation,  self.mdpActionAdviceSimulation, self.mdpPathAdviceSimulation, self.mdpStateScore, rejectFactor=self.rejectFactor, quiet=self.quiet)
		simulationReward=simEngine.getSimulationReward(numSims,horizon,alpha)
		if simulationReward is None:
			simEngine = MDPSimulationEngine(executionEngine, self.mdpActionStrategySimulation,  self.mdpActionAdviceSimulation, MDPFullPathAdvice(), self.mdpStateScore, quiet=self.quiet)
			simulationReward=simEngine.getSimulationReward(numSims,horizon,alpha)
			if simulationReward is None:
				raise Exception("could not get a reward estimate from simulation")

		if not self.quiet: print("->\tsimulation outputs score",simulationReward)
		node.numVisits += numSims
		node.totalScore += simulationReward * numSims

		# Backpropagation phase
		if not self.quiet: print("Backpropagation phase")
		while node.mdpParentNode!=None:
			action=node.mdpParentTransition.mdpAction
			node.mdpParentNode.actionVisits[action] += numSims
			node.mdpParentNode.actionScore[action] += simulationReward * numSims
			node.mdpParentNode.numVisits += numSims
			node.mdpParentNode.totalScore += simulationReward * numSims
			node=node.mdpParentNode
		executionEngine.fastReset(fastResetData0)
		if not self.quiet: print("->\tnew root score for action",action.miniConsoleStr(),":",(node.actionScore[action] / node.actionVisits[action]))

	def doMCTSIterations(self, numMCTSIters, numSims, horizon, alpha):
		for i in range(numMCTSIters):
			if not self.quiet:
				print("===========================================================")
				print("MCTS Iteration",i)
				print("===========================================================")
			self.doMCTSIteration(numSims, horizon, alpha)

	def getMCTSRootReward(self,action):
		if action in self.root.actionVisits and self.root.actionVisits[action]>0:
			return self.root.actionScore[action] / self.root.actionVisits[action]
		return 0

	def getMCTSRootAction(self):
		scoreEstimates = util.ConsoleStrFloatCounter()
		for action in  self.root.actionVisits:
			scoreEstimates[action] = self.getMCTSRootReward(action)
		if len(scoreEstimates)==0:
			raise Exception("no known actions at root of MCTS tree")
		argMax=scoreEstimates.argMax()
		if argMax==None:
			raise Exception("no move in argMax")
		actionChosen=random.choice(argMax)
		if not self.quiet:
			print("===========================================================")
			print("MCTS returns",actionChosen.consoleStr(),"from reward estimates",str(scoreEstimates))
			print("===========================================================")
		return actionChosen

class MDPMCTSActionStrategy( MDPActionStrategy ):
	"""
	A probabilistic strategy that draws from a distribution over legal actions
	"""
	# Virtual class
	def __init__(self, numMCTSIters=10, numSims=10, horizon=10, mctsConstant=math.sqrt(2)/2, quiet=False):

		self.numMCTSIters=numMCTSIters
		self.numSims=numSims
		self.horizon=horizon
		self.mctsConstant=mctsConstant
		self.quiet=quiet
	def getMDPAction(self, mdpState, mdpOperations):
		"""
		The strategy will receive an MDPOperations instance and
		must return a legal MDPAction
		"""
		predicates=[]#[ProductMDPPredicate(p) for p in initialPredicateDatas]
		initState=mdpState.deepCopy()
		endState=mdpState.deepCopy()
		mdpPath=mdpOperations.copyFactory().newMDPPath(initState,[],[predicates])
		execEngine=mdpOperations.copyFactory().newMDPExecutionEngine(mdpOperations,mdpOperations.copyFactory().newMDPExecution(mdpPath,endState,0,False,1))
		strategySelection = MDPUniformActionStrategy()
		adviceSelection = MDPFullActionAdvice()
		strategySimulation = MDPUniformActionStrategy()
		adviceSimulation = MDPFullActionAdvice()
		pathAdviceSimulation = MDPFullPathAdvice()
		mdpStateScore = MDPStateScore()
		mctsEngine = MCTSEngine(execEngine, strategySelection, strategySimulation, adviceSelection, adviceSimulation, pathAdviceSimulation,mdpStateScore, mctsConstant=self.mctsConstant, quiet=self.quiet)
		mctsEngine.doMCTSIterations(self.numMCTSIters,self.numSims,self.horizon)
		choice = mctsEngine.getMCTSRootAction()
		return choice
	def getMDPActionInSubset(self, mdpActions, mdpState, mdpOperations):
		return self.getMDPAction(mdpState, mdpOperations)

def getMCTSSimulationEngine(mdpState, mdpPredicates, mdpOperations, numMCTSIters, numSims, horizon, mctsConstant, quietMCTS, rejectFactor, quietSim):
	initState=mdpState.deepCopy()
	endState=mdpState.deepCopy()
	mdpPath=mdpOperations.copyFactory().newMDPPath(initState,[],[mdpPredicates])
	mdpExecutionEngine=mdpOperations.copyFactory().newMDPExecutionEngine(mdpOperations,mdpOperations.copyFactory().newMDPExecution(mdpPath,endState,0,False,1))
	mdpActionStrategy = MDPMCTSActionStrategy(numMCTSIters=numMCTSIters, numSims=numSims, horizon=horizon, mctsConstant=mctsConstant, quiet=quietMCTS)
	mdpActionAdvice = MDPFullActionAdvice()
	mdpPathAdvice = MDPFullPathAdvice()
	mdpStateScore = MDPStateScore()
	mdpSimulationEngine=MDPSimulationEngine(mdpExecutionEngine, mdpActionStrategy, mdpActionAdvice, mdpPathAdvice, mdpStateScore, rejectFactor=rejectFactor, quiet=quietSim)
	return mdpSimulationEngine

def runMCTSTrace(mdpState, mdpPredicates, mdpOperations, numTraces=1, horizonTrace=10, numMCTSIters=10, numSims=10, horizon=10, mctsConstant=math.sqrt(2)/2, quietMCTS=False, rejectFactor=100, quietSim=False):
	mdpSimulationEngine=getMCTSSimulationEngine(mdpState, mdpPredicates, mdpOperations, numMCTSIters, numSims, horizon, mctsConstant, quietMCTS, rejectFactor, quietSim)
	results=mdpSimulationEngine.getSimulations(numTraces,horizonTrace,True)
	return results
