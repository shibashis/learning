import numpy as np
import sys
from six import StringIO, b

from gym import Env, spaces, utils
from gym.utils import seeding


def get_tasks(file_name):
    f=open(file_name, "r")

    #readlines reads the individual line into a list
    fl =f.readlines()
    lines=[]
    tasks= []
    hard_tasks= []
    soft_tasks= []
    has_soft_tasks = False

    f.close()

    # for each line, extract the task parameters

    for x in fl:
        y = x.strip()

        if y[0] == '-':
            hard_tasks = tasks
            tasks = []
            has_soft_tasks = True

        elif y!='' and y[0]!='#':
            lines.append(y)
            task = y.split("|")  # task should have 4 elements

            arrival = int(task[0])

            dist = task[1].split(";")  # distribution on the execution time
            exe = []
            max_exe_time = 0
            for z in dist:
                z = z.strip("[")
                z = z.strip("]")
                z = z.split(",")
                time = int(z[0])

                if time > max_exe_time:  # compute maximum execution time
                    max_exe_time = time

                # change to fraction since float arithmetic produces bad precision
                #prob = float(z[1])
                #exe.append((time, prob))
                exe.append((time, Fraction(Decimal(z[1]))))

            deadline = int(task[2])

            dist = task[3].split(";")  # distribution on the period
            period = []
            arrive_time = []
            for z in dist:
                z = z.strip("[")
                z = z.strip("]")
                z = z.split(",")
                time = int(z[0])

                # change to fraction since float arithmetic produces bad precision
                #prob = float(z[1])
                #period.append((time, prob))
                period.append((time, Fraction(Decimal(z[1]))))

                arrive_time.append(time)

            min_arrive_time = min(arrive_time)

            if has_soft_tasks:
                cost = Fraction(Decimal(task[4]))
                tasks.append([arrival, exe, deadline, period, max_exe_time, min_arrive_time, cost])
            else:
                tasks.append([arrival, exe, deadline, period, max_exe_time, min_arrive_time])

    if has_soft_tasks:
        soft_tasks = tasks
    else:
        hard_tasks = tasks

    return hard_tasks, soft_tasks



def get_init_state(hard_tasks, soft_tasks):

    # fill up the function

    return initState



def get_num_tasks(hard_tasks, soft_tasks):

    # fill up the function

    return num_tasks


def get_upper_bound_num_states(hard_tasks, soft_tasks):

    # fill up the function

    return num_states


def get_reward(curr_state, a, next_state):

    # fill up the function

    return reward



class TaskSystemEnv(Env):

    def __init__(self, file_name):
        # create the task parameters
        #self.hard_tasks, self.soft_tasks = get_tasks(file_name)

        self.action_space = spaces.Discrete(2)
        self.observation_space = spaces.Discrete(10)

        self.id_map = {0: 0}
        self.next_id = 1

        self.seed()
        self.reset()


    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]


    def reset(self):
        self.currentState = 0
        self.last_action=None
        return self.id_map[self.currentState]


    def step(self, a):
        # execute the action and return the next state
        next_state = 0
        reward = 1

        # check if next_state is already in self.id_map
        #for name, age in self.id_map.items():  # for name, age in dictionary.iteritems():  (for Python 2.x)
        #    if age == search_age:

        next_state_key = list(self.id_map.keys())[list(self.id_map.values()).index(next_state)]

        if not next_state_key:
            self.id_map[self.next_id] = next_state
            next_state_key = [self.next_id]
            self.next_id = self.next_id + 1

        self.currentState = next_state
        self.last_action = a

        return (next_state_key, reward, False, {})

        #transitions = self.P[self.s][a]
        #i = categorical_sample([t[0] for t in transitions], self.np_random)
        #p, s, r, d= transitions[i]
        #self.s = s
        #self.last_action=a
        #return (s, r, d, {"prob" : p})