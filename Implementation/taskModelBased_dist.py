import numpy as np
import random
from fractions import Fraction
from decimal import Decimal
import sys
import math


def get_tasks(file_name):
    f=open(file_name, "r")

    #readlines reads the individual line into a list
    fl =f.readlines()
    lines=[]
    tasks= []
    hard_tasks= []
    soft_tasks= []
    has_soft_tasks = False

    f.close()

    # for each line, extract the task parameters

    for x in fl:
        y = x.strip()

        if y[0] == '-':
            hard_tasks = tasks
            tasks = []
            has_soft_tasks = True

        elif y!='' and y[0]!='#':
            lines.append(y)
            task = y.split("|")  # task should have 4 elements

            arrival = int(task[0])

            dist = task[1].split(";")  # distribution on the execution time
            exe = []
            max_exe_time = 0
            for z in dist:
                z = z.strip("[")
                z = z.strip("]")
                z = z.split(",")
                time = int(z[0])

                if time > max_exe_time:  # compute maximum execution time
                    max_exe_time = time

                # change to fraction since float arithmetic produces bad precision
                #prob = float(z[1])
                #exe.append((time, prob))
                exe.append((time, Fraction(Decimal(z[1]))))

            deadline = int(task[2])

            dist = task[3].split(";")  # distribution on the period
            period = []
            arrive_time = []
            for z in dist:
                z = z.strip("[")
                z = z.strip("]")
                z = z.split(",")
                time = int(z[0])

                # change to fraction since float arithmetic produces bad precision
                #prob = float(z[1])
                #period.append((time, prob))
                period.append((time, Fraction(Decimal(z[1]))))

                arrive_time.append(time)

            min_arrive_time = min(arrive_time)

            if has_soft_tasks:
                cost = Fraction(Decimal(task[4]))
                tasks.append([arrival, exe, deadline, period, max_exe_time, min_arrive_time, cost])
            else:
                tasks.append([arrival, exe, deadline, period, max_exe_time, min_arrive_time])

    if has_soft_tasks:
        soft_tasks = tasks
    else:
        hard_tasks = tasks

    return hard_tasks, soft_tasks


# learn execution time and inter-arrival time distributions
def learn_tasks(tasks, time_step):

    total_jobs_arr_per_task = len(tasks) * [0]
    total_jobs_exe_per_task = len(tasks) * [0]
    len_tasks = len(tasks)
    exe_times = []
    arr_times = []
    exe_probs = []
    arr_probs = []

    arrival = [x[0] for x in tasks]
    exe_dist = [x[1] for x in tasks]
    arr_dist = [x[3] for x in tasks]

    for i in range(len_tasks):
        exe_times = exe_times + [len(exe_dist[i]) * [0]]
        arr_times = arr_times + [len(arr_dist[i]) * [0]]
        exe_probs = exe_probs + [len(exe_dist[i]) * [0]]
        arr_probs = arr_probs + [len(arr_dist[i]) * [0]]

    # we schedule the tasks in round robin manner
    task_to_schedule = 0
    time_to_schedule = 0
    for t in range(time_step):

        if arrival[task_to_schedule] == t and t >= time_to_schedule:
            # schedule a job of the next task in round robin manner if the job of the previous task has finished
            total_jobs_exe_per_task[task_to_schedule] = total_jobs_exe_per_task[task_to_schedule] + 1
            # randomly generate the execution time following the distribution
            probs = [x[1] for x in exe_dist[task_to_schedule]]
            cum_probs = np.cumsum(probs)  # cumulative sum of probabilities

            random_prob = random.uniform(0, 1)
            # From next_states choose the state whose index i is such that probs[i-1] <= random_prob <= probs[i]
            ind_list = [j for j in range(len(cum_probs)) if cum_probs[j] > random_prob]
            exe_times[task_to_schedule][ind_list[0]] = exe_times[task_to_schedule][ind_list[0]] + 1

            time_to_schedule = t + exe_dist[task_to_schedule][ind_list[0]][0]
            task_to_schedule+=1
            if task_to_schedule == len_tasks:
                task_to_schedule = 0

        for i in range(len_tasks):
            if arrival[i] == t:
                # collect samples for inter-arrival time distribution
                total_jobs_arr_per_task[i] = total_jobs_arr_per_task[i] + 1
                # randomly generate the arrival time following the distribution
                probs = [x[1] for x in arr_dist[i]]
                cum_probs = np.cumsum(probs)  # cumulative sum of probabilities

                random_prob = random.uniform(0, 1)
                # From next_states choose the state whose index i is such that probs[i-1] <= random_prob <= probs[i]
                ind_list = [j for j in range(len(cum_probs)) if cum_probs[j] > random_prob]
                arrival[i] = t + arr_dist[i][ind_list[0]][0]
                arr_times[i][ind_list[0]] = arr_times[i][ind_list[0]] + 1

                # randomly generate the execution time following the distribution
                # probs = [x[1] for x in exe_dist[i]]
                # cum_probs = np.cumsum(probs)  # cumulative sum of probabilities
                #
                # random_prob = random.uniform(0, 1)
                # # From next_states choose the state whose index i is such that probs[i-1] <= random_prob <= probs[i]
                # ind_list = [j for j in range(len(cum_probs)) if cum_probs[j] > random_prob]
                # exe_times[i][ind_list[0]] = exe_times[i][ind_list[0]] + 1

    for i in range(len_tasks):
        for j in range(len(exe_times[i])):
            exe_probs[i][j] = round(float(exe_times[i][j]) / total_jobs_exe_per_task[i], 4) # 4 places after decimal

        for j in range(len(arr_times[i])):
            arr_probs[i][j] = round(float(arr_times[i][j]) / total_jobs_arr_per_task[i], 4) # 4 places after decimal

    return exe_probs, arr_probs, min(total_jobs_arr_per_task), min(total_jobs_exe_per_task)


# learn execution time and inter-arrival time distributions
def learn_tasks_old(tasks, time_step):

    total_jobs_per_task = len(tasks) * [0]
    len_tasks = len(tasks)
    exe_times = []
    arr_times = []
    exe_probs = []
    arr_probs = []

    arrival = [x[0] for x in tasks]
    exe_dist = [x[1] for x in tasks]
    arr_dist = [x[3] for x in tasks]

    for i in range(len_tasks):
        exe_times = exe_times + [len(exe_dist[i]) * [0]]
        arr_times = arr_times + [len(arr_dist[i]) * [0]]
        exe_probs = exe_probs + [len(exe_dist[i]) * [0]]
        arr_probs = arr_probs + [len(arr_dist[i]) * [0]]

    # we learn all tasks simultaneously to make it order independent
    for t in range(time_step):

        for i in range(len_tasks):
            if arrival[i] == t:
                total_jobs_per_task[i] = total_jobs_per_task[i] + 1
                # randomly generate the arrival time following the distribution
                probs = [x[1] for x in arr_dist[i]]
                cum_probs = np.cumsum(probs)  # cumulative sum of probabilities

                random_prob = random.uniform(0, 1)
                # From next_states choose the state whose index i is such that probs[i-1] <= random_prob <= probs[i]
                ind_list = [j for j in range(len(cum_probs)) if cum_probs[j] > random_prob]
                arrival[i] = t + arr_dist[i][ind_list[0]][0]
                arr_times[i][ind_list[0]] = arr_times[i][ind_list[0]] + 1

                # randomly generate the execution time following the distribution
                probs = [x[1] for x in exe_dist[i]]
                cum_probs = np.cumsum(probs)  # cumulative sum of probabilities

                random_prob = random.uniform(0, 1)
                # From next_states choose the state whose index i is such that probs[i-1] <= random_prob <= probs[i]
                ind_list = [j for j in range(len(cum_probs)) if cum_probs[j] > random_prob]
                exe_times[i][ind_list[0]] = exe_times[i][ind_list[0]] + 1

    for i in range(len_tasks):
        for j in range(len(exe_times[i])):
            exe_probs[i][j] = round(float(exe_times[i][j]) / total_jobs_per_task[i], 4) # 4 places after decimal

        for j in range(len(arr_times[i])):
            arr_probs[i][j] = round(float(arr_times[i][j]) / total_jobs_per_task[i], 4) # 4 places after decimal

    return exe_probs, arr_probs

# sum norm
def get_norm1(learnt_task_dists, soft_tasks):

    sum_exe_diff = 0
    sum_arr_diff = 0
    learnt_exe_probs, learnt_arr_probs = learnt_task_dists

    for i in range(len(soft_tasks)):

        [arrival, exe, deadline, period, max_exe_time, min_arrive_time, cost] = soft_tasks[i]
        exe_prob = [elem[1] for elem in exe]
        for j in range(len(exe_prob)):
            sum_exe_diff += abs(exe_prob[j] - learnt_exe_probs[i][j])

        arr_prob = [elem[1] for elem in period]
        for j in range(len(arr_prob)):
            sum_arr_diff += abs(arr_prob[j] - learnt_arr_probs[i][j])

    #print 'Sum norm: exe_dist ' + str(sum_exe_diff) + '; arr_dist ' + str(sum_arr_diff)
    return sum_exe_diff, sum_arr_diff

# Euclidaan norm
def get_norm2(learnt_task_dists, soft_tasks):

    sqrt_exe_diff = 0
    sqrt_arr_diff = 0
    learnt_exe_probs, learnt_arr_probs = learnt_task_dists

    for i in range(len(soft_tasks)):

        [arrival, exe, deadline, period, max_exe_time, min_arrive_time, cost] = soft_tasks[i]
        exe_prob = [elem[1] for elem in exe]
        for j in range(len(exe_prob)):
            sqrt_exe_diff += abs(exe_prob[j] - learnt_exe_probs[i][j]) ** 2

        arr_prob = [elem[1] for elem in period]
        for j in range(len(arr_prob)):
            sqrt_arr_diff += abs(arr_prob[j] - learnt_arr_probs[i][j]) ** 2

    sqrt_exe_diff = math.sqrt(sqrt_exe_diff)
    sqrt_arr_diff = math.sqrt(sqrt_arr_diff)

    #print 'Euclidean norm: exe_dist ' + str(sqrt_exe_diff) + '; arr_dist ' + str(sqrt_arr_diff)
    return sqrt_exe_diff, sqrt_arr_diff



# max norm
def get_norm_inf(learnt_task_dists, soft_tasks):

    exe_diff_list = []
    arr_diff_list = []
    learnt_exe_probs, learnt_arr_probs = learnt_task_dists

    for i in range(len(soft_tasks)):

        [arrival, exe, deadline, period, max_exe_time, min_arrive_time, cost] = soft_tasks[i]
        exe_prob = [elem[1] for elem in exe]
        for j in range(len(exe_prob)):
            exe_diff_list.append(abs(exe_prob[j] - learnt_exe_probs[i][j]))

        arr_prob = [elem[1] for elem in period]
        for j in range(len(arr_prob)):
            arr_diff_list.append(abs(arr_prob[j] - learnt_arr_probs[i][j]))

    max_exe_diff = max(exe_diff_list)
    max_arr_diff = max(arr_diff_list)

    #print 'Max norm: exe_dist ' + str(max_exe_diff) + '; arr_dist ' + str(max_arr_diff)
    return max_exe_diff, max_arr_diff



def main():

    # file_name = raw_input("Enter file name: ")
    file_name = sys.argv[1]
    file_name = file_name.strip()
    hard_tasks, soft_tasks = get_tasks(file_name)  # hard_tasks and soft_tasks are a list of task descriptions: [arrival, exe dist, deadline, period dist, max_exe_time, min_arrive_time] elements

    # We want to simulate the tasks and learn over so many steps
    # Assuming that we learn n tasks, the actual number of time_step is time_step*n
    # time_step = 1000
    max_step = int(sys.argv[2])
    interval = int(sys.argv[3])
    num_expt = 50

    interval = interval * -1
    # tasks = hard_tasks + soft_tasks

    print 'Training steps; Min inter-arrival samples; Min exe time samples; Sum norm exe; Sum norm arr; Euclidean norm exe; Euclidean norm arr; Max norm exe; Max norm arr'

    for time_step in range(max_step, 0, interval):
        time_step_per_task = time_step / len(soft_tasks)

        diff_exe_norm1 = 0
        diff_arr_norm1 = 0

        diff_exe_norm2 = 0
        diff_arr_norm2 = 0

        diff_exe_norm_inf = 0
        diff_arr_norm_inf = 0

        jobs_arr_per_task = 0
        jobs_exe_per_task = 0
        for i in range(num_expt):
            #learnt_task_dists = learn_tasks_old(soft_tasks, time_step_per_task)
            exe_probs, arr_probs, min_jobs_arr_per_task, min_jobs_exe_per_task = learn_tasks(soft_tasks, time_step)

            jobs_arr_per_task += min_jobs_arr_per_task
            jobs_exe_per_task += min_jobs_exe_per_task

            learnt_task_dists = (exe_probs, arr_probs)

            (exe_diff, arr_diff) = get_norm1(learnt_task_dists, soft_tasks)
            diff_exe_norm1 = diff_exe_norm1 + exe_diff
            diff_arr_norm1 = diff_arr_norm1 + arr_diff

            (exe_diff, arr_diff) = get_norm2(learnt_task_dists, soft_tasks)
            diff_exe_norm2 = diff_exe_norm2 + exe_diff
            diff_arr_norm2 = diff_arr_norm2 + arr_diff

            (exe_diff, arr_diff) = get_norm_inf(learnt_task_dists, soft_tasks)
            diff_exe_norm_inf = diff_exe_norm_inf + exe_diff
            diff_arr_norm_inf = diff_arr_norm_inf + arr_diff

        jobs_arr_per_task = jobs_arr_per_task / num_expt
        jobs_exe_per_task = jobs_exe_per_task / num_expt

        diff_exe_norm1 = diff_exe_norm1 / num_expt
        diff_arr_norm1 = diff_arr_norm1 / num_expt

        diff_exe_norm2 = diff_exe_norm2 / num_expt
        diff_arr_norm2 = diff_arr_norm2 / num_expt

        diff_exe_norm_inf = diff_exe_norm_inf / num_expt
        diff_arr_norm_inf = diff_arr_norm_inf / num_expt

        print str(time_step) + ';' + str(jobs_arr_per_task) + ';' + str(jobs_exe_per_task) + ';' + str(diff_exe_norm1) + ';' + str(diff_arr_norm1) + ';' + str(diff_exe_norm2) + ';' + \
              str(diff_arr_norm2) + ';' + str(diff_exe_norm_inf) + ';' + str(diff_arr_norm_inf)

    #out_file = file_name.split(".")
    #generate_file(out_file[0] + 'model' + str(time_step*len(soft_tasks)) + '.txt', learnt_task_dists, hard_tasks, soft_tasks)


if __name__== "__main__":
    main()
