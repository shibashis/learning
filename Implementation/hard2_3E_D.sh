touch resultsTACAS_hard2_3E_D.txt

counter=10000
while [ $counter -ge 1000 ]
do
	echo $counter >> resultsTACAS_hard2_3E_D.txt
	for i in {1..10}
	do
		echo $i
		printf "\r\n" >> resultsTACAS_hard2_3E_D.txt
		python3 trainTasks.py hard2.txt $counter >> resultsTACAS_hard2_3E_D.txt
		echo "------------------------------------------" >> resultsTACAS_hard2_3E_D.txt
	done
	((counter-=1000))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_hard2_3E_D.txt
done

echo "===============================" >> resultsTACAS_hard2_3E_D.txt
