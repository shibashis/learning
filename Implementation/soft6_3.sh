touch resultsTACAS_soft6_3.txt

counter=10000
while [ $counter -ge 1000 ]
do
	echo $counter >> resultsTACAS_soft6_3.txt
	for i in {1..10}
	do
		echo $i
		printf "\r\n" >> resultsTACAS_soft6_3.txt
		python3 trainTasks.py soft6.txt $counter >> resultsTACAS_soft6_3.txt
		echo "------------------------------------------" >> resultsTACAS_soft6_3.txt
	done
	((counter-=1000))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_soft6_3.txt
done

echo "===============================" >> resultsTACAS_soft6_3.txt
