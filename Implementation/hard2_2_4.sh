touch resultsTACAS_hard2.txt

counter=10000
while [ $counter -ge 1000 ]
do
	echo $counter >> resultsTACAS_hard2.txt
	for i in {1..10}
	do
		echo $i
		printf "\r\n" >> resultsTACAS_hard2.txt
		python3 trainTasks_cost_shield.py hard2.txt $counter >> resultsTACAS_hard2.txt
		echo "------------------------------------------" >> resultsTACAS_hard2.txt
	done
	((counter-=1000))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_hard2.txt
done

echo "===============================" >> resultsTACAS_hard2.txt

counter=10000
while [ $counter -ge 1000 ]
do
	txtfile="hard2model${counter}.txt"
	pmfile="hard2model${counter}.pm"
	echo $counter $txtfile $pmfile
	echo $counter >> resultsTACAS_hard2.txt
	for i in {1..10}
	do
		echo $i
		python2.7 taskModelBased.py hard2.txt $counter
		python2.7 task.py $txtfile
		printf "\r\n" >> resultsTACAS_hard2.txt
		storm --prism $pmfile --prop "Rmin=?[LRA]" >> resultsTACAS_hard2.txt
		rm $txtfile $pmfile
	done
	((counter-=1000))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_hard2.txt
done
