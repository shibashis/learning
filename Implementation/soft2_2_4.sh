touch resultsTACAS_soft2.txt

counter=10000
while [ $counter -ge 1000 ]
do
	echo $counter >> resultsTACAS_soft2.txt
	for i in {1..10}
	do
		echo $i
		printf "\r\n" >> resultsTACAS_soft2.txt
		python3 trainTasks_cost_shield.py soft2.txt $counter >> resultsTACAS_soft2.txt
		echo "------------------------------------------" >> resultsTACAS_soft2.txt
	done
	((counter-=1000))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_soft2.txt
done

echo "===============================" >> resultsTACAS_soft2.txt

counter=5000
num=2
while [ $counter -ge 500 ]
do
	steps=$((num * counter))
	txtfile="soft2model${steps}.txt"
	pmfile="soft2model${steps}.pm"
	echo $steps $txtfile $pmfile
	echo $steps >> resultsTACAS_soft2.txt
	for i in {1..10}
	do
		echo $i
		python2.7 taskModelBased.py soft2.txt $counter
		python2.7 task.py $txtfile
		printf "\r\n" >> resultsTACAS_soft2.txt
		storm --prism $pmfile --prop "Rmin=?[LRA]" >> resultsTACAS_soft2.txt
		rm $txtfile $pmfile
	done
	((counter-=500))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_soft2.txt
done
