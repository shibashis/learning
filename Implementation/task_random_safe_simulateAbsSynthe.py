import networkx as nx
import time
from fractions import Fraction
from decimal import Decimal
import numpy as np
import random
from decodeSafe import AIG

import sys
import os

def get_tasks(file_name):
    f=open(file_name, "r")

    #readlines reads the individual line into a list
    fl =f.readlines()
    lines=[]
    tasks= []
    hard_tasks= []
    soft_tasks= []
    has_soft_tasks = False

    f.close()

    # for each line, extract the task parameters

    for x in fl:
        y = x.strip()

        if y[0] == '-':
            hard_tasks = tasks
            tasks = []
            has_soft_tasks = True

        elif y!='' and y[0]!='#':
            lines.append(y)
            task = y.split("|")  # task should have 4 elements

            arrival = int(task[0])

            dist = task[1].split(";")  # distribution on the execution time
            exe = []
            max_exe_time = 0
            for z in dist:
                z = z.strip("[")
                z = z.strip("]")
                z = z.split(",")
                time = int(z[0])

                if time > max_exe_time:  # compute maximum execution time
                    max_exe_time = time

                # change to fraction since float arithmetic produces bad precision
                #prob = float(z[1])
                #exe.append((time, prob))
                exe.append((time, Fraction(Decimal(z[1]))))

            deadline = int(task[2])

            dist = task[3].split(";")  # distribution on the period
            period = []
            arrive_time = []
            for z in dist:
                z = z.strip("[")
                z = z.strip("]")
                z = z.split(",")
                time = int(z[0])

                # change to fraction since float arithmetic produces bad precision
                #prob = float(z[1])
                #period.append((time, prob))
                period.append((time, Fraction(Decimal(z[1]))))

                arrive_time.append(time)

            min_arrive_time = min(arrive_time)

            if has_soft_tasks:
                cost = Fraction(Decimal(task[4]))
                tasks.append([arrival, exe, deadline, period, max_exe_time, min_arrive_time, cost])
            else:
                tasks.append([arrival, exe, deadline, period, max_exe_time, min_arrive_time])

    if has_soft_tasks:
        soft_tasks = tasks
    else:
        hard_tasks = tasks

    return hard_tasks, soft_tasks


# pop the first element of queue
def pop_queue(queue):

    return (queue[0], queue[1:])

# push n elements to the end of queue
def push_queue(queue, n):

    return queue + range(queue[-1]+1, queue[-1]+1+n)


def dec_dist(dist):
    new_dist = []
    for x in dist:
        new_dist.append((x[0]-1, x[1]))

    return new_dist


def modify_params(rct_dist, curr_deadline, iat_dist):
    dec_rct_dist = dec_dist(rct_dist)
    dec_iat_dist = dec_dist(iat_dist)

    if curr_deadline > 0:  # check if deadline is greater than 0
        deadline = curr_deadline - 1
    else:
        deadline = curr_deadline

    return dec_rct_dist, deadline, dec_iat_dist



def normalize_dist(dist):
    # dist is a list with elements of the form (time, prob)
    prob_dist = [x[1] for x in dist]
    sum_dist = sum(prob_dist)

    dist = [(x[0], 1 / sum_dist * x[1]) for x in dist]  # float is not needed since we use fraction
    #dist = [(x[0], float(1/sum_dist) * x[1]) for x in dist]
    # dist = [(x[0], float(x[1]/sum_dist)) for x in dist] # This expression is correct but gives imprecise floating point numbers
    return dist


def add_possible_states_task_j(rct_dist, iat_dist, init_config_j, deadline, dec_iat_dist, hard):

    possible_states_task_j = []

    if iat_dist[0][0] > 1:
        possible_states_task_j.append((1, [rct_dist, deadline, dec_iat_dist], 0))

    elif iat_dist[0][0] == 1:
        if hard:
            if deadline >= rct_dist[-1][0]:  # deadline >= max rct
                possible_states_task_j.append((iat_dist[0][1], init_config_j, 0))  # initial state
        else:
            bad_flag = 0
            if rct_dist[0][0] > 0:
                bad_flag = 1
            possible_states_task_j.append((iat_dist[0][1], init_config_j, bad_flag))  # initial state

        if len(iat_dist) > 1:
            possible_states_task_j.append((1-iat_dist[0][1], [rct_dist, deadline, dec_iat_dist[1:]], 0))

    return possible_states_task_j


def safe(task):
    # task is an element of the form [rct, deadline, iat], where rct is of the form [(t1, p1), ..., (tn, pn)]
    # check if deadline >= max rct
    max_rct = task[0][-1][0]
    deadline = task[1]
    if max_rct > deadline:
        return False

    else:
        return True


def compute_states(raw_states, index, num_hard_tasks):
    # raw_states is a list where each element is also a list that corresponds to each task and has elements (prob, [rct, deadline, iat], bad_flag)
    # it returns states where each element is of the form (prob, [[rct1, deadline1, iat1], [rct2, deadline2, iat2], ... [rctn, deadlinen, iatn]], [bad_flag1, bad_flag2, ... bad_flagn]),
    # that is the set of future states
    # we compute the probabilities of each state, denoted by prob and bad_array_i in state i is an array with an element
    # for each task; the element is 1 if the task does not meet its deadline, else it is 0

    hard = True
    if index > num_hard_tasks:
        hard = False
    states = []
    if len(raw_states)==1:
        for x in raw_states[0]:
            if hard:
                if safe(x[1]):
                    states.append((x[0], [x[1]], [0]))
            else:
                states.append((x[0], [x[1]], [x[2]]))

        return states

    else:
        # recursive call
        subsets = compute_states(raw_states[1:], index+1, num_hard_tasks)
        for x in raw_states[0]:
            if hard:
                if safe(x[1]):
                    for y in subsets:
                        states.append((x[0]*y[0], [x[1]] + y[1], [0] + y[2]))

            else:  # this will never be executed since we do not compute the product of the soft tasks together
                for y in subsets:
                    states.append((x[0] * y[0], [x[1]] + y[1], [x[2]] + y[2]))

        return states



def get_states_execute_i(all_tasks, i, tasks, num_hard_tasks):
    # all_tasks is a list of [rct dist, deadline, iat dist] elements, one for each task. It denotes the current state
    # i is the task that will be executed (start index is 0)
    # tasks is a list of the task descriptions [arrival, exe dist, deadline, period dist, max_exe_time, min_arrive_time]

    # This function returns a tuple (i, states), where states is the set of all states obtained after executing task i,
    # and the second member of the tuple denotes that task i has been executed to reach these states
    # each element of states is a state where each state is of the form
    # (prob, [[rct1, deadline1, iat1], [rct2, deadline2, iat2], ... [rctn, deadlinen, iatn]], [bad_flag1, bad_flag2, ... bad_flagn]),

    raw_states = []
    hard = True

    for j in range(len(all_tasks)):

        if j == num_hard_tasks:
            hard = False

        task = all_tasks[j]
        init_config_j = tasks[j][1:4]  # gives rct, deadline, iat in the current config

        # normalize distributions so that the probabilities sum up to 1
        rct_dist = normalize_dist(task[0])
        iat_dist = normalize_dist(task[2])
        possible_states_task_j = []

        # reduce times by 1 due to one clock tick
        dec_rct_dist, deadline, dec_iat_dist = modify_params(rct_dist, task[1], iat_dist)

        # compute remaining computation time distribution for task j that is scheduled
        if j == i:
            if (rct_dist[0][0] > 1 or len(rct_dist) == 1) and iat_dist[0][0] > 1:
                # only one successor for task j
                # the first parameter is the probability that task j evolves to this state
                # the probability is 1 since there is only one successor
                # the last parameter is a bad flag needed only for soft tasks, it is 1 when a new task comes but the previous one is not finished
                possible_states_task_j.append((1, [dec_rct_dist, deadline, dec_iat_dist], 0))

            elif iat_dist[0][0] == 1 and len(iat_dist) == 1:
                if not hard:
                    # soft task executes and remaining computation time > 1
                    if rct_dist[0][0] > 1:
                        possible_states_task_j.append((1, init_config_j, 1))  # initial state

                    else:  # rct_dist[0][0] == 1
                        possible_states_task_j.append((rct_dist[0][1], init_config_j, 0))

                        if len(rct_dist) > 1:  # task not getting finished with probability 1 - rct_dist[0][1]
                            possible_states_task_j.append(((1 - rct_dist[0][1]), init_config_j, 1))

                else:  # hard task
                    # one successor that is the initial config for task j
                    # the probability is 1 since there is only one successor
                    possible_states_task_j.append((1, init_config_j, 0))  # initial state

            else:
                # we reach here if
                # min RCT=1 and len(RCT) > 1 and len(iat) >= 1 (min(iat) > 1 when len(iat)=1) or
                # min(RCT) > 1 and len(iat) > 1 and min(iat) = 1
                if iat_dist[0][0] == 1:  # one of the successor states is the initial state of task i=j
                    # len(iat) > 1 here
                    if rct_dist[0][0] == 1 and len(rct_dist) > 1:
                        possible_states_task_j.append((rct_dist[0][1] * iat_dist[0][1], init_config_j, 0))  # initial state with task finished
                        if hard:
                            possible_states_task_j.append(((1 - rct_dist[0][1]) * iat_dist[0][1], init_config_j, 0))
                        else:
                            possible_states_task_j.append(((1 - rct_dist[0][1]) * iat_dist[0][1], init_config_j, 1))  # initial state with task not finished
                        possible_states_task_j.append((rct_dist[0][1] * (1 - iat_dist[0][1]), [[dec_rct_dist[0]], deadline, dec_iat_dist[1:]], 0))
                        possible_states_task_j.append(((1-rct_dist[0][1]) * (1-iat_dist[0][1]), [dec_rct_dist[1:], deadline, dec_iat_dist[1:]], 0))

                    elif rct_dist[0][0] == 1:  # and len(rct_dist) == 1
                        possible_states_task_j.append((iat_dist[0][1], init_config_j, 0))
                        possible_states_task_j.append((1-iat_dist[0][1], [dec_rct_dist, deadline, dec_iat_dist[1:]], 0))

                    else:  # rct_dist[0][0] > 1
                        if hard:
                            possible_states_task_j.append((iat_dist[0][1], init_config_j, 0))
                        else:
                            possible_states_task_j.append((iat_dist[0][1], init_config_j, 1))
                        possible_states_task_j.append((1-iat_dist[0][1], [dec_rct_dist, deadline, dec_iat_dist[1:]], 0))

                else:  # min(iat) > 1
                    # Here min RCT=1 and len(RCT) > 1
                    possible_states_task_j.append((rct_dist[0][1], [[dec_rct_dist[0]], deadline, dec_iat_dist], 0))
                    possible_states_task_j.append((1-rct_dist[0][1], [dec_rct_dist[1:], deadline, dec_iat_dist], 0))

        else:  # when j!=i is scheduled
            possible_states_task_j = add_possible_states_task_j(rct_dist, iat_dist, init_config_j, deadline,
                                                                dec_iat_dist, hard)
        raw_states.append(possible_states_task_j)

    states = compute_states(raw_states, 1, num_hard_tasks)
    len_tasks = len(tasks)
    # states = filter(lambda (prob, next, bad_flag): len(next)==len_tasks, states)  # filters only valid states: the ones that have n tasks

    states_next = [x[1] for x in states]

    st = []
    for k in range(len(states_next)):
        if len(states_next[k])==len_tasks:
           st.append(states[k])

    # filters only valid states: the ones that have n tasks

    return i, st  # returns a tuple of the form (i, states)



def get_states_execute_none(all_tasks, tasks, num_hard_tasks):
    # This function returns a tuple (-1, states), where states is the set of all states obtained from the current state
    # all_tasks when no task is executed. The slacks can be used by soft tasks

    raw_states = []
    hard = True

    for j in range(len(all_tasks)):

        if j == num_hard_tasks:
            hard = False

        task = all_tasks[j]
        init_config_j = tasks[j][1:4]  # gives rct, deadline, iat in the current config

        # normalize distributions so that the probabilities sum up to 1
        rct_dist = normalize_dist(task[0])
        iat_dist = normalize_dist(task[2])

        dec_rct_dist, deadline, dec_iat_dist = modify_params(rct_dist, task[1], iat_dist)

        possible_states_task_j = add_possible_states_task_j(rct_dist, iat_dist, init_config_j, deadline, dec_iat_dist, hard)
        raw_states.append(possible_states_task_j)

    states = compute_states(raw_states, 1, num_hard_tasks)
    len_tasks = len(tasks)
    # states = filter(lambda (prob, next, bad_flag): len(next)==len_tasks, states)

    states_next = [x[1] for x in states]
    st = []
    for k in range(len(states_next)):
        if len(states_next[k])==len_tasks:
           st.append(states[k])

    return -1, st  # -1 denotes no task has been executed


def get_new_states(all_tasks, tasks):
    # all_tasks is a list of [rct dist, deadline, iat dist] elements, one for each task. It denotes the current state
    # tasks is a list of the task descriptions [arrival, exe dist, deadline, period dist, max_exe_time, min_arrive_time]
    # hard is a flag that is set to True for hard tasks and False for soft tasks

    # This function returns new_states which is a list of elements of the form (i, states), where
    # i -- (start index 0) is the task that is executed, -1 when no task is executed and
    # states -- is the list of states reached from current state all_tasks after executing task i.
    # Every state is a list of the form [rct dist, deadline, iat dist]

    new_states = []
    for i in range(len(all_tasks)):
        # execute task i if min rct > 0

        task_i_rct_dist = all_tasks[i][0]
        task_i_deadline = all_tasks[i][1]
        if task_i_rct_dist[0][0] > 0 and task_i_deadline > 0:
            # get_states_execute_i returns the set of states that are obtained by executing task i from current state all_tasks
            # and are locally known to be safe as guaranteed by function safe, i.e., max rct <= deadline
            new_states.append(get_states_execute_i(all_tasks, i, tasks, len(all_tasks)))

    # add states when no task is executed
    new_states.append((get_states_execute_none(all_tasks, tasks, len(all_tasks))))

    return new_states


def normalize_state(state):

    norm_state = []

    for task in state:
        [rct_dist, deadline, iat_dist] = task
        mod_task = [normalize_dist(rct_dist), deadline, normalize_dist(iat_dist)]
        norm_state.append(mod_task)

    return norm_state


def get_reachable_nodes(G):

    queue = [1]
    visited = []
    while len(queue) > 0:
        (n, queue) = pop_queue(queue)

        if not n in visited:
            succ_nodes = list(G.successors(n))
            queue = queue + succ_nodes
            visited.append(n)

    return visited


def choose_state(next_states_execute_i):
    # next_states_execute_i is a list of states of the form [(prob1, state1, bad_flag_list1), ... (probm, statem, bad_flag_listm)]
    # where m is the number of states that are obtained by executing task i. Here i can be -1 also denoting no task scheduled
    probs = [x[0] for x in next_states_execute_i]
    cum_probs = np.cumsum(probs)  # cumulative sum of probabilities

    random_prob = random.uniform(0, 1)
    # From next_states choose the state whose index i is such that probs[i-1] <= random_prob <= probs[i]
    ind_list = [i for i in range(len(cum_probs)) if cum_probs[i] >= random_prob]

    return next_states_execute_i[ind_list[0]]


def get_next_state(all_tasks, i, tasks, num_hard_tasks):  # all_tasks is the current State, and task i is executed.
    # all_tasks is a list of [rct dist, deadline, iat dist] elements, one for each task. It denotes the current state
    # i is the task to be executed
    # tasks is a list of the task descriptions [arrival, exe dist, deadline, period dist, max_exe_time, min_arrive_time]

    next_states = []
    if i >=0:
        task_i_rct_dist = all_tasks[i][0]
        task_i_deadline = all_tasks[i][1]

        if task_i_rct_dist[0][0] > 0 and task_i_deadline > 0:  # task i can be executed only if its deadline
            # is not over and it has not finished execution
            next_states.append(get_states_execute_i(all_tasks, i, tasks, num_hard_tasks))

    else:
        next_states.append((get_states_execute_none(all_tasks, tasks, num_hard_tasks)))
    # next_states is of the form [(i,states)]; this is a list of only one element
    # next_states[0][1] is states

    next_state = choose_state(next_states[0][1]) # returns a tuple of the form (prob, state, bad_flag_list)

    # state is a list where each element is of the form [[rct dist], deadline, [iat_dist]]
    return normalize_state(next_state[1]), next_state[2]  # returns a tuple of the form (state, bad_flag_list)



def get_init_state(hard_tasks, soft_tasks):
    # add states representing individual tasks

    tasks = hard_tasks + soft_tasks
    # extract the parameters for each task
    init_state = []
    for x in tasks:
        if x[0] > 0:  # initial arrival time > 0
            #task = [0, 0, x[0]]
            # change to fraction
            #task = [[(0,1)], 0, [(x[0],1)]]
            task = [[(0,Fraction(1,1))], 0, [(x[0],Fraction(1,1))]]

        else:
            task = [x[1], x[2], x[3]]  # [RCT dist, deadline, Period dist]

        init_state.append(task)

    # We assume that initially all tasks are safe
    return init_state, [0] * len(tasks)  # [0] denotes bad_flag


def find_exe_time(rct_dist, max_exe_dist):
    if len(rct_dist) == 1 and rct_dist[0][0] == 0:
        # return -2  # a task should be allowed to execute if remaining execution time is positive
        return max_exe_dist[-1][0]
    else:
        return max_exe_dist[-1][0] - rct_dist[-1][0]


def get_safe_actions(G, currentState, hard_tasks):
    # G is a graph of safe states and currentState is a list of tasks, where each task is of the form
    # [rct distribution, deadline, iat distribution]
    # This function returns the safe set of actions from currentState; an action is a hard task or a nop action

    len_hard_tasks = len(hard_tasks)
    exe_times = []
    times_since_arr = []
    max_exe_times = []
    max_arr_times = []
    allowed_actions = []

    for ind in range(len_hard_tasks):
        rct_dist = currentState[ind][0]
        arr_dist = currentState[ind][2]

        max_exe_dist = hard_tasks[ind][1]
        max_arr_dist = hard_tasks[ind][3]

        exe_times.append(find_exe_time(rct_dist, max_exe_dist))

        assert(arr_dist[-1][0] != 0)
        times_since_arr.append((max_arr_dist[-1][0] - arr_dist[-1][0]))

        max_exe_times.append(hard_tasks[ind][4])
        max_arr_times.append(max_arr_dist[-1][0])

    for ind in range(len_hard_tasks):
        if exe_times[ind] >= 0 and currentState[ind][0][0][0] > 0:  # remaining execution time is greater than 0
            if G.decode(exe_times, times_since_arr, max_exe_times, max_arr_times, ind):
                allowed_actions.append(ind)

    if G.decode(exe_times, times_since_arr, max_exe_times, max_arr_times, -1):
        allowed_actions.append(-1)

    return allowed_actions


def get_reward(state, cost_list):
    # This function returns the cost if a soft task does not finish
    bad_flag_list = state[-1]
    bad_flag_list = bad_flag_list[len(bad_flag_list)-len(cost_list):]

    return sum([a*b for a,b in zip(cost_list, bad_flag_list)])


def step(currentState, a, hard_tasks, soft_tasks, cost_list):
    # hard_miss_reward = 10000  # (We are not using this feature now.) This is a high cost; once a task is suggested that does not belong to the set of safe actions
    # Check the set of actions that can be executed from the current state
    # The current state should always be safe here

    # execute the action a and return the next state
    next_state = get_next_state(currentState[0], a, hard_tasks + soft_tasks, len(hard_tasks))

    reward = get_reward(next_state, cost_list)

    currentState = next_state

    return (currentState, reward)  # In our case, the reward is the cost that we want to minimize.


def active(i, all_tasks):
    # all_tasks is a list of [rct dist, deadline, iat dist] elements, one for each task. It denotes the current state
    # i is the task to be executed
    if i == -1:  # nop action is always considered active
        return True
    else:
        task_i_rct_dist = all_tasks[i][0]
        task_i_deadline = all_tasks[i][1]

        if task_i_rct_dist[0][0] > 0 and task_i_deadline > 0:  # task i can be executed only if its deadline
            # is not over and it has not finished execution, that is task i is active
            return True
        else:
            return False


def get_active_soft_tasks(currentState, len_hard_tasks):

    active_tasks = []
    currSoftState = currentState[len_hard_tasks:]
    for i in range(len(currSoftState)):
        if active(i, currSoftState):
            active_tasks.append(i+len_hard_tasks)

    return active_tasks


def main():

    # file_name = raw_input("Enter file name: ")
    file_name = sys.argv[1]
    file_name = file_name.strip()
    hard_tasks, soft_tasks = get_tasks(file_name)  # hard_tasks and soft_tasks are a list of task descriptions: [arrival, exe dist, deadline, period dist, max_exe_time, min_arrive_time] elements

    if len(hard_tasks) > 0:
        # G = construct_graph(self.hard_tasks)
        os.system('python3 encodeTasks.py ' + file_name)
        print('reached here')

        # Compute subgraph for only safe strategies
        # self.G = restrict_to_safe_states(G, len(self.hard_tasks))
        os.system('./abssynthe --out_file strat.aag --win_region safe.aag tasks.aag')
        G = AIG('safe.aag')
        print('and now here with safe actions')

    # Compute antichain
    # task_dict = compute_antichain(G)

    # timesteps = 100000  # dummy value
    num_steps = 1000000

    len_hard_tasks = len(hard_tasks)
    cost_list = [task[-1] for task in soft_tasks]

    final_mean = 0
    num_expts = 1

    for num_exp in range(num_expts):

        # Get the initial state
        #if len(G.nodes()) > 0:
        init_state_tasks = get_init_state(hard_tasks, soft_tasks)

        # simulate the arrival and execution of soft tasks one after another
        current_state = init_state_tasks

        # print('Time to train with ' + str(timesteps) + ' steps and buffer size ' + str(0) + ' : ' + str(0) + ' seconds, Hard deadline misses (dummy for parsing): ' + str(0))

        total_reward = 0

        start_time = time.time()
        for ind in range(num_steps):
            # generate an action randomly from the current state
            if len(hard_tasks) > 0:
                safe_actions = get_safe_actions(G, current_state[0], hard_tasks)  # allowed action is a hard task or -1
            else:
                safe_actions = [-1]

            allowed_actions = safe_actions
            if -1 in allowed_actions:
                allowed_actions = allowed_actions + get_active_soft_tasks(current_state[0], len_hard_tasks)

            if allowed_actions==[]:
                random_action = -1
            else:
                ind = random.randint(0, len(allowed_actions) - 1)
                random_action = allowed_actions[ind]

            current_state, reward = step(current_state, random_action, hard_tasks, soft_tasks, cost_list)

            total_reward += reward

        elapsed_time = time.time() - start_time
        print('Time to simulate with ' + str(num_steps) + ' steps' + ' : ' + str(elapsed_time) + ' seconds, Hard deadline misses : ' + str(0))

        avg_cost = float(total_reward / num_steps)
        print('mean cost ' + str(avg_cost))

        final_mean = final_mean + avg_cost

    final_mean = final_mean / num_expts
    print('final mean cost ' + str(final_mean))


if __name__== "__main__":
    main()
