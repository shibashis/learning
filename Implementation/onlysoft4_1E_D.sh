touch results_onlysoft4_1E_D.txt

# Add some dummy lines so that txt2csv can parse

printf "soft2.txt: actual states 5369 (Storm output: 0.06785261296)" >> results_onlysoft4_1E_D.txt

printf "\r\n" >> results_onlysoft4_1E_D.txt

counter=10000
# while [ $counter -ge 1000 ]
while [ $counter -ge 9001 ]
do
	echo $counter >> results_onlysoft4_1E_D.txt
	for i in {1..10}
	do
		echo $i
		printf "\r\n" >> results_onlysoft4_1E_D.txt
		python3.7 trainTasks.py onlysoft4.txt $counter >> results_onlysoft4_1E_D.txt
		echo "------------------------------------------" >> results_onlysoft4_1E_D.txt
	done
	((counter-=1000))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> results_onlysoft4_1E_D.txt
done

echo "===============================" >> results_onlysoft4_1E_D.txt

python3.7 avg.py results_onlysoft4_1E_D.txt

# This is a dummy file with results of MB learning for soft2.txt that is needed so that txt2csv can parse
# cat dummy_MB.txt >> results_onlysoft4_1E_D.txt

# python3 txt2csv_safe.py results_onlysoft4_1E_D.txt out_onlysoft4_1E_D_tmp.txt

# cut -d';' -f1-2 out_onlysoft4_1E_D_tmp.txt > out_onlysoft4_1E_D.txt
# rm out_onlysoft4_1E_D_tmp.txt

