touch results_onlysoft10E_D_large_simu.txt

counter=1000000
while [ $counter -ge 1000000 ]
do
	echo $counter >> results_onlysoft10E_D_large_simu.txt
	for i in {1..10}
	do
		echo $i
		printf "\r\n" >> results_onlysoft10E_D_large_simu.txt
		python3.7 trainTasks.py onlysoft10.txt $counter >> results_onlysoft10E_D_large_simu.txt
		echo "------------------------------------------" >> results_onlysoft10E_D_large_simu.txt
	done
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> results_onlysoft10E_D_large_simu.txt
	((counter-=1000))
done

echo "===============================" >> results_onlysoft10E_D_large_simu.txt
