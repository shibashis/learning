counter=900
while [ $counter -ge 100 ]
do
	pmfile="hard2model_safe${counter}.pm"
	echo $counter $pmfile
	echo $counter >> resultsTACAS_hard2_3_5.txt
	for i in {1..10}
	do
		echo $i
		python2.7 taskModelBased_safe.py hard2.txt $counter
		printf "\r\n" >> resultsTACAS_hard2_3_5.txt
		storm --prism $pmfile --prop "Rmin=?[LRA]" >> resultsTACAS_hard2_3_5.txt
		rm $pmfile
	done
	((counter-=100))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_hard2_3_5.txt
done
