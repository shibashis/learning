import sys
import matplotlib.pyplot as plt

def parseResults(fName):
    f = open(fName,'r')
    s = f.read()
    f.close()
    s = s.split("\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n")
    d = {}

    tmp = file_name.split(".")
    out_file = tmp[0] + '_summary.txt'
    f = open(out_file, "w+")
    for e in s:
        e = e.split('\n\n')
        if len(e) > 1:
            numSteps = int(e[0])
            total = 0
            numExperiments = 0
            for l in e[1:]:
                if l[0] == 'E':
                    pass
                else:
                    total += float(l)
                    numExperiments += 1
            if numExperiments == 0:
                print (f"{numSteps} : failed")
            else:
                avg = total/numExperiments
                print(f"{numSteps} : {avg}")
                f.write(str(numSteps) + ',' + str(avg) + '\r\n')
                d[numSteps] = avg
    f.close()
    return d

def myPlot(d):
    plt.plot(d.keys(),d.values(),'o')
    plt.show()

if __name__ == "__main__":
    file_name = sys.argv[1]
    file_name = file_name.strip()
    d = parseResults(file_name)
    myPlot(d)
