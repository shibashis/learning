touch results_soft3EDFshield.txt

# Add some dummy lines so that txt2csv can parse

printf "soft2.txt: actual states 5369 (Storm output: 0.06785261296)" >> results_soft3EDFshield.txt

printf "\r\n" >> results_soft3EDFshield.txt

counter=10000
#while [ $counter -ge 1000 ]
while [ $counter -ge 9001 ]
do
	echo $counter >> results_soft3EDFshield.txt
	for i in {1..10}
	do
		echo $i
		printf "\r\n" >> results_soft3EDFshield.txt
		python3.7 trainTasks.py soft3.txt $counter >> results_soft3EDFshield.txt
		echo "------------------------------------------" >> results_soft3EDFshield.txt
	done
	((counter-=1000))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> results_soft3EDFshield.txt
done

echo "===============================" >> results_soft3EDFshield.txt

python3.7 avg.py results_soft3EDFshield.txt

# This is a dummy file with results of MB learning for soft2.txt that is needed so that txt2csv can parse
# cat dummy_MB.txt >> results_soft3EDFshield.txt

# python3.7 txt2csv_safe.py results_soft3EDFshield.txt out_soft3EDFshield_tmp.txt

# cut -d';' -f1-2 out_soft3EDFshield_tmp.txt > out_soft3EDFshield.txt
# rm out_soft3EDFshield_tmp.txt

