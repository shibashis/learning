touch results_soft2_4.txt

counter=450
num=2
while [ $counter -ge 45 ]
do
	steps=$((num * counter))
	txtfile="soft2model${steps}.txt"
	pmfile="soft2model${steps}.pm"
	echo $steps $txtfile $pmfile
	echo $steps >> results_soft2_4.txt
	for i in {1..10}
	do
		echo $i
		python2.7 taskModelBased.py soft2.txt $counter
		python2.7 task.py $txtfile
		printf "\r\n" >> results_soft2_4.txt
		#storm --prism $pmfile --prop "Rmin=?[LRA]" >> results_soft2_4.txt
		python3.7 useOptLearntStrategy.py $pmfile >> results_soft2_4.txt
		rm $txtfile $pmfile
	done
	((counter-=45))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> results_soft2_4.txt
done
