# This works with Python 3.6.5_1, but TensorFlow gives an error with Python 3.7
import gym
import pickle
import numpy as np
import time
import sys
import random

from baselines import deepq
from gym.envs.registration import register


def callback(lcl, _glb):
    # stop training if reward exceeds 199
    is_solved = lcl['t'] > 100 and sum(lcl['episode_rewards'][-101:-1]) / 100 >= 199
    return is_solved


def main():

    # file_name = input("Enter file name: ")
    file_name = sys.argv[1]
    file_name = file_name.strip()

    register(
        id='learnTasks-v0',
        #entry_point='learnTasks:TaskSystemEnv', kwargs={'file_name':file_name},  # observation is exact state space
        #entry_point='learnTasks_safe:TaskSystemEnv', kwargs={'file_name':file_name}, # observation is exact state space
        #entry_point='learnTasksAbs:TaskSystemEnv', kwargs={'file_name': file_name}, # observation: max remaining exe time, deadline, min remaining arrival time
        #entry_point='learnTasks_safeAbs:TaskSystemEnv', kwargs={'file_name': file_name},
        #entry_point='learnTasksDeadline:TaskSystemEnv', kwargs={'file_name': file_name},  # observation: per task remaining time before deadline
        #entry_point='learnTasks_safeDeadline:TaskSystemEnv', kwargs={'file_name': file_name},
        #entry_point='learnTasksSortActiveDL:TaskSystemEnv', kwargs={'file_name': file_name},
        #entry_point='learnTasksED:TaskSystemEnv', kwargs={'file_name': file_name}, # observation: max remaining exe time, deadline
        #entry_point='learnTasksED_debug:TaskSystemEnv', kwargs={'file_name': file_name}, # observation: max remaining exe time, deadline
        #entry_point='learnTasks_safeED:TaskSystemEnv', kwargs={'file_name': file_name},
        #entry_point='learnTasks_safeED_MCTS:TaskSystemEnv', kwargs={'file_name': file_name},
        #entry_point='learnTasks_safeED_AbsSynthe:TaskSystemEnv', kwargs={'file_name': file_name}, # observation: max remaining exe time, deadline
        #entry_point='learnTasksAbs_lax:TaskSystemEnv', kwargs={'file_name': file_name}, # observation: max(0,laxity with max rct) per task
        #entry_point='learnTasksSortActiveLax:TaskSystemEnv', kwargs={'file_name': file_name}, # observation: active tasks sorted on laxity
        entry_point='learnTasksEDFshield:TaskSystemEnv', kwargs={'file_name': file_name}, # EDF as shield, observation: max remaining exe time, deadline, min remaining arrival time
    )

    env = gym.make("learnTasks-v0")

    # timesteps = 10000
    timesteps = int(sys.argv[2])
    buff_size = 2000
    start_time = time.time()
    act = deepq.learn(  # calls the env.step action
        env,
        network='mlp',
        seed=random.randint(0,2**32-1),
        lr=1e-3,
        #total_timesteps=100000,
        total_timesteps=timesteps,
        #buffer_size=50000,
        buffer_size=buff_size,
        exploration_fraction=0.1,
        exploration_final_eps=0.02,
        print_freq=10,
        #gamma=0.9, # default value: 1.0
        callback=callback
        #hiddens=[512,256,256]
    )
    elapsed_time = time.time() - start_time
    print('Time to train with ' + str(timesteps) + ' steps and buffer size ' + str(buff_size) + ' : ' + str(elapsed_time) + ' seconds, Hard deadline misses : ' + str(env.get_deadline_misses()))

    # print("Saving model to tasks_schedule.pkl")
    # act.save() has been removed from baselines tf2 branch that uses Tensorflow2 (needed for python3.8).
    # Tensorflow1.14 does not work with python3.8
    # act.save("tasks_schedule.pkl")

    #print('Training done ')
    # with open('tasks_schedule.pkl', 'rb') as f:
    #    data = pickle.load(f)
    observation = env.reset()
    #num_steps = timesteps
    num_steps = 600
    total_reward = 0

    start_time = time.time()

    # debug why random schedule is better for large task system with soft tasks
    #fname = "debug_" + file_name
    #f = open(fname, "w")
    #f.close()

    for ind in range(num_steps):
        # env.render()
        # The following function act() does not work with baselines tf2 branch.
        action = act(np.array(observation)[None], update_eps=0)[0]

        #f = open(fname, "a")
        #f.write(str(action - 1))
        #f.close()

        observation, reward, done, info = env.step(action)

        total_reward += reward
        #print(reward)

        # print(reward)

        # reset every num_steps / 5 steps

        # if ind % (num_steps /10) == 0:
        #     done = True

        if done:
            observation = env.reset()
            print('reset')

    elapsed_time = time.time() - start_time
    print('Time to simulate with ' + str(num_steps) + ' steps' + ' : ' + str(elapsed_time) + ' seconds, Hard deadline misses : ' + str(env.get_deadline_misses()))

    #f.close()
    env.close()

    avg_cost = float(-1 * total_reward / num_steps)
    print('mean cost ' + str(avg_cost))

    # avg_cost = -1 * total_reward / num_steps
    # print('mean cost ' + str(avg_cost))


if __name__ == '__main__':
    main()
