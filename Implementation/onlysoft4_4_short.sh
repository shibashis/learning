touch resultsTACAS_onlysoft4.txt

counter=125
num=4
while [ $counter -ge 25 ]
do
	steps=$((num * counter))
	txtfile="onlysoft4model${steps}.txt"
	pmfile="onlysoft4model${steps}.pm"
	echo $steps $txtfile $pmfile
	echo $steps >> resultsTACAS_onlysoft4.txt
	for i in {1..10}
	do
		echo $i
		python2.7 taskModelBased.py onlysoft4.txt $counter
		python2.7 task.py $txtfile
		printf "\r\n" >> resultsTACAS_onlysoft4.txt
		storm --prism $pmfile --prop "Rmin=?[LRA]" --precision 0.01 --absolute >> resultsTACAS_onlysoft4.txt
		rm $txtfile $pmfile
	done
	((counter-=25))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_onlysoft4.txt
done
