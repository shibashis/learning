import gym
#import learnTasks
import pickle
import numpy as np
import time
import sys

from baselines import deepq
from gym.envs.registration import register


def callback(lcl, _glb):
    # stop training if reward exceeds 199
    is_solved = lcl['t'] > 100 and sum(lcl['episode_rewards'][-101:-1]) / 100 >= 199
    return is_solved


def main():

    # file_name = input("Enter file name: ")
    file_name = sys.argv[1]
    file_name = file_name.strip()

    register(
        id='learnTasks-v0',
        #entry_point='learnTasksAbs_shield:TaskSystemEnv', kwargs={'file_name': file_name},
        entry_point='learnTasksED_shield:TaskSystemEnv', kwargs={'file_name': file_name},
    )

    env = gym.make("learnTasks-v0")

    # timesteps = 8000
    timesteps = int(sys.argv[2])
    buff_size = 2000
    start_time = time.time()
    act = deepq.learn(  # calls the env.step action
        env,
        network='mlp',
        lr=1e-3,
        #total_timesteps=100000,
        total_timesteps=timesteps,
        #buffer_size=50000,
        buffer_size=buff_size,
        exploration_fraction=0.1,
        exploration_final_eps=0.02,
        print_freq=10,
        callback=callback
    )
    elapsed_time = time.time() - start_time
    print('Time to train with ' + str(timesteps) + ' steps and buffer size ' + str(buff_size) + ' : ' + str(elapsed_time) + ' seconds, Hard deadline misses : ' + str(env.get_deadline_misses()))

    # print("Saving model to tasks_schedule.pkl")
    act.save("tasks_schedule.pkl")

    #print('Training done ')
    # with open('tasks_schedule.pkl', 'rb') as f:
    #    data = pickle.load(f)
    observation = env.reset()
    # num_steps = timesteps
    num_steps = 10000
    total_reward = 0

    start_time = time.time()
    for ind in range(num_steps):
        # env.render()
        action = act(np.array(observation)[None], update_eps=0)[0]

        observation, reward, done, info = env.step_shield(action)

        total_reward += reward
        #print(reward)

        # print(reward)

        # reset every num_steps / 5 steps

        # if ind % (num_steps /10) == 0:
        #     done = True

        if done:
            observation = env.reset()
            print('reset')

    elapsed_time = time.time() - start_time
    # print('Time to simulate with ' + str(num_steps) + ' steps' + ' : ' + str(elapsed_time) + ' seconds, Hard deadline misses : ' + str(env.get_deadline_misses()))
    print('Time to simulate with ' + str(num_steps) + ' steps : ' + str(elapsed_time) + ' seconds')

    env.close()

    avg_cost = float(-1 * total_reward / num_steps)
    print('mean cost ' + str(avg_cost))

    # avg_cost = -1 * total_reward / num_steps
    # print('mean cost ' + str(avg_cost) + '\r\n\r\n')


if __name__ == '__main__':
    main()
