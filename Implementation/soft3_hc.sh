touch results_soft3_hc.txt

# Add some dummy lines so that txt2csv can parse

printf "soft3.txt: safe states 3273 (Storm output: 10.2817493834)" >> results_soft3_hc.txt

printf "\r\n" >> results_soft3_hc.txt

counter=10000
#while [ $counter -ge 1000 ]
while [ $counter -ge 9001 ]
do
	echo $counter >> results_soft3_hc.txt
	for i in {1..10}
	do
		echo $i
		printf "\r\n" >> results_soft3_hc.txt
		python3.7 trainTasksModelCheck.py soft3.txt $counter >> results_soft3_hc.txt
		echo "------------------------------------------" >> results_soft3_hc.txt
	done
	((counter-=1000))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> results_soft3_hc.txt
done

echo "===============================" >> results_soft3_hc.txt

python3.7 avg.py results_soft3_hc.txt

# This is a dummy file with results of MB learning for soft3.txt that is needed so that txt2csv can parse
#cat dummy_MB.txt >> results_soft3_hc.txt

#python3.7 txt2csv_safe.py results_soft3_hc.txt out_soft3_hc_tmp.txt

#cut -d';' -f1-2 out_soft3_hc_tmp.txt > out_soft3_hc.txt
#rm out_soft3_hc_tmp.txt

