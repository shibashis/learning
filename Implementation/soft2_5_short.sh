counter=900
while [ $counter -ge 100 ]
do
	pmfile="soft2model_safe${counter}.pm"
	echo $counter $pmfile
	echo $counter >> resultsTACAS_soft2_3_5.txt
	for i in {1..10}
	do
		echo $i
		python2.7 taskModelBased_safe.py soft2.txt $counter
		printf "\r\n" >> resultsTACAS_soft2_3_5.txt
		storm --prism $pmfile --prop "Rmin=?[LRA]" >> resultsTACAS_soft2_3_5.txt
		rm $pmfile
	done
	((counter-=100))
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> resultsTACAS_soft2_3_5.txt
done
