import sys

f = open(sys.argv[1], "r")

# readlines reads the individual line into a list
fl = f.readlines()
lines = []
f.close()

# for each line, extract the task parameters

total = 0.0
hard_misses = 0
for x in fl:
    y = x.strip()
    if y[0:4] == 'mean':
        val = y.split()
        #print(val[2])
        total += float(val[2])

    if y[0:9] == 'Hard dead':
        val = y.split()
        hard_misses += int(val[3])

print('mean cost average', total/10)
print('mean hard tasks deadlines misses', float(hard_misses)/10.0)