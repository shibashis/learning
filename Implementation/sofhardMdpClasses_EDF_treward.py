# This is for shielded EDF for both hard and soft tasks

import math
import random
import copy

import util
from util import raiseNotDefined

from mdpClasses_treward import *
from learnTasks_safeED_EDF_MCTS import *

def getRewardFromConfig(config):
	state,flag = config
	# insert call to something according to flag
	return 0

class SHTasksCopyFactory(CopyFactory):
	# abstract methods that need to be redefined
	@staticmethod
	def newMDPPredicate(*x):
		return SHTasksMDPPredicate(*x)
	@staticmethod
	def newMDPState(*x):
		return SHTasksMDPState(*x)
	@staticmethod
	def newMDPAction(*x):
		return SHTasksMDPAction(*x)
	@staticmethod
	def newMDPStochasticAction(*x):
		return SHTasksMDPStochasticAction(*x)
	@staticmethod
	def newMDPTransition(*x):
		return SHTasksMDPTransition(*x)
	@staticmethod
	def newMDPPath(*x):
		return SHTasksMDPPath(*x)
	@staticmethod
	def newMDPExecution(*x):
		return SHTasksMDPExecution(*x)
	@staticmethod
	def newMDPOperations(*x):
		return SHTasksMDPOperations(*x)
	@staticmethod
	def newMDPExecutionEngine(*x):
		return SHTasksMDPExecutionEngine(*x)


class SHTasksFileStrFactory(FileStrFactory):
	# abstract methods that need to be redefined
	@staticmethod
	def getMDPPredicate(s:str):
		return SHTasksMDPPredicate.fromFileStr(s)
	@staticmethod
	def getMDPState(s:str):
		return SHTasksMDPState.fromFileStr(s)
	@staticmethod
	def getMDPAction(s:str):
		return SHTasksMDPAction.fromFileStr(s)
	@staticmethod
	def getMDPStochasticAction(s:str):
		return SHTasksMDPStochasticAction.fromFileStr(s)
	@staticmethod
	def getMDPTransition(s:str):
		return SHTasksMDPTransition.fromFileStr(s)
	@staticmethod
	def getMDPPath(s:str):
		return SHTasksMDPPath.fromFileStr(s)
	@staticmethod
	def getMDPExecution(s:str):
		return SHTasksMDPExecution.fromFileStr(s)
	@staticmethod
	def getMDPOperations(s:str):
		return SHTasksMDPOperations.fromFileStr(s)
	@staticmethod
	def getMDPExecutionEngine(s:str):
		return SHTasksMDPExecutionEngine.fromFileStr(s)

def mdpToStr(mdp) -> str:
	# return "MDP"
	raiseNotDefined()

def strToMDP(s:str):
	raiseNotDefined()

class SHTasksMDPEncoding: #SynchronisedSHTasks:
	# Encoding of an MDP with fixed types for states, actions, etc
	# predicateDataType = str # needs to support conversion to and from str
	# stateDataType = intTuple # internal types need to support conversion to and from str
	actionDataType = int # internal types need to support conversion to and from str, hash, == (used as dict key)
	# stochasticActionDataType = intTuple # internal types need to support conversion to and from str, hash, == (used as dict key)
	# mdpDataType : TaskSystemEnv instance

	@staticmethod
	def predicateDataFromStr(s:str):
		return SHTasksMDPEncoding.predicateDataType.fromStr(s)

	@staticmethod
	def stateDataFromStr(s:str):
		raiseNotDefined()
		return SHTasksMDPEncoding.stateDataType.fromStr(s)

	@staticmethod
	def actionDataFromStr(s:str):
		return SHTasksMDPEncoding.actionDataType(s)

	@staticmethod
	def stochasticActionDataFromStr(s:str):
		raiseNotDefined()
		return SHTasksMDPEncoding.stochasticActionDataType.fromStr(s)

	@staticmethod
	def mdpApplyTransition(mdpData, stateData, actionData, stochasticActionData):
		# computes next state and reward for taking a given action and stochastic action
		return stochasticActionData[0], stochasticActionData[1]

	@staticmethod
	def mdpDrawStochasticAction(mdpData, stateData, actionData):
		# draws a stochastic action at random from a state action pair
		mdpData.set_current_state(stateData)
		mdpData.step(actionData)
		return (mdpData.get_current_state(), mdpData.state_reward())

	@staticmethod
	def mdpGetLegalActions(mdpData, stateData):
		# get the list of all legal actions from a given state
		mdpData.set_current_state(stateData)
		return mdpData.get_allowed_actions_EDF()

	@staticmethod
	def getAllPredicates(mdpData, stateData):
		return []

	@staticmethod
	def getPredicates(mdpData, stateData):
		return []

class SHTasksMDPPredicate(MDPPredicate):
	# abstract methods that need to be redefined
	def copyFactory(self):
		return SHTasksCopyFactory
	@classmethod
	def fileStrFactory(cls):
		return SHTasksFileStrFactory
	@classmethod
	def mdpEncoding(cls):
		return SHTasksMDPEncoding

	def __init__(self, data):
		self.data = data
	def deepCopy(self):
		return self.copyFactory().newMDPState(copy.deepcopy(self.data))
	def initFromCopy(self, other):
		self.data = copy.deepcopy(other.data)
	def __str__(self) -> str:
		return "(data:"+str(self.data)+")"

	# abstract methods that can be redefined if needed
	def consoleStr(self) -> str:
		return str(self.data)
	def fastReset(self, fastResetData):
		self.initFromCopy(fastResetData)
	def getFastResetData(self):
		fastResetData=self.deepCopy()
		return fastResetData

	# define if you intend to write or read instances from a text file
	def fileStr(self) -> str:
		return str(self.data)
	@classmethod
	def fromFileStr(cls, s:str):
		# if s=="":
		# 	raise Exception("parsing error:"+s)
		data = cls.mdpEncoding().stateDataFromStr(s)
		return cls(data)

class SHTasksMDPState(MDPState):
	# abstract methods that need to be redefined
	def copyFactory(self):
		return SHTasksCopyFactory
	@classmethod
	def fileStrFactory(cls):
		return SHTasksFileStrFactory
	@classmethod
	def mdpEncoding(cls):
		return SHTasksMDPEncoding

	def __init__(self, data):
		self.data = data
	def deepCopy(self):
		return self.copyFactory().newMDPState(copy.deepcopy(self.data))
	def initFromCopy(self, other):
		self.data = copy.deepcopy(other.data)
	def __str__(self) -> str:
		return "(data:"+str(self.data)+")"

	# abstract methods that can be redefined if needed
	def consoleStr(self) -> str:
		return str(self.data)
	def fastReset(self, fastResetData):
		self.initFromCopy(fastResetData)
	def getFastResetData(self):
		fastResetData=self.deepCopy()
		return fastResetData

	# define if you intend to write or read instances from a text file
	def fileStr(self) -> str:
		return str(self.data)
	@classmethod
	def fromFileStr(cls, s:str):
		# if s=="":
		# 	raise Exception("parsing error:"+s)
		data = cls.mdpEncoding().stateDataFromStr(s)
		return cls(data)


class SHTasksMDPAction(MDPAction):
	# Immutable class
	# abstract methods that need to be redefined
	def copyFactory(self):
		return SHTasksCopyFactory
	@classmethod
	def fileStrFactory(cls):
		return SHTasksFileStrFactory
	@classmethod
	def mdpEncoding(cls):
		return SHTasksMDPEncoding

	STR_SEPARATOR="#"
	FILE_SEPARATOR="\n#infoStr:"
	def __init__(self, data, infoStr:str):
		self.data = data
		self.infoStr=infoStr
	def deepCopy(self):
		return self.copyFactory().newMDPAction(copy.deepcopy(self.data), self.infoStr)
	def __hash__(self) -> int:
		return hash((self.data))
	def __eq__(self, other) -> bool:
		if self is None and other is None:
			return True
		elif self is None or other is None:
			return False
		if not self.data == other.data:
			return False
		return True
	def __str__(self) -> str:
		return "(data:"+str(self.data)+",infoStr:"+str(self.infoStr)+")"

	# abstract methods that can be redefined if needed
	def consoleStr(self) -> str:
		return str(self.data)+(SHTasksMDPAction.STR_SEPARATOR+str(self.infoStr) if self.infoStr!="" else "")
	def miniConsoleStr(self) -> str:
		return str(self.data)

	# define if you intend to write or read instances from a text file
	def fileStr(self) -> str:
		return str(self.data)+(SHTasksMDPAction.FILE_SEPARATOR+str(self.infoStr) if self.infoStr!="" else "")
	@classmethod
	def fromFileStr(cls, s:str):
		splits=s.split(SHTasksMDPAction.FILE_SEPARATOR)
		if len(split) != 2:
			raise Exception("parsing error:"+s)
		data = cls.mdpEncoding().actionDataFromStr(split[0])
		infoStr = split[1]
		return cls(data,infoStr)


class SHTasksMDPStochasticAction(MDPStochasticAction):
	# Immutable class
	# abstract methods that need to be redefined
	def copyFactory(self):
		return SHTasksCopyFactory
	@classmethod
	def fileStrFactory(cls):
		return SHTasksFileStrFactory
	@classmethod
	def mdpEncoding(cls):
		return SHTasksMDPEncoding

	STR_SEPARATOR="#"
	FILE_SEPARATOR="\n#infoStr:"
	def __init__(self, data, infoStr:str):
		self.data = data
		self.infoStr=infoStr
	def deepCopy(self):
		return self.copyFactory().newMDPStochasticAction(copy.deepcopy(self.data), self.infoStr)
	def __hash__(self) -> int:
		return hash((self.data))
	def __eq__(self, other) -> bool:
		if self is None and other is None:
			return True
		elif self is None or other is None:
			return False
		if not self.data == other.data:
			return False
		return True
	def __str__(self) -> str:
		return "(data:"+str(self.data)+",infoStr:"+str(self.infoStr)+")"

	# abstract methods that can be redefined if needed
	def consoleStr(self) -> str:
		return str(self.data)+(SHTasksMDPStochasticAction.STR_SEPARATOR+str(self.infoStr) if self.infoStr!="" else "")
	def miniConsoleStr(self) -> str:
		return str(self.data)

	# define if you intend to write or read instances from a text file
	def fileStr(self) -> str:
		return str(self.data)+(SHTasksMDPStochasticAction.FILE_SEPARATOR+str(self.infoStr) if self.infoStr!="" else "")
	@classmethod
	def fromFileStr(cls, s:str):
		splits=s.split(SHTasksMDPStochasticAction.FILE_SEPARATOR)
		if len(split) != 2:
			raise Exception("parsing error:"+s)
		data = cls.mdpEncoding().stochasticActionDataFromStr(split[0])
		infoStr = split[1]
		return cls(data,infoStr)


class SHTasksMDPTransition(MDPTransition):
	# Immutable class
	# abstract methods that need to be redefined
	def copyFactory(self):
		return SHTasksCopyFactory
	@classmethod
	def fileStrFactory(cls):
		return SHTasksFileStrFactory


class SHTasksMDPPath(MDPPath):
	# abstract methods that need to be redefined
	def copyFactory(self):
		return SHTasksCopyFactory
	@classmethod
	def fileStrFactory(cls):
		return SHTasksFileStrFactory


class SHTasksMDPExecution(MDPExecution):
	# abstract methods that need to be redefined
	def copyFactory(self):
		return SHTasksCopyFactory
	def fileStrFactory(self):
		return SHTasksFileStrFactory


class SHTasksMDPOperations(MDPOperations):
	# abstract methods that need to be redefined
	def copyFactory(self):
		return SHTasksCopyFactory
	@classmethod
	def fileStrFactory(cls):
		return SHTasksFileStrFactory
	@classmethod
	def mdpEncoding(cls):
		return SHTasksMDPEncoding

	def __init__(self, data, discountFactor:float):
		self.data = data
		self.discountFactor=discountFactor
	def deepCopy(self):
		return self.copyFactory().newMDPOperations(copy.deepcopy(self.data),self.discountFactor)
	def __str__(self) -> str:
		return "(data:"+str(self.data)+",discountFactor:"+str(self.discountFactor)+")"

	def applyTransitionOnState(self, mdpState, mdpTransition):
		# make changes to mdpState according to mdpTransition, and return the reward of this transition
		newStateData, mdpReward = SHTasksMDPOperations.mdpEncoding().mdpApplyTransition(self.data, mdpState.data, mdpTransition.mdpAction.data, mdpTransition.mdpStochasticAction.data)
		mdpState.data = newStateData
		return mdpReward
	def drawTransition(self, mdpState, mdpAction):
		# draw a stochastic transition according to the MDP's distributions
		stochasticActionData = SHTasksMDPOperations.mdpEncoding().mdpDrawStochasticAction(self.data, mdpState.data, mdpAction.data)
		mdpStochasticAction=self.copyFactory().newMDPStochasticAction(stochasticActionData,"")
		return self.copyFactory().newMDPTransition(mdpAction,mdpStochasticAction)
	def getLegalActions(self, mdpState):
		legalActions = [self.copyFactory().newMDPAction(actionData,"") for actionData in SHTasksMDPOperations.mdpEncoding().mdpGetLegalActions(self.data, mdpState.data)]
		return legalActions


	# abstract methods that can be redefined if needed
	def consoleStr(self) -> str:
		return str(self)

	def getAllPredicates(self, mdpState):
		predicateDatas = SHTasksMDPOperations.mdpEncoding().getAllPredicates(self.data, mdpState.data)
		mdpPredicates=[self.copyFactory().newMDPPredicate(data) for data in predicateDatas]
		return mdpPredicates
	def getPredicates(self, mdpState):
		predicateDatas = SHTasksMDPOperations.mdpEncoding().getPredicates(self.data, mdpState.data)
		mdpPredicates=[self.copyFactory().newMDPPredicate(data) for data in predicateDatas]
		return mdpPredicates


	def isExecutionTerminal(self, mdpExecution) -> bool:
		# Redefine this method if you want a notion of terminal state/path
		isTerminal = False
		# is the execution over? (terminal state reached, horizon reached, etc)
		return isTerminal

	def getHorizonReward(self, mdpState):
		return self.data.state_terminal_reward(mdpState.data)

	# def getHorizonReward(self, mdpState):
	# 	# return the sum of the laxities
	# 	mdpHorizonReward = 0
	# 	for i in range(len(mdpState.data[0])):
	# 		mdpHorizonReward += mdpState.data[0][i][1] - mdpState.data[0][i][0][-1][0]
	# 	return mdpHorizonReward

	def getTerminalReward(self, mdpExecution) -> float:
		# Redefine this method if you want a notion of terminal reward
		mdpTerminalReward=0
		# reward for terminal states
		return mdpTerminalReward * mdpExecution.discountFactor
	def applyTransition(self, mdpExecution, mdpTransition):
		# Redefine this method if you want something else than discounted total reward
		mdpReward = 0
		isTerminal = mdpExecution.isTerminal
		if isTerminal:
			raise Exception("applying transition to terminal execution")
		mdpState = mdpExecution.mdpEndState
		# make changes to mdpState according to mdpTransition, and set mdpReward accordingly
		mdpReward = self.applyTransitionOnState(mdpState, mdpTransition)
		# update mdpExecution
		mdpExecution.discountFactor *= self.discountFactor
		mdpExecution.mdpPathReward += mdpReward * mdpExecution.discountFactor
		isTerminal = self.isExecutionTerminal(mdpExecution)
		mdpExecution.isTerminal = isTerminal
		if isTerminal:
			mdpExecution.mdpPathReward += self.getTerminalReward(mdpExecution)

	# define if you intend to write or read instances from a text file
	def fileStr(self) -> str:
		raiseNotDefined()
		return "d"+str(self.discountFactor)
	@classmethod
	def fromFileStr(cls, s:str):
		raiseNotDefined()
		if s[:1]!="d":
			raise Exception("bad str provided")
		discountFactor=float(s[1:])
		return cls(discountFactor)


class SHTasksMDPExecutionEngine(MDPExecutionEngine):
	# abstract methods that need to be redefined
	def copyFactory(self):
		return SHTasksCopyFactory
	def fileStrFactory(self):
		return SHTasksFileStrFactory

	# abstract methods that can be redefined if needed
	def __init__(self, mdpOperations, mdpExecution):
		self.mdpOperations = mdpOperations # an MDPOperations instance
		self.mdpExecution = mdpExecution # an MDPExecution instance
	def deepCopy(self):
		return self.copyFactory().newMDPExecutionEngine(self.mdpOperations.deepCopy(),self.mdpExecution.deepCopy())
	def __str__(self) -> str:
		return "(mdpOperations:"+str(self.mdpOperations)+", mdpExecution:"+str(self.mdpExecution) + ")"

	def consoleStr(self) -> str:
		return self.mdpOperations.consoleStr()+"\n"+self.mdpExecution.consoleStr()
	def executionCopy(self):
		"""
		half-shallow copy of an MDPExecutionEngine instance. Can be used to run independent executions on the same MDP.
		"""
		return self.copyFactory().newMDPExecutionEngine(self.mdpOperations,self.mdpExecution.executionCopy())

	def append(self, mdpTransition):
		self.mdpOperations.applyTransition(self.mdpExecution,mdpTransition)
		mdpPredicates = self.mdpOperations.getPredicates(self.mdpEndState())
		self.mdpExecution.append(mdpTransition, mdpPredicates)
	def appendPath(self, mdpPath):
		for i in range(mdpPath.length()):
			self.append(mdpPath.mdpTransitionsSequence[i])

	def fastReset(self, fastResetData):
		fastResetDataExecution=fastResetData
		self.mdpExecution.fastReset(fastResetDataExecution)
	def getFastResetData(self):
		return (self.mdpExecution.getFastResetData())

def getMCTSEngine(initialStateData, initialPredicateDatas, mdpData, mctsConstant, quiet=False):
	state=SHTasksMDPState(initialStateData)
	predicates=[SHTasksMDPPredicate(p) for p in initialPredicateDatas]
	mdp=SHTasksMDPOperations(mdpData,1)
	initState=state.deepCopy()
	endState=state.deepCopy()
	mdpPath=SHTasksMDPPath(initState,[],[predicates])
	execEngine=SHTasksMDPExecutionEngine(mdp,SHTasksMDPExecution(mdpPath,endState,0,False,1))
	strategySelection = MDPUniformActionStrategy()
	adviceSelection = MDPFullActionAdvice()
	strategySimulation = MDPUniformActionStrategy()
	adviceSimulation = MDPFullActionAdvice()
	pathAdviceSimulation = MDPFullPathAdvice()
	mdpStateScore = MDPStateScore()
	mctsEngine = MCTSEngine(execEngine, strategySelection, strategySimulation, adviceSelection, adviceSimulation, pathAdviceSimulation, mdpStateScore, mctsConstant = mctsConstant, quiet=quiet)
	return mctsEngine


import networkx

if __name__ == '__main__':

	mdpData = TaskSystemEnv(sys.argv[1])

	gameTotalLength=int(sys.argv[2])

	# number of mcts iterations
	numMCTSIters = 100
	# number of simulations for each new node
	numSims = 100
	# horizon for total reward
	horizon = 30
	# coefficient for endstate score
	alpha = 0.4
	# mcts constant
	#mctsConstant = math.sqrt(2)/2
	mctsConstant = 30

	file_str = '_' + str(numMCTSIters) + '_' + str(numSims) + '_' + str(horizon) + '_' + str(alpha) + '_' + str(gameTotalLength)

	file_name = sys.argv[1]
	out_file = file_name.split(".")
	op_file = out_file[0] + file_str + '.txt'
	f = open(op_file, "w+")
	total_cost = 0

	for i in range(gameTotalLength):
		print("state",i,":",mdpData.get_current_state())
		current_copy = copy.deepcopy(mdpData.get_current_state())

		initialStateData = mdpData.get_current_state()
		initialPredicateDatas = []
		mctsEngine = getMCTSEngine(initialStateData, initialPredicateDatas, mdpData, mctsConstant, quiet=True)
		mctsEngine.doMCTSIterations(numMCTSIters, numSims, horizon, alpha)
		action = mctsEngine.getMCTSRootAction()
		# mdpRewardEstimate=mctsEngine.getMCTSRootReward(action)
		# print("mdpRewardEstimate for",action.consoleStr(),":",mdpRewardEstimate)
		# averageRewardEstimate=mdpRewardEstimate/horizon
		# print("mean payoff estimate for",action.consoleStr(),":",averageRewardEstimate)
		print("mcts chose action",action.data)
		mdpData.set_current_state(current_copy)
		mdpData.step(action.data)
		reward = mdpData.state_reward()
		print("reward for this step",reward)
		total_cost = total_cost + reward

		if i%100 == 99:
			avg_cost = -1 * total_cost / (i+1)
			f.write("Steps: " + str(i+1) + ", Average cost: " + str(avg_cost) +"\r\n")
			f.flush()

	# action is an optimal action
	# action = SHTasksMDPAction(1,"")
	# mdpRewardEstimate=mctsEngine.getMCTSRootReward(action)
	# print("mdpRewardEstimate for",action.consoleStr(),":",mdpRewardEstimate)
	# averageRewardEstimate=mdpRewardEstimate/horizon
	# print("mean payoff estimate for",action.consoleStr(),":",averageRewardEstimate)
	f.close()
	avg_cost = -1*total_cost / gameTotalLength
	print("Average cost: " + str(avg_cost))
	pass
