import numpy as np
import random
from fractions import Fraction
from decimal import Decimal
import sys


def get_tasks(file_name):
    f=open(file_name, "r")

    #readlines reads the individual line into a list
    fl =f.readlines()
    lines=[]
    tasks= []
    hard_tasks= []
    soft_tasks= []
    has_soft_tasks = False

    f.close()

    # for each line, extract the task parameters

    for x in fl:
        y = x.strip()

        if y[0] == '-':
            hard_tasks = tasks
            tasks = []
            has_soft_tasks = True

        elif y!='' and y[0]!='#':
            lines.append(y)
            task = y.split("|")  # task should have 4 elements

            arrival = int(task[0])

            dist = task[1].split(";")  # distribution on the execution time
            exe = []
            max_exe_time = 0
            for z in dist:
                z = z.strip("[")
                z = z.strip("]")
                z = z.split(",")
                time = int(z[0])

                if time > max_exe_time:  # compute maximum execution time
                    max_exe_time = time

                # change to fraction since float arithmetic produces bad precision
                #prob = float(z[1])
                #exe.append((time, prob))
                exe.append((time, Fraction(Decimal(z[1]))))

            deadline = int(task[2])

            dist = task[3].split(";")  # distribution on the period
            period = []
            arrive_time = []
            for z in dist:
                z = z.strip("[")
                z = z.strip("]")
                z = z.split(",")
                time = int(z[0])

                # change to fraction since float arithmetic produces bad precision
                #prob = float(z[1])
                #period.append((time, prob))
                period.append((time, Fraction(Decimal(z[1]))))

                arrive_time.append(time)

            min_arrive_time = min(arrive_time)

            if has_soft_tasks:
                cost = Fraction(Decimal(task[4]))
                tasks.append([arrival, exe, deadline, period, max_exe_time, min_arrive_time, cost])
            else:
                tasks.append([arrival, exe, deadline, period, max_exe_time, min_arrive_time])

    if has_soft_tasks:
        soft_tasks = tasks
    else:
        hard_tasks = tasks

    return hard_tasks, soft_tasks


# learn execution time and inter-arrival time distributions
def learn_tasks(tasks, time_step):

    total_jobs_per_task = len(tasks) * [0]
    len_tasks = len(tasks)
    exe_times = []
    arr_times = []
    exe_probs = []
    arr_probs = []

    arrival = [x[0] for x in tasks]
    exe_dist = [x[1] for x in tasks]
    arr_dist = [x[3] for x in tasks]

    for i in range(len_tasks):
        exe_times = exe_times + [len(exe_dist[i]) * [0]]
        arr_times = arr_times + [len(arr_dist[i]) * [0]]
        exe_probs = exe_probs + [len(exe_dist[i]) * [0]]
        arr_probs = arr_probs + [len(arr_dist[i]) * [0]]

    # we learn all tasks simultaneously to make it order independent
    for t in range(time_step):
        for i in range(len_tasks):
            if arrival[i] == t:
                total_jobs_per_task[i] = total_jobs_per_task[i] + 1
                # randomly generate the arrival time following the distribution
                probs = [x[1] for x in arr_dist[i]]
                cum_probs = np.cumsum(probs)  # cumulative sum of probabilities

                random_prob = random.uniform(0, 1)
                # From next_states choose the state whose index i is such that probs[i-1] <= random_prob <= probs[i]
                ind_list = [j for j in range(len(cum_probs)) if cum_probs[j] > random_prob]
                arrival[i] = t + arr_dist[i][ind_list[0]][0]
                arr_times[i][ind_list[0]] = arr_times[i][ind_list[0]] + 1

                # randomly generate the execution time following the distribution
                probs = [x[1] for x in exe_dist[i]]
                cum_probs = np.cumsum(probs)  # cumulative sum of probabilities

                random_prob = random.uniform(0, 1)
                # From next_states choose the state whose index i is such that probs[i-1] <= random_prob <= probs[i]
                ind_list = [j for j in range(len(cum_probs)) if cum_probs[j] > random_prob]
                exe_times[i][ind_list[0]] = exe_times[i][ind_list[0]] + 1

    for i in range(len_tasks):
        for j in range(len(exe_times[i])):
            exe_probs[i][j] = round(float(exe_times[i][j]) / total_jobs_per_task[i], 4) # 4 places after decimal

        # due to fractional error, the sum may not be equal to 1. In that case, change the probability of the last element
        sum_exe_probs = sum(exe_probs[i])
        if sum_exe_probs > 1.0:
            exe_probs[i][j] -= (sum_exe_probs-1.0)
        if sum_exe_probs < 1.0:
            exe_probs[i][j] += (1.0-sum_exe_probs)
        #sum_exe_probs = sum(exe_probs[i])
        #if sum_exe_probs < 1.0:
            #print("probabilities do not sum up to 1" + sum_exe_probs)

        for j in range(len(arr_times[i])):
            arr_probs[i][j] = round(float(arr_times[i][j]) / total_jobs_per_task[i], 4) # 4 places after decimal

        sum_arr_probs = sum(arr_probs[i])
        if sum_arr_probs > 1.0:
            arr_probs[i][j] -= (sum_arr_probs-1.0)
        if sum_arr_probs < 1.0:
            arr_probs[i][j] += (1.0-sum_arr_probs)
        #sum_arr_probs = sum(arr_probs[i])
        #if sum_arr_probs < 1.0:
            #print("probabilities do not sum up to 1" + sum_arr_probs)

    return exe_probs, arr_probs



def generate_file(out_file, learnt_task_dists, hard_tasks, soft_tasks):

    f = open(out_file, "w+")

    len_hard_tasks = len(hard_tasks)
    len_soft_tasks = len(soft_tasks)

    for i in range(len(hard_tasks)):
        [arrival, exe, deadline, period, max_exe_time, min_arrive_time] = hard_tasks[i]

        exe_str = ''
        for j in range(len(exe)):
            exe_str = exe_str + str(exe[j][0]) + ',' + str(float(exe[j][1]))

            if j < len(exe) - 1:
                exe_str = exe_str + ';'

        period_str = ''
        for j in range(len(period)):
            period_str = period_str + str(period[j][0]) + ',' + str(float(period[j][1]))

            if j < len(period) - 1:
                period_str = period_str + ';'

        f.write(str(arrival) + '|[' + exe_str + ']|' + str(deadline) + '|[' + period_str + ']\r\n')

    f.write('----------------------------\r\n')

    for i in range(len(soft_tasks)):
        [arrival, exe, deadline, period, max_exe_time, min_arrive_time, cost] = soft_tasks[i]

        exe_str = ''
        for j in range(len(exe)):
            # do not print if the probability that is learnt is 0 for a specific support in the initial dist
            if learnt_task_dists[0][i][j] > 0:
                exe_str = exe_str + str(exe[j][0]) + ',' + str(learnt_task_dists[0][i][j])

            if learnt_task_dists[0][i][j] > 0 and j < len(exe) - 1:
                exe_str = exe_str + ';'

        if exe_str[-1] == ';':
            exe_str = exe_str[:-1]

        period_str = ''
        for j in range(len(period)):
            # do not print if the probability that is learnt is 0 for a specific support in the initial dist
            if learnt_task_dists[1][i][j] > 0:
                period_str = period_str + str(period[j][0]) + ',' + str(learnt_task_dists[1][i][j])

            if learnt_task_dists[1][i][j] > 0 and j < len(period) - 1:
                period_str = period_str + ';'

        if period_str[-1] == ';':
            period_str = period_str[:-1]

        f.write(str(arrival) + '|[' + exe_str + ']|' + str(deadline) + '|[' + period_str + ']|' + str(cost) + '\r\n')

    f.close()



def main():

    # file_name = raw_input("Enter file name: ")
    file_name = sys.argv[1]
    file_name = file_name.strip()
    hard_tasks, soft_tasks = get_tasks(file_name)  # hard_tasks and soft_tasks are a list of task descriptions: [arrival, exe dist, deadline, period dist, max_exe_time, min_arrive_time] elements

    # We want to simulate the tasks and learn over so many steps
    # Assuming that we learn n tasks, the actual number of time_step is time_step*n
    # time_step = 1000
    time_step = int(sys.argv[2])
    # tasks = hard_tasks + soft_tasks

    learnt_task_dists = learn_tasks(soft_tasks, time_step)

    out_file = file_name.split(".")
    generate_file(out_file[0] + 'model' + str(time_step*len(soft_tasks)) + '.txt', learnt_task_dists, hard_tasks, soft_tasks)


if __name__== "__main__":
    main()
