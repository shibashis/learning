import gym
import learnTasks
import pickle
import numpy as np

from baselines import deepq
from gym.envs.registration import register


def callback(lcl, _glb):
    # stop training if reward exceeds 199
    is_solved = lcl['t'] > 100 and sum(lcl['episode_rewards'][-101:-1]) / 100 >= 199
    return is_solved


def main():

    file_name = input("Enter file name: ")
    file_name = file_name.strip()

    register(
        id='learnTasks-v0',
        #entry_point='learnTasks_safe:TaskSystemEnv', kwargs={'file_name':file_name},
        entry_point='learnTasks:TaskSystemEnv', kwargs={'file_name': file_name},
    )

    env = gym.make("learnTasks-v0")
    act = deepq.learn(  # calls the env.step action
        env,
        network='mlp',
        lr=1e-3,
        #total_timesteps=100000,
        total_timesteps=10000,
        #buffer_size=50000,
        buffer_size=5000,
        exploration_fraction=0.1,
        exploration_final_eps=0.02,
        print_freq=10,
        callback=callback
    )
    # print("Saving model to tasks_schedule.pkl")
    act.save("tasks_schedule.pkl")

    # with open('tasks_schedule.pkl', 'rb') as f:
    #    data = pickle.load(f)
    observation = env.reset()
    num_steps = 10000
    total_reward = 0
    for ind in range(num_steps):
        # env.render()
        action = act(np.array(observation)[None], update_eps=0)[0]
        observation, reward, done, info = env.step(action)

        total_reward += reward
        #print(reward)

        # print(reward)

        # reset every num_steps / 10 steps

        if ind % (num_steps /10) == 0:
            done = True

        if done:
            observation = env.reset()
            print('reset')

    avg_cost = -1 * total_reward / num_steps
    print('mean cost ' + str(avg_cost))

    # Get action corresponding to each observation (state)
    # Construct the set of states as product



    # For each key in the dictionary, get the state (value) and the action
    # Store the state action pair in a new dictionary stateact


    # For each real state that does not appear in the dictionary assign an action randomly and store it in the dictionary statect


    if len(hard_tasks) > 0:
        G = construct_graph(hard_tasks, True)

        # Compute subgraph for only safe strategies
        # G = restrict_to_safe_states(G, len(hard_tasks))

        print("%d Safe nodes" % len(G.nodes()))

    # if there exists some safe and reachable node
    if (len(hard_tasks) == 0) or (len(hard_tasks) > 0 and len(G.nodes()) > 0):
        if len(hard_tasks) > 0:
            task_graphs_list = [G] + construct_graphs_soft_tasks(soft_tasks)
        else:
            task_graphs_list = construct_graphs_soft_tasks(soft_tasks)

        # Generate the prism file
        out_file = file_name.split(".")
        generate_prism_file(task_graphs_list, out_file[0] + '.pm', hard_tasks, soft_tasks)  # .pm extension for mdp

    env.close()

if __name__ == '__main__':
    main()
