%\section{Modelling the system as an MDP}\label{sec:modelling}
For a task $\tau_i$ for $i \in [n]$, and a cost function $cost$ we model our scheduling problem by means of an MDP $\Gamma_{\tau_i}$. 
This will provide us with a precise and formal definition of the problem, and will allow us to rely on automatic tools (such as {\sc Storm} \cite{DJKV17}, see Section~\ref{sec:impl-exper}) to solve it. 
In order to define $\Gamma_{\tau_i}$, it is easier to first build an \emph{infinite} MDP $\Gamma=\zug{V,E,L,(V_\Box, V_\ocircle),\ell, Prob}$. 
Then, we define $\Gamma_{\tau_i}$ as the (finite) portion of $\Gamma$ that is reachable from some designated initial state $v_{\sf init}$.
In our model, $\playerOne$ models the \emph{Scheduler} and $\playerTwo$ models the task generator (abbreviated \emph{TaskGen}).


\textbf{Modelling the system states.} Since  $D_i\leq \min(\supp(\A_i))$ for all tasks $\tau_i$, there can be at most one job of each task at a time $t$ that can be scheduled at $t$.
Thus when the system executes, we keep information related to only one job of task $\tau_i$.
For task $\tau_i$, at every time, in the vertices of the MDP we maintain the following information about the current job in the system at that time:
\begin{inparaenum}[(i)]
\item a \emph{distribution} $c_i$ over the job's possible remaining computation times (rct);
\item the time $d_i$ up to its deadline; and
\item a distribution $a_i$ over the possible times up to the next arrival of a new job of $\tau_i$.
\end{inparaenum}
We also have a special vertex $\bot$ that will be reached when a hard task misses a deadline. 
Let 
${\Cmax}_i=\max(\supp(\C_i)))$, 
${\Amax}_i=\max(\supp(\A_i))$.
Formally $V_\playerOne=\big(\D([{\Cmax}_i]_0)\times [D_i]_0\times\D([{\Amax}_i]_0)\big) \times\{\playerOne\}\cup \{\bot\}$ and
$V_\playerTwo=\big(\D([{\Cmax}_i]_0)\times [D_i]_0\times\D([{\Amax}_i]_0)\big) \times\{\playerTwo\}$;
For a vertex $v=(c_j, d_j, a_j)$, we say that task $\tau_i$ is active in vertex $v$ if $c_j(0)\neq 1\text{ and }d_j>0\}$, and $\tau_i$ has missed its deadline if $c_j(0)=0\text{ and } d_j=0\}$.

In $v$, for every task $i \in [n]$, the tuple $(c_i, d_i, a_i)$ is called its \emph{configuration}.

\textbf{Distribution updates.} We now introduce the $\decr$ and $\upd$ functions that will be useful when we will need to update the knowledge of the Scheduler.
For example, consider a state where $c(1)=0.5$, $c(4)=0.1$ and $c(5)=0.4$ and $\tau_i$ is granted one CPU time unit.
Then, all elements in the support of $c$ should be decremented, yielding $c'$ with $c'(0)=0.5$, $c'(3)=0.1$ and $c'(4)=0.4$. 
Since $0\in\supp(c')$, the current job of $\tau_i$ \emph{could} now terminate with probability $c'(0)=0.5$, or continue running, which will be observed by  Scheduler. 
In the case where the job does not terminate, the probability mass must be redistributed to update Scheduler's knowledge, yielding the distribution $c''$ with$c''(3)=0.2$ and $c''(4)=0.8$.
Formally, let $p$ and $p'$ be probability distributions on~$\N$ s.t. $0\not\in\supp(p)$.
Then, we let $\decr(p)$ and $\upd(p')$ be probability distributions on $\{x-1\mid x\in \supp(p)\}$ and $\supp(p')\setminus \{0\}$ respectively s.t.:
\begin{align*}
  \text{for all }x\in \supp(p) : \decr(p)(x-1) &= p(x)\\
  \text{for all }x\in \supp(p')\setminus \{0\} : \upd(p')(x) &= \frac{p'(x)}{\sum_{x \geq 1}p'(x)}.
\end{align*}
Observe that, when $0\not\in \supp(p')$, then $\upd(p')=p'$.

\textbf{Possible moves of the Scheduler.} 
Given a task $\tau_i$, at every CPU tick, Scheduler has two possible actions, either to schedule the current job of task $\tau_i$ if $\tau_i$ is active or not schedule it.
We model this by having, from all states $v\in V_\playerOne$ one transition labelled by $i$, or by $\varepsilon$ (no job gets scheduled). 
The effect of the transition models the elapsing of one clock tick.

Formally, fix $v=(c,d,a)\in V_\playerOne$ such that $0\notin \supp(c)$ and $0\not\in \supp(a)$.
Then, there is $e=(v,v')\in E$ with $L(e)\in [i] \cup \{\varepsilon\}$ and $v'=(c',d',a')$ iff the following four conditions hold:
\begin{inparaenum}[(i)]
\item $L(e)=i\in [n]$ implies that the task $\tau_i$ is active and $c_i'=\decr(c_i)$, i.e. if a task is scheduled, it must be active, and its rct is decremented; 
%\item for all $j\in [n]\setminus\{L(e)\}$: $c_j'=c_j$, i.e. the rct of all the other tasks does not change;
\item $d'=\max(d-1,0)$, i.e. the deadline is one time unit closer, if not reached yet;
\item $a'=\decr(a)$, i.e. we decrement the time to next arrival of all tasks.
\end{inparaenum}
Observe that when a \emph{soft} task misses a deadline, we maintain the positive rct of the job in the next state: this will be used as a marker to ensure that the associated cost is paid when a new job of the same task will arrive. For example, consider $\tau_i$ to be a soft task, and let a state be $\big((c, 0, a),\playerOne\big)$ with $c(2)=1$ and $a(1)=1$ i.e. the task has reached its deadline but still need 2 time units to complete and the next job arrives in 1 time unit.
Then, the successor state will be $\big((c,0, a'),\playerTwo\big)$ with $a'(0)=1$, from which a new job will be submitted, which will incur a cost (see action $killANDsub$ in the next paragraph).


\textbf{Possible moves of the Task Generator.} The moves of TaskGen (modelled by $\playerTwo$) consist in selecting, one possible \emph{action} out of four: either
\begin{inparaenum}[(i)]
  nothing ($\varepsilon$); or
\item  to finish the current job without submitting a new one ($fin$); or 
\item to submit a new job while the previous one is already finished ($sub$); or
\item to submit a new job and kill the previous one, in the case of a soft task ($killANDsub$), which will incur a cost.
\end{inparaenum}

Formally, let $Actions=\{fin,sub,killANDsub,\varepsilon\}$.  
To define $\Gamma_{\tau_i}$, we introduce a function $\L: (V_{\ocircle} \times V_{\Box}) \mapsto Actions$, 
%Edges from states in $V_\playerTwo$ are labelled by elements from $Actions^n$,}
i.e. $\L(e)$ is the action of $\playerTwo$ on edge $e$.
Fix a state $v=\big((c,d,a) ,\playerTwo\big)\in V_\playerTwo$. 
Let $\hat{v} = \big((\hat{c_1}, \hat{d_1}, \hat{a_1}), \Box \big) \in V_{\Box}$ be such that that $\hat{v} \to{i} v$.
Note that there is a unique such $\hat{v}$ from which action $i$ can be done to reach $v$.
We consider two cases.
If task $\tau_i$ is a hard task and misses its deadline, then the only transition from $v$ is $e=(v,\bot)$ with $\L(e)=\varepsilon$.
Otherwise, we have an edge $e=(v,v')$ with $v'=\big((c',d',a'),\playerOne\big)$ iff one of the following holds:
\begin{enumerate}
\item 
%    \track{(I remove the condition $i\in\act(v)$ since rct has already been decremented by the Scheduler action and it is possible to have $c_i(0)=1$.)}
$\L(e,i) = fin$, $\min(\supp(\hat{c})=1)$, $a(0)\neq 1$, $c'(0)=1$, $d'=d$, and  $a'=\upd(a)$.
The current job of $\tau_i$ finishes now ($c_i(0)>0$) and the next arrival will occur in the future ($a_i(0)\neq 1$), according to the probability distribution $\upd(a_i)$.
\item $\L(e) = sub$, $c(0)>0$, $a(0)>0$, $c'=\C$, $d'=D$, and $a'=\A$.
In this case, we assume that the previous job of $\tau_i$ has completed ($c(0)>0$) and we let $\tau_i$ submit a new job (see the new values $c'$, $d'$ and $a'$); or
\item $\L(e)=killANDsub$, $c(0)\neq 1$, $a(0)>0$, $c'=\C$, $d'=D$, and $a'=\A$. 
In this case, $\tau_i$ (necessarily a soft task) submits a new job, and kills the previous one (there is possibly some remaining rct as $c_i(0)\neq 1$); or
\item $\L(e)=\varepsilon$, $a(0)\neq 1$, either $c'=\upd(c)$ or $c'(0) = c(0) = \hat{c}(0)=1$, $d'=d$ and $a'=\upd(a)$.
No action is performed on $\tau_i$ which must not submit a new job now ($a(0)\neq 1$) and does not finish now. 
For $c'(0) = c(0) = \hat{c}(0)=1$, it denotes that the job already finished during a previous clock tick.
The knowledge of the scheduler ($c'$ and $a'$) is updated accordingly.
\end{enumerate}
The cost of and edge $e$ is: $L(e)=c = cost(i)$ if $L(e) = killANDsub$.
As said earlier, the cost is incurred when the $killANDsub$ action is performed by some task $\tau_i$, although the deadline miss might have occurred earlier.
Finally, the probability of an edge $e$ is $p$, where, for all $i\in [n]$:
\begin{align*}
  p &= 
        \begin{cases}
          c(0) \cdot (1-a(0))&\text{if }\L(e)=fin\\
          c(0) \cdot a(0)&\text{if }\L(e)=sub\\
          (1-c(0)) \cdot a(0)&\text{if }\L(e)=killANDsub\\
          (1-c(0)) \cdot (1-a(0))&\text{if }\L(e)=\varepsilon\text{ and }c(0) \neq 1 \\
          1-a(0)&\text{if }\L(e)=\varepsilon\text{ and }c(0) = 1.
        \end{cases}
\end{align*}

Let $\supp(I) = \{n_1, \dots, n_i\}$ where w.l.o.g. $n_1 < \dots < n_i$.
Finally, the set of initial vertices is $\{\initV=\big((c_j,d_j,a_j), \playerOne\big) \mid j \in [|\supp(I)|]\} \subseteq V_\playerOne$ such that $(c_j,d_j,a_j)=(\C,D,\A)$ corresponding to $0 \in \supp(I)$; and $(c_j,d_j,a_j)=(c, 0, a)$ with $c(0)=1$ and $a_j(n_j) > 0$ otherwise, that is for $n_j \neq 0$.
This finishes the definition of $\Gamma$.
As explained above, the MDP $\Gamma_{\tau_i}$ modelling our problem is the portion of
$\Gamma$ that is reachable from $\initV$.
One can check, by a careful inspection of the definitions, that $\Gamma_{\tau_i}$ is indeed finite.
