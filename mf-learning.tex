\section{On shielded model-free learning}\label{sec:model-free-learning}
One of the main elements according to which one can classify
\emph{reinforcement learning} algorithms is their use of
\emph{models}\cite{suttonbarto98}.
More precisely, a learning algorithm may initially perform some learning of
how an unknown environment evolves by playing actions and observing their
outcomes. Based on the learnt model, the algorithm may then compute a
\emph{plan} (in our notation, a \emph{strategy}) to follow for some number of
steps. Learning algorithms which compute such models are \emph{model based}.
Clearly, the approach we have pursued in the previous section is model based.

In contrast, \emph{model-free} methods focus on keeping minimal information
about the unknown environment. This information often takes the form of a
state-action table that assigns to every pair a value based on previous
observations and which is updated by the algorithm periodically. (Crucially,
model-free algorithms perform minimal computation during their execution, i.e.
they only update the value table and choose actions based on its contents.) In
this section we describe such a learning algorithm which is applicable to the
problem at hand.

\subsection{Q-learning for discounted sum}
The most well-known model-free learning algorithm is \emph{Q-learning}, a
simple on-the-fly version of value iteration due to Watkins and
Dayan~\cite{wd92}. Unfortunately, the Q-learning algorithm solves the
optimization problem for a different function than the mean payoff.
Nonetheless, there is a strong link between both functions which we will
shortly state. Before doing so, we first give a definition of the
discounted-sum function.

\paragraph{Discounted sum.} Let $d \in \mathbb{R}$ be a \emph{discount factor}
such that $0 < d < 1$. For a prefix $\rho = e_0 e_1 \dots e_{n-1}$, we define
$\mathsf{DS}(\rho) = \sum^{n-1}_{i=0} \mathsf{cost}(e_i) d^i$. Then, for a
play $\pi = e_0e_1 \dots$ we set $\mathsf{DS}(\pi) = \lim_{n \to \infty}
\mathsf{DS}(\pi(n))$ --- indeed, the limit always exists. Note that, for all
discount factors, the discounted-sum function is Borel-definable and hence
measurable.

\paragraph{Q-learning.}
Given a fully known Markov decision process, computing the maximal expected
discounted-sum value for each state can be realized using a value-iteration
procedure~\cite{puterman94}. The update rule for value iteration is based on
the following Bellman optimality inequalities which are easy to verify.
\[
  \mathsf{DS}(v) = \begin{cases}
    \max_{v' \in E(v)} \mathsf{DS}(v') & \text{if } v \in V_{\Square}\\
    \sum_{v' \in E(v)} \mathit{Prob}(v')\mathsf{DS}(v')d &
      \text{if } v \in V_{\Circle}
  \end{cases}
\]

The Q-learning algorithm works on partially-known MDPs and it can be seen as a
stochastic approximation of the value iteration procedure. It essentially
consists in following a learning strategy which executes the following update
rule after every witnessed step $t$ from $v \in V_{\Square}$ to $v' \in
V_{\Circle}$ and then to $v'' \in V_{\Square}$, while observing a cost of $c$.
\[
  Q^{\mathit{new}}(v,v') \leftarrow (1-\alpha_t) \cdot Q(v,v') + \alpha_t \left(
    c + d \max_{w \in E(v'')} Q(v'',w)
  \right)
\]
The value of $\alpha$ determines the balance between exploration and
exploitation with respect to the \emph{Q-values} that have already been
learned. In very rough terms, \emph{deep reinforcement learning} extends
Q-learning by using \emph{neural networks} to encode the Q-value table in a
succinct and (hopefully) generalizable way.

We refer the reader interested in the technical details of the algorithms or
the guarantees that they provide to~\cite{wd92,suttonbarto98,deeprl}.

\paragraph{Discounted sum and mean payoff.}
Although the functions do not seem connected, we have the following equality
which has been observed in the literature~\cite{Solan03,mn81} holds for all MDPs $M$.
\[
  \mathbb{E}^M_{v_\mathit{init}}(\mathsf{MC}) = \lim_{d \to 1} \left(
    d \cdot \mathbb{E}^M_{v_\mathit{init}}(\mathsf{DS})
  \right)
\]
This relation between the functions has been exploited to argue that
Q-learning can be applied to problems in which the mean payoff is of interest
(see, e.g.,~\cite{tacas19}). Indeed, to the best of our knowledge, there is
currently no online model-free algorithm which directly works for mean
payoff~\cite{gosavi09} despite its importance in AI~\cite{schwartz93}.

\subsection{Shields for safety}
A straightforward yet elegant way of pairing learning algorithms with safety
guarantees is to couple them with \emph{safety shields}. The work of Alshiekh
et al.~\cite{shields} proposes two ways of using information about the safety
region of an MDP as follows. Let us write $v \mathrel{S} v'$ if and only if
$v' \in E(v)$ and both $v,v'$ are in the safe region for $\Square$ (that is,
$\Square$ has a memoryless strategy such that from both vertices he can make
sure the unsafe vertices are never visited).

\paragraph{Preemptive shielding.} This approach essentially adds constraints
on the kind of learning strategies $\sigma$ that $\Square$ may use. A
preemptively-shielded strategy $\sigma \circ S$ maps a prefix $h v$ to
$\sigma(hv)$ and, for all such $hv$, we have that $v \mathrel{S} \sigma(hv)$.
Intuitively, the shield impedes the learning strategy from using any unsafe
moves. Importantly, this happens during training (a.k.a. learning).

\paragraph{Post-poned shielding.} A second way of using $S$ is to let $\sigma$
play whatever move it wants but to modify it in the composition with the
shield. That is, a post-poned-shielded strategy $S \circ \sigma$ deals with a
play prefix $h v $ as follows.
\[
  S \circ \sigma(hv) = \begin{cases}
    \sigma(hv) & \text{if } v \mathrel{S} \sigma(hv)\\
    v' \text{ s.t. } v \mathrel{S} v' & \text{otherwise}
  \end{cases}
\]

Observe that both shielding strategies guarantee that the resulting strategy
is safe. However, post-poned shielding requires that the shielded strategy be
used even after training (i.e. during deployment) since $\sigma$ is not
guaranteed to be safe on its own.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
